package cnki.kg.demo;

import cnki.kg.demo.entity.CnkiDetailRepoPageProcessor;
import cnki.kg.demo.util.KBaseUtils;
import cnki.kg.demo.util.StringUtil;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import us.codecraft.webmagic.Spider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@SpringBootTest
@WebAppConfiguration
public class TestDic {
    @Autowired
    private KBaseUtils kBaseUtil;
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Test
    public void GetDicWord() throws SQLException {
        String sql = "SELECT count(*) FROM NAVIFREQ_TOP50_CLASSIFY ";
        Integer totalCount = kBaseUtil.excuteSalar(sql);
        Integer pageSize = 500;
        Integer toatlPage = totalCount / pageSize + ((totalCount % pageSize) == 0 ? 0 : 1);
        for (Integer pageIndex = 1732; pageIndex < toatlPage; pageIndex++) {
            System.out.println(pageIndex);
            String contentsql = String.format("SELECT 指标,指标类别,精炼标题 FROM NAVIFREQ_TOP50_CLASSIFY limit %s,%s", pageIndex * pageSize, pageSize);
            List<HashMap<String, String>> record = kBaseUtil.excuteQuery(contentsql);
            List<String> valueReco = record.stream().map(n -> String.format("(113,6,'sa',now(),'%s','%s','%s')", n.get("指标").replace("\'", "\\'"), n.get("指标类别").replace("\'", "\\'"), n.get("精炼标题").replace("\'", "\\'"))).collect(Collectors.toList());
            //每页插入一次
            String values = String.join(",", valueReco);
            String insertSql = String.format("insert into kg_word_item_00000113_copy1 (DICID,STATE,CREATEUSER,CREATEDATE,CONTENT,`first`,`second`) values %s ON DUPLICATE KEY UPDATE CONTENT =VALUES(CONTENT)", values);
            System.out.println(insertSql);
            jdbcTemplate.execute(insertSql);
        }
    }

    public Integer InsertReturnId(String sql) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
                                @Override
                                public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
                                    PreparedStatement ps = conn.prepareStatement(sql, new String[]{});
                                    ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                                    return ps;
                                }
                            },
                keyHolder);
        return keyHolder.getKey().intValue();
    }

    @Test
    public void GetSecondCategory() throws SQLException {

        String contentsql = "SELECT 精炼标题 FROM NAVIFREQ_TOP50_CLASSIFY  group by 精炼标题 ";
        List<HashMap<String, String>> record = kBaseUtil.excuteQuery(contentsql);
        record.forEach(n -> {
            String second = n.get("精炼标题");
            System.out.println(second);
            Integer pid = 0;
            String sqlExists = String.format("select ID from kg_word_item_00000113_copy1 where content='%s'", second);
            List<Map<String, Object>> objectExists = jdbcTemplate.queryForList(sqlExists);
            if (objectExists != null && objectExists.size() > 0) {
                pid = Integer.parseInt(objectExists.get(0).get("ID").toString());
            } else {
                String insertSql = String.format("insert into kg_word_item_00000113_copy1 (DICID,STATE,CREATEUSER,CREATEDATE,CONTENT)  VALUES (113,  3,'sa', now(),'%s')", second);
                pid = InsertReturnId(insertSql);
            }
            if (pid > 0) {
                String indicatorRecordSql = String.format("select ID from kg_word_item_00000113_copy1 where second='%s'", second);
                List<Map<String, Object>> objectrecord = jdbcTemplate.queryForList(indicatorRecordSql);
                Integer finalPid = pid;
                List<String> valueReco = objectrecord.stream().map(m -> String.format("('%s', '%s', '%s', 2, 'sa', now(), 'sa', now())", finalPid, m.get("ID"), 2)).collect(Collectors.toList());
                String values = String.join(",", valueReco);
                //每类插入一次
                String relationSql = String.format("INSERT INTO kg_word_item_relation_00000113_copy1 (ORGID,TARID,RELTYPE,STATE,CREATEUSER,CREATEDATE,MODIFYUSER,MODIFYDATE) VALUES %s", values);
                jdbcTemplate.execute(relationSql);
            }

        });
    }

    @Test
    public void GetSecondCategory2() throws SQLException {

        String contentsql = "SELECT 精炼标题 FROM NAVIFREQ_TOP50_CLASSIFY  group by 精炼标题 ";
        List<HashMap<String, String>> record = kBaseUtil.excuteQuery(contentsql);
        Integer i = 0;
        for (HashMap<String, String> n : record) {
            i++;
            String second = n.get("精炼标题");
            System.out.println(String.format("%s,%s/%s", second, i, record.size()));
            Integer pid = 0;
            String sqlExists = String.format("select ID from kg_word_item_00000113_copy1 where content='%s'", second);
            List<Map<String, Object>> objectExists = jdbcTemplate.queryForList(sqlExists);
            if (objectExists != null && objectExists.size() > 0) {
                //pid=Integer.parseInt(objectExists.get(0).get("ID").toString());
                System.out.println(String.format("存在指标%s,跳过,%s/%s", second, i, record.size()));
            } else {
                String insertSql = String.format("insert into kg_word_item_00000113_copy1 (DICID,STATE,CREATEUSER,CREATEDATE,CONTENT)  VALUES (113,  3,'sa', now(),'%s')", second);
                pid = InsertReturnId(insertSql);
            }
            if (pid > 0) {
                String indicatorRecordCountSql = String.format("select count(ID) from kg_word_item_00000113_copy1 where second='%s'", second);
                Integer ctotalCount = jdbcTemplate.queryForObject(indicatorRecordCountSql, Integer.class);
                Integer pageSize = 500;
                Integer toatlPage = ctotalCount / pageSize + ((ctotalCount % pageSize) == 0 ? 0 : 1);
                for (Integer pageIndex = 0; pageIndex < toatlPage; pageIndex++) {
                    String indicatorRecordSql = String.format("select ID from kg_word_item_00000113_copy1 where second='%s' limit %s,%s", second, pageIndex * pageSize, pageSize);
                    List<Map<String, Object>> objectrecord = jdbcTemplate.queryForList(indicatorRecordSql);
                    Integer finalPid = pid;
                    if (objectrecord.size() <= 500 && objectrecord.size() > 0) {
                        List<String> valueReco = objectrecord.stream().map(m -> String.format("('%s', '%s', '%s', 2, 'sa', now(), 'sa', now())", finalPid, m.get("ID"), 2)).collect(Collectors.toList());
                        String values = String.join(",", valueReco);
                        //每类插入一次
                        String relationSql = String.format("INSERT INTO kg_word_item_relation_00000113_copy1 (ORGID,TARID,RELTYPE,STATE,CREATEUSER,CREATEDATE,MODIFYUSER,MODIFYDATE) VALUES %s", values);
                        jdbcTemplate.execute(relationSql);
                    }
                }


            }

        }
    }

    @Test
    public void GetSearchIndicator() {
        String indicatorRecordCountSql = String.format("select count(ID) from kg_word_item_00000113 where second='%s'", "检索指标");
        Integer ctotalCount = jdbcTemplate.queryForObject(indicatorRecordCountSql, Integer.class);
        Integer pageSize = 500;
        Integer toatlPage = ctotalCount / pageSize + ((ctotalCount % pageSize) == 0 ? 0 : 1);
        for (Integer pageIndex = 0; pageIndex < toatlPage; pageIndex++) {
            String indicatorRecordSql = String.format("select ID from kg_word_item_00000113 where second='%s' limit %s,%s", "检索指标", pageIndex * pageSize, pageSize);
            List<Map<String, Object>> objectrecord = jdbcTemplate.queryForList(indicatorRecordSql);
            Integer finalPid = 1252545;
            if (objectrecord.size() <= 500 && objectrecord.size() > 0) {
                List<String> valueReco = objectrecord.stream().map(m -> String.format("('%s', '%s', '%s', 2, 'sa', now(), 'sa', now())", finalPid, m.get("ID"), 2)).collect(Collectors.toList());
                String values = String.join(",", valueReco);
                //每类插入一次
                String relationSql = String.format("INSERT INTO kg_word_item_relation_00000113 (ORGID,TARID,RELTYPE,STATE,CREATEUSER,CREATEDATE,MODIFYUSER,MODIFYDATE) VALUES %s", values);
                jdbcTemplate.execute(relationSql);
            }
        }
    }

    @Test
    public void GetCJFDClass() throws SQLException {
       /* String sql = "SELECT count(*) FROM ZJCLS ";
        Integer totalCount = kBaseUtil.excuteSalar(sql);
        Integer pageSize = 500;
        Integer toatlPage = totalCount / pageSize + ((totalCount % pageSize) == 0 ? 0 : 1);
            for (Integer pageIndex = 0; pageIndex < toatlPage; pageIndex++) {
                String sqlQuery = String.format("SELECT SYS_FLD_CLASS_CODE,SYS_FLD_CLASS_NAME,SYS_FLD_PARENT_CODE,SYS_FLD_CLASS_GRADE,SYS_FLD_CHILD_FLAG FROM ZJCLS limit %s,%s", pageIndex * pageSize, pageSize);
                List<HashMap<String, String>> record = kBaseUtil.excuteQuery(sqlQuery);
                for (HashMap<String, String> remap : record) {
                    String code = remap.get("SYS_FLD_CLASS_CODE");
                    String name = remap.get("SYS_FLD_CLASS_NAME");
                    String parentCode = remap.get("SYS_FLD_PARENT_CODE");
                    String grade = remap.get("SYS_FLD_CLASS_GRADE");
                    String childflag = remap.get("SYS_FLD_CHILD_FLAG");
                    String insertSql = String.format("INSERT INTO `searchnavcategorydetail` VALUES (null, '%s', '%s', '161', null, '%s', '%s', '%s', '1', null, null, null, null, 'sa', now(), 'sa', now())"
                            , name,code,parentCode,(Integer.parseInt(grade)-1),childflag.equals('0')?1:0);
                    System.out.println(insertSql);
                    jdbcTemplate.execute(insertSql);
                }
            }*/
        String recordSql = "select * from searchnavcategorydetail where SearchNavCategoryId=161 ";
        List<Map<String, Object>> objectrecord = jdbcTemplate.queryForList(recordSql);
        for (Map<String, Object> mp : objectrecord) {
            String parentCode = mp.get("ParentCode") == null ? "" : mp.get("ParentCode").toString();
            String recordId = mp.get("CategoryId").toString();
            String categorycode = mp.get("CategoryCode").toString();
            if (StringUtil.isBlank(parentCode) || categorycode.equals(parentCode)) continue;
            List<Map<String, Object>> item = objectrecord.stream().filter(n -> n.get("CategoryCode").toString().equals(parentCode)).collect(Collectors.toList());
            if (item != null && item.size() > 0) {
                String pid = item.get(0).get("CategoryId").toString();
                String updatesql = String.format("update searchnavcategorydetail set ParentId='%s' where SearchNavCategoryId=161 and CategoryId='%s'", pid, recordId);
                System.out.println(updatesql);
                jdbcTemplate.execute(updatesql);
            }
        }
    }

    @Test
    public void respairClassParentCode() {
        String recordSql = "select * from searchnavcategorydetail where SearchNavCategoryId=161 ";
        List<Map<String, Object>> objectrecord = jdbcTemplate.queryForList(recordSql);
        for (Map<String, Object> mp : objectrecord) {
            String parentCode = mp.get("ParentCode") == null ? "" : mp.get("ParentCode").toString();
            if (StringUtil.isBlank(parentCode) || parentCode.length() <= 4) continue;
            String recordId = mp.get("CategoryId").toString();
            String f = parentCode.substring(0, 4);
            String s = parentCode.substring(4, parentCode.length());
            String realPcode = f + "_" + s;
            String updatesql = String.format("update searchnavcategorydetail set ParentCode='%s' where SearchNavCategoryId=161 and CategoryId='%s'", realPcode, recordId);
            System.out.println(updatesql);
            jdbcTemplate.execute(updatesql);
        }
    }

    @Test
    public void GetCTWH() throws SQLException {
        String sql = "SELECT count(*) FROM WHBR ";
        Integer totalCount = kBaseUtil.excuteSalar(sql);
        Integer pageSize = 500;
        Integer toatlPage = totalCount / pageSize + ((totalCount % pageSize) == 0 ? 0 : 1);
        for (Integer pageIndex = 0; pageIndex < toatlPage; pageIndex++) {
            System.out.println(pageIndex);
            String contentsql = String.format("SELECT 工具书编号,工具书名称,条目名称,条目编码,条目作者,条目字数,正文快照,专题代码,行业分类代码, 条目原书页码,词目 FROM WHBR  limit %s,%s", pageIndex * pageSize, pageSize);
            List<HashMap<String, String>> record = kBaseUtil.excuteQuery(contentsql);
            List<String> valueReco = record.stream().map(n -> String.format("('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                    n.get("工具书编号"), getText(n.get("工具书名称")), getText(n.get("条目名称")), n.get("条目编码"), getText(n.get("条目作者")), n.get("条目字数"), getText(n.get("正文快照"))
                    , n.get("专题代码"), n.get("行业分类代码"), n.get("条目原书页码"), n.get("词目"))).collect(Collectors.toList());
            //每页插入一次
            String values = String.join(",", valueReco);
            String insertSql = String.format("insert into ctwh (工具书编号,工具书名称,条目名称,条目编码,条目作者,条目字数,正文快照,专题代码,行业分类代码,条目原书页码,词目) values %s ", values);
            System.out.println(insertSql);
            jdbcTemplate.execute(insertSql);
        }
    }

    public String getText(String content) {
        String txtcontent = content;
        txtcontent=txtcontent.replaceAll("\r\n","");
        txtcontent=txtcontent.replaceAll("<br>","");
        txtcontent=txtcontent.replaceAll("<small>","").replaceAll("</small>","");
        txtcontent=txtcontent.replaceAll("<sm>","").replaceAll("</sm>","");
        txtcontent=txtcontent.replaceAll("</?[^>]+>", ""); //剔出<html>的标签  
        txtcontent = txtcontent.replaceAll("<a>\\s*|\t|\r|\n|</a>", "");//去除字符串中的空格,回车,换行符,制表符
        txtcontent=txtcontent.replaceAll("'","\\’");
        txtcontent=txtcontent.replaceAll("/<(w+)[^>]+>.*?</1>/g","");
        txtcontent=txtcontent.replaceAll("&#.+?;", "");
        txtcontent = txtcontent.replaceAll("\\s/g","");
        return txtcontent;
    }
    @Test
    public void spliderCnki(){
        String urlFormat="http://r.cnki.net/KCMS/detail/detail.aspx?dbcode=WHBR&dbname=WHBR&filename=%s&uid=%s";
        String uid="WEEvREcwSlJHSldRa1FhcTdnTnhXM28yRU1QcHRSTFJYdWpaU3R4M2QxUT0=$9A4hF_YAuvQ5obgVAqNKPCYcEjKensW4IQMovwHtwkF4VYPoHbKxJw!!";
        String sql = "SELECT count(*) FROM ctwh ";
        Integer totalCount = jdbcTemplate.queryForObject(sql, Integer.class);
        Integer pageSize = 500;
        Integer toatlPage = totalCount / pageSize + ((totalCount % pageSize) == 0 ? 0 : 1);
        for (Integer pageIndex = 0; pageIndex < toatlPage; pageIndex++) {
            System.out.println(pageIndex);
            String contentsql = String.format("SELECT id,条目编码 FROM ctwh  limit %s,%s", pageIndex * pageSize, pageSize);
            List<Map<String, Object>> record = jdbcTemplate.queryForList(contentsql);
            for (Map<String, Object> ob : record) {
                Integer id=Integer.parseInt(ob.get("id").toString());
                String detailsql=String.format("SELECT count(*) FROM ctwhdetail where id=%s",id);
                Integer detailtotalCount = jdbcTemplate.queryForObject(detailsql, Integer.class);
                if(detailtotalCount==0){
                    String filename=ob.get("条目编码").toString();
                    String url=String.format(urlFormat,filename,uid);
                    Spider.create(new CnkiDetailRepoPageProcessor(id,jdbcTemplate,filename)).addUrl(url).thread(1).run();
                }else{
                    System.out.println("条目："+ob.get("条目编码")+" 已存在，跳过");
                }
            }

        }
    }
}
