package cnki.kg.demo;

import cnki.kg.demo.util.Neo4jUtil;
import cnki.kg.demo.util.StringUtil;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


@SpringBootTest
@WebAppConfiguration
public class UpdateGraphLabel {
    @Autowired
    Neo4jUtil neo4jUtil;
    @Test
    public void update(){
        String domainLabel="科协图谱_ee9b0166-81dd-4c3f-b6db-ee9a72a0e254";
        String cypher = String.format("match(n:`%s`) where n.label is not null and n.label<>[] RETURN n",domainLabel);
        List<HashMap<String, Object>> nodes = neo4jUtil.GetGraphNode(cypher);
        for (HashMap<String, Object> node : nodes) {
            String name = node.get("name").toString();
            String uuid = node.get("uuid").toString();
            String label = node.get("label").toString();
            if(StringUtil.isNotBlank(label)){
                label=label.replaceAll("\\[","").replaceAll("]","");
                String[] labelArr=label.split(",");
                List<String> labels= new ArrayList<>(Arrays.asList(labelArr));
                if(!labels.contains("18")){
                    labels.add("18");
                    String newLabel=String.format("[%s]",String.join(",",labels));
                    String cypherSql2 = String.format("MATCH (n:`%s`) where id(n)=%s set n.label=%s ", domainLabel, uuid,newLabel);
                    neo4jUtil.excuteCypherSql(cypherSql2);
                }

            }

            System.out.println(name);

        }
    }
}
