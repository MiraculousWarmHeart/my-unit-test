package cnki.kg.demo;

import cnki.kg.demo.util.KBaseUtils;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;


@SpringBootTest
@WebAppConfiguration
public class TestUploadRec {
    @Autowired
    private KBaseUtils kBaseUtil;
    @Test
    public void upload() throws Exception {
        String content="";
        StringBuilder builder=new StringBuilder();
        builder.append("<REC>");
        builder.append(String.format("<%s>=%s", "SOURCEID", "tttttttt"));
        content=builder.toString();
        kBaseUtil.InsertByRec("test-upload","PARTNERSEARCHDATA_KBASE20 ",content,true);
    }
}
