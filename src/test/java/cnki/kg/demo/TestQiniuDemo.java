package cnki.kg.demo;

import cnki.kg.demo.util.MimeTypes;
import cnki.kg.demo.util.StringUtil;
import com.qiniu.common.Zone;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.model.FileInfo;
import com.qiniu.storage.model.FileListing;
import com.qiniu.util.Auth;
import com.qiniu.util.StringUtils;
import net.coobird.thumbnailator.Thumbnails;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


@SpringBootTest
@WebAppConfiguration
public class TestQiniuDemo {
    private String accessKey = "2J5BOKpbxDlzkYVYZ5dwGS3jAevVmOJwcL3fIdpw";
    private String secretKey = "2LXAQbFFbFR_I76bseNEmu-Sjnh4RRaSRsazX5Dj";
    private String bucketName = "nndt";
    private String bucketHostName = "file.miaoleyan.com";
    private String prefixName = "/nndt";
    private String filePath = "";

    @Autowired
    @Qualifier("mysqlJdbcTemplate")
    JdbcTemplate mysqlJdbcTemplate;
    @Test
    public void Update() throws IOException {
        String sql = "select * from filelist ";
        List<Map<String, Object>> maps = mysqlJdbcTemplate.queryForList(sql);
        for (Map<String, Object> map : maps) {
            String id=map.get("id").toString();
            //String key=map.get("key").toString();
            String mimeType=map.get("mimeType").toString();
            String extTypeName = MimeTypes.getDefaultExt(mimeType);
            String update=String.format("update filelist set suffix='%s',scope='blog' where id=%s",extTypeName,id);
            mysqlJdbcTemplate.execute(update);
        }

    }
    @Test
    public void Test() throws IOException {
        Auth auth = Auth.create(this.accessKey, this.secretKey);
        //构造一个带指定Zone对象的配置类
        Configuration cfg = new Configuration(Zone.zone0());
        BucketManager bucketManager = new BucketManager(auth, cfg);
        //文件名前缀
        String prefix = "";
        String delimiter = "";
        String marker = "";
        int limit = 100;
        String insertSql = "insert into filelist (`key`,`hash`,fsize,mimeType,url) values ";
        List<String> params = new ArrayList<>();
        for (int i = 1; i <= 200; i++) {
            FileListing fileListing = bucketManager.listFiles(this.bucketName, prefix, marker, limit, delimiter);
            if (fileListing != null && fileListing.items.length > 0) {
                marker = fileListing.marker;
                if(i<8||i>9){
                    continue;
                }
                if (StringUtils.isNullOrEmpty(fileListing.marker)) {
                    break;
                }
                for (FileInfo item : fileListing.items) {
                    handData(params, item);
                }
                if (params.size() > 0) {
                    String fullSql = insertSql + String.join(",", params);
                    mysqlJdbcTemplate.execute(fullSql);
                    params.clear();
                }

            }

        }
    }

    public void handData(List<String> params, FileInfo item) throws IOException {
        String urlString = String.format("http://%s/%s", this.bucketHostName, item.key);
        System.out.println(urlString);
        // 构造URL
        URL url = null;
        try {
            url = new URL(urlString);
            // 打开连接
            URLConnection con = url.openConnection();
            // 输入流
            InputStream is = con.getInputStream();
            // 1K的数据缓冲
            byte[] bs = new byte[1024];
            // 读取到的数据长度
            int len;
            // 输出的文件流
            String extTypeName = MimeTypes.getDefaultExt(item.mimeType);
            String filename = "D:\\qiniufile\\" + item.key;  //下载路径及下载图片名称
            String targetfilename = "D:\\qiniufile\\thumbnails\\" + item.key;  //下载路径及下载图片名称
            if(StringUtil.isNotBlank(extTypeName)&&!"unknown".equalsIgnoreCase(extTypeName)){
                filename = "D:\\qiniufile\\" + item.key + "." + extTypeName;  //下载路径及下载图片名称
                targetfilename = "D:\\qiniufile\\thumbnails\\" + item.key + "." + extTypeName;  //下载路径及下载图片名称
            }
            File file = new File(filename);
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            String str = String.format("('%s','%s','%s','%s','%s')", item.key, item.hash, item.fsize, item.mimeType, String.format("http://%s/%s", this.bucketHostName, item.key));

            params.add(str);
            FileOutputStream os = new FileOutputStream(file, true);
            // 开始读取
            while ((len = is.read(bs)) != -1) {
                os.write(bs, 0, len);
            }
            String[] allowMimeTypes=new String[]{"image/png","image/jpeg","image/bmp"};
            List<String> allowMimeTypeList=new ArrayList<>(Arrays.asList(allowMimeTypes));

            File fileThumbnails = new File(targetfilename);
            if (!fileThumbnails.exists()) {
                fileThumbnails.getParentFile().mkdirs();
                fileThumbnails.createNewFile();
                if (allowMimeTypeList.contains(extTypeName)){
                    Thumbnails.of(filename)
                            .scale(1f)
                            .outputQuality(0.5f)
                            .toFile(targetfilename);
                }else{
                    FileOutputStream os2 = new FileOutputStream(fileThumbnails, true);
                    // 开始读取
                    while ((len = is.read(bs)) != -1) {
                        os2.write(bs, 0, len);
                    }
                    os2.close();
                }
            }
            // 完毕，关闭所有链接
            os.close();
            is.close();
            //其中的scale是可以指定图片的大小，值在0到1之间，1f就是原图大小，0.5就是原图的一半大小，这里的大小是指图片的长宽。
            //而outputQuality是图片的质量，值也是在0到1，越接近于1质量越好，越接近于0质量越差。
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void Test2() {
        String mimeType1 = "audio/mpeg";
        String mimeType2 = "image/png";
        String mimeType3 = "image/gif";
        String a1 = MimeTypes.getDefaultExt(mimeType1);
        String a2 = MimeTypes.getDefaultExt(mimeType2);
        String a3 = MimeTypes.getDefaultExt(mimeType3);
        System.out.println(a1);
        System.out.println(a2);
        System.out.println(a3);
    }

    @Test
    public void Test3() throws IOException {
        String path = "D:\\qiniufile\\9IdICTAM3obxEsi3PM4a5DZFgZjSvWhL";
        String path2 = "D:\\qiniufile\\thumbnails\\9IdICTAM3obxEsi3PM4a5DZFgZjSvWhL";
        File file = new File(path2);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
            Thumbnails.of(path)
                    .scale(1f)
                    .outputQuality(0.5f)
                    .toFile(path2);
        }

    }
}
