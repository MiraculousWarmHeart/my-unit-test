package cnki.kg.demo;

import cnki.kg.demo.util.HttpClientUtil;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;


@SpringBootTest
@WebAppConfiguration
public class TestInvokeApi {
    /**
     * 定义并发线程数量
     */
    public static final int THREAD_NUM = 20;

    /**
     * 开始时间
     */
    private static long startTime = 0L;
    @Test
    public void invoke2() throws Exception {
        //这些参数写入了数据库，从数据库直接取
        StringBuilder builder = new StringBuilder();
        String text = "";
        String keyword = "务正线的设计速度";
        String nlpIntentUrl = "http://10.170.128.57:7221/recognition";
        Map<String, String> param = new HashMap<>();
        param.put("text", keyword);//自然语言问句
        for (int i = 0; i < THREAD_NUM; i ++) {
            String intentStr = HttpClientUtil.doGet(nlpIntentUrl, param);
            long endTime = System.currentTimeMillis();
            builder.append(String.format("第[%s]次调用=============================\r\n",i));
            builder.append(intentStr);
            builder.append("\r\n");
            String tx= " ended at: " + endTime + ", cost: " + (endTime - startTime) + " ms.";
            builder.append(tx);
            builder.append("\r\n");

        }
        text = builder.toString();
        outputlog(text);
    }
    @Test
    public void invoke() {
        //这些参数写入了数据库，从数据库直接取
        StringBuilder builder = new StringBuilder();
        String text = "";
        ArrayList<Future<String>> results = new ArrayList<>();
        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_NUM);
        for (int i = 0; i < THREAD_NUM; i++) {
            results.add(executorService.submit(new RecognitionRun(i)));
        }
        //        等待所有的线程的返回值都结束
        boolean isDone = false;
        while (!isDone) {
            isDone = true;
            for (Future<String> future : results) {
                if (!future.isDone()) {
                    isDone = false;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {

                    }
                    break;
                }
            }
        }
        executorService.shutdown();
        for (Future<String> future : results) {
            try {
                //  取返回值
                builder.append(future.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        text = builder.toString();
        outputlog(text);
    }
    private void outputlog(String resultJson) {
        try {
            String path = "D:\\text\\recognition.txt";
            File file = new File(path);
            if (!file.exists()) {
                file.getParentFile().mkdirs();
            }
            file.createNewFile();
            // write 解决中文乱码问题
            // FileWriter fw = new FileWriter(file, true);
            OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(resultJson);
            bw.flush();
            bw.close();
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    /**
     * 线程类
     */
    class RecognitionRun implements Callable <String> {

        private final int index;
        public RecognitionRun(int index) {
            this.index = index;
        }

        @Override
        public String call() throws Exception {
            String keyword = "务正线的设计速度";
            String nlpIntentUrl = "http://10.170.128.57:7221/recognition";
            Map<String, String> param = new HashMap<>();
            param.put("text", keyword);//自然语言问句
            String intentStr="";
            intentStr+=String.format("第[%s]次调用=============================\r\n",index);
            intentStr+= HttpClientUtil.doGet(nlpIntentUrl, param);
            intentStr+="\r\n";
            return intentStr;
        }

    }

}
