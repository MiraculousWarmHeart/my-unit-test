package cnki.kg.demo;

import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@SpringBootTest
@WebAppConfiguration
public class TestMergeCategoryCode {
    @Test
    public void merge() throws Exception {
        String[] codeArr=new String[]{"001","001002","001003","001003007","004","004005","004006","008009"};//=>"001","004","008009"
        List<String> codeList= new ArrayList<>(Arrays.asList(codeArr));
        List<String> result= new ArrayList<>(codeList);
        for (String code : codeList) {
            List<String> arr= codeList.stream().filter(n->n.startsWith(code)&&!n.equals(code)).collect(Collectors.toList());
            if(arr.size()>0){
                result.removeAll(arr);
            }
        }

        System.out.println("success");
    }
}
