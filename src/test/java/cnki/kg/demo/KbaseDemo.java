package cnki.kg.demo;

import cnki.kg.demo.util.KBaseUtils;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.*;
import java.util.HashMap;
import java.util.List;


@SpringBootTest
@WebAppConfiguration
public class KbaseDemo {
    @Autowired
    private KBaseUtils kBaseUtil;
    @Test
    public void respairDb() throws IOException {
        //String sql1 = "ALTER TABLE PARTNERSEARCHDATA_KBASE%s ALTER FFD AS FFD NUM(8) ASCII INDEX NUM NORMAL ";
        String sqlCateField1="ALTER TABLE PARTNERSEARCHDATA_KBASE%s ADD CATEGORYFIELD1  MVCHAR(32767) ASCII INDEX MVCHAR NORMAL ALIASNAME 分类扩展1";
        String sqlCateField2="ALTER TABLE PARTNERSEARCHDATA_KBASE%s ADD CATEGORYFIELD2  MVCHAR(32767) ASCII INDEX MVCHAR NORMAL ALIASNAME 分类扩展2";
        String sqlCateField3="ALTER TABLE PARTNERSEARCHDATA_KBASE%s ADD CATEGORYFIELD3  MVCHAR(32767) ASCII INDEX MVCHAR NORMAL ALIASNAME 分类扩展3";
        String sqlGroupField1="ALTER TABLE PARTNERSEARCHDATA_KBASE%s ALTER GROUPFIELD1 AS GROUPFIELD1  MVCHAR(32767) ASCII INDEX MVCHAR NORMAL ALIASNAME 分组扩展1";
        String sqlGroupField2="ALTER TABLE PARTNERSEARCHDATA_KBASE%s ALTER GROUPFIELD2 AS GROUPFIELD2  MVCHAR(32767) ASCII INDEX MVCHAR NORMAL ALIASNAME 分组扩展2";
        String sqlGroupField3="ALTER TABLE PARTNERSEARCHDATA_KBASE%s ALTER GROUPFIELD3 AS GROUPFIELD3  MVCHAR(32767) ASCII INDEX MVCHAR NORMAL ALIASNAME 分组扩展3";
        String sqlSortField1="ALTER TABLE PARTNERSEARCHDATA_KBASE%s ALTER SORTFIELD1 AS SORTFIELD1  MVCHAR(32767) ASCII INDEX MVCHAR NORMAL ALIASNAME 排序扩展1";
        String sqlSortField2="ALTER TABLE PARTNERSEARCHDATA_KBASE%s ALTER SORTFIELD2 AS SORTFIELD2  MVCHAR(32767) ASCII INDEX MVCHAR NORMAL ALIASNAME 排序扩展2";
        String sqlSortField3="ALTER TABLE PARTNERSEARCHDATA_KBASE%s ALTER SORTFIELD3 AS SORTFIELD3  MVCHAR(32767) ASCII INDEX MVCHAR NORMAL ALIASNAME 排序扩展3";
      /*  String sql3 = "UPDATE PARTNERSEARCHDATA_KBASE%s SET FFD=0.0001";
        String sql4 = "UPDATE PARTNERINDICATORSEACH_KBASE%s SET FFD=0.09";*/
      StringBuilder builder=new StringBuilder();
        for (Integer i = 1; i <= 20; i++) {
            builder.append(String.format(sqlCateField1+"\r\ngo\n", i));
            builder.append(String.format(sqlCateField2+"\r\ngo\n", i));
            builder.append(String.format(sqlCateField3+"\r\ngo\n", i));
            builder.append(String.format(sqlGroupField1+"\r\ngo\n", i));
            builder.append(String.format(sqlGroupField2+"\r\ngo\n", i));
            builder.append(String.format(sqlGroupField3+"\r\ngo\n", i));
            builder.append(String.format(sqlSortField1+"\r\ngo\n", i));
            builder.append(String.format(sqlSortField2+"\r\ngo\n", i));
            builder.append(String.format(sqlSortField3+"\r\ngo\n", i));
        }
        String sql=builder.toString();
        String path = "D:\\text\\alertsql.txt";
        File file = new File(path);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
        }
        file.createNewFile();
        // write 解决中文乱码问题
        // FileWriter fw = new FileWriter(file, true);
        OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(sql);
        bw.flush();
        bw.close();
        fw.close();
    }
    @Test
    public void GetFromDb(){
        String sql="select * from model_kbase";
        List<HashMap<String, String>> result=kBaseUtil.excuteQuery(sql);
        for (HashMap<String,String> ob:result){
            for(String key:ob.keySet()){
                System.out.println(String.format("%s:%s",key,ob.get(key)));
            }

        }
    }
    @Test
    public void GetViewByName(){
        String name="期刊";
        kBaseUtil.getKbaseViewStruct(name);
    }
    @Test
    public void respairDbIndex() throws IOException {
        //String sql1 = "ALTER TABLE PARTNERSEARCHDATA_KBASE%s ALTER FFD AS FFD NUM(8) ASCII INDEX NUM NORMAL ";
        String sqlCateField1="index PARTNERSEARCHDATA_KBASE%s all";
        StringBuilder builder=new StringBuilder();
        for (Integer i = 1; i <= 20; i++) {
            builder.append(String.format(sqlCateField1+"\r\ngo\n", i));
        }
        String sql=builder.toString();
        String path = "D:\\text\\alertindexsql.txt";
        File file = new File(path);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
        }
        file.createNewFile();
        // write 解决中文乱码问题
        // FileWriter fw = new FileWriter(file, true);
        OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(sql);
        bw.flush();
        bw.close();
        fw.close();
    }
    @Test
    public void updateFullTextField() throws IOException {
        //String sql1 = "ALTER TABLE PARTNERSEARCHDATA_KBASE1 ALTER 全文 as FULLTEXT LTEXT(2147483647) ASCII INDEX QTEXT NORMAL ALIASNAME 全文 ";
        String sqlCateField1="ALTER TABLE PARTNERSEARCHDATA_KBASE%s ALTER FULLTEXT as FULLTEXT LTEXT(2147483647) ASCII INDEX QTEXT NORMAL ALIASNAME 全文";
        StringBuilder builder=new StringBuilder();
        for (Integer i = 1; i <= 20; i++) {
            builder.append(String.format(sqlCateField1+"\r\ngo\n", i));
        }
        String sql=builder.toString();
        String path = "D:\\text\\updatefulltextaliasql.txt";
        File file = new File(path);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
        }
        file.createNewFile();
        // write 解决中文乱码问题
        // FileWriter fw = new FileWriter(file, true);
        OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(sql);
        bw.flush();
        bw.close();
        fw.close();
    }
    @Test
    public void addTimeField() throws IOException {
        String sqlCateField1="ALTER TABLE PARTNERSEARCHDATA_KBASE%s ADD DATEFORMAT  DATE(10) ASCII INDEX DATE NORMAL ALIASNAME 时间";
        StringBuilder builder=new StringBuilder();
        for (Integer i = 1; i <= 20; i++) {
            builder.append(String.format(sqlCateField1+"\r\ngo\n", i));
        }
        String sql=builder.toString();
        String path = "D:\\text\\addtimesql.txt";
        File file = new File(path);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
        }
        file.createNewFile();
        // write 解决中文乱码问题
        // FileWriter fw = new FileWriter(file, true);
        OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(sql);
        bw.flush();
        bw.close();
        fw.close();
    }
    @Test
    public void indexTableField() throws IOException {
        String sqlCateField1="index PARTNERSEARCHDATA_KBASE%s on FULLTEXT";
        StringBuilder builder=new StringBuilder();
        for (Integer i = 1; i <= 20; i++) {
            builder.append(String.format(sqlCateField1+"\r\ngo\n", i));
        }
        String sql=builder.toString();
        String path = "D:\\text\\indexFulltextsql.txt";
        File file = new File(path);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
        }
        file.createNewFile();
        // write 解决中文乱码问题
        // FileWriter fw = new FileWriter(file, true);
        OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(sql);
        bw.flush();
        bw.close();
        fw.close();
    }
}
