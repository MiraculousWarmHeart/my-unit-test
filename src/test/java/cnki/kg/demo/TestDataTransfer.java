package cnki.kg.demo;

import cnki.kg.demo.datatransfer.DataSourceItem;
import cnki.kg.demo.datatransfer.DataTransfer;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.sql.SQLException;

@SpringBootTest
@WebAppConfiguration
public class TestDataTransfer {
    @Autowired
    DataTransfer dataTransfer;
    @Test
    public void process() throws SQLException, ClassNotFoundException {
        final String sourceUrl = "jdbc:kbase://10.120.130.89";
        DataSourceItem sourceItem=new DataSourceItem();
        sourceItem.setUrl(sourceUrl);
        sourceItem.setDriverName("com.kbase.jdbc.Driver");
        sourceItem.setUser("DBOWN");
        sourceItem.setPassword("");
        sourceItem.setTableName("CCND0005");
        String targetUrl = "jdbc:mysql://localhost:3307/cnki?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=Asia/Shanghai&allowMultiQueries=true&rewriteBatchedStatements=true";
        DataSourceItem targetItem=new DataSourceItem();
        targetItem.setUrl(targetUrl);
        targetItem.setDriverName("com.mysql.cj.jdbc.Driver");
        targetItem.setUser("root");
        targetItem.setPassword("123456");
        targetItem.setTableName("ccnd0006");
        dataTransfer.execute(sourceItem,targetItem);
    }
}
