package cnki.kg.demo;

import cnki.kg.demo.entity.FiledItem;
import cnki.kg.demo.util.ExcelUtil;
import cnki.kg.demo.util.Neo4jUtil;
import com.alibaba.fastjson.JSON;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;

import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Value;
import org.neo4j.driver.v1.types.Relationship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;


@SpringBootTest
@WebAppConfiguration
public class DemoApplicationTests {

    @Autowired
    private Neo4jUtil neo4jUtil;


    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Test
    public void addNav() {
        String domain="ctwh";
        String sql = "SELECT SYS_FLD_CLASS_CODE,navname FROM WHBT_CLS where navname is not null";
        List<Map<String, Object>> result=jdbcTemplate.queryForList(sql);
        System.out.println(result);
        for (Map<String, Object> item:result){
            String cypherSql = String.format("match (n:`%s`) where n.code='%s' set n.navname='%s' return n", domain, item.get("SYS_FLD_CLASS_CODE"),item.get("navname"));
            String ncypherSql = String.format("match (n:`%s`)-[r]->() where r.code='%s' set r.navname='%s' return n", domain, item.get("SYS_FLD_CLASS_CODE"),item.get("navname"));
            neo4jUtil.excuteCypherSql(cypherSql);
            neo4jUtil.excuteCypherSql(ncypherSql);
        }
    }
    @Test
    public void importJiaotongKG() {
        String domain="jtzj";
        String sqlcount = "SELECT count(*)  FROM searchcategory ";
        Integer totalCount =jdbcTemplate.queryForObject(sqlcount,Integer.class);
        Integer y=totalCount%500;
        Integer totalPage=y>0?totalCount/500+1:totalCount/500;
        //createnodes();//创建节点
        //创建关系
        for(Integer i=0;i<=totalPage;i++){
            System.out.println("i第"+i+"页");
            String sql = String.format("SELECT *  FROM searchcategory  limit %s,500",i*500);
            List<Map<String, Object>> result=jdbcTemplate.queryForList(sql);
            for (Map<String, Object> record:result){
                String parentCode=record.get("CategoryID").toString();
                String sqlchild = String.format("SELECT *  FROM searchcategory  where ParentID='%s' ",parentCode);
                List<Map<String, Object>> resultChild=jdbcTemplate.queryForList(sqlchild);
                for (Map<String, Object> item:resultChild){
                    String childCode=item.get("CategoryID").toString();
                    String cypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE n.code=%s AND m.code =%s "
                            + "merge (n)-[r:RE{name:'%s',sourcecode:%s,targetcode:%s}]->(m)" + "RETURN r", domain.trim(), domain.trim(), parentCode, childCode, "",parentCode,childCode);
                    neo4jUtil.excuteCypherSql(cypherSql);
                }

            }
        }
        System.out.println("success");
    }
    private void createnodes(){
        String sql = String.format("SELECT *  FROM searchcategory  limit %s,500",0);
        List<Map<String, Object>> result=jdbcTemplate.queryForList(sql);
        for (Map<String, Object> record:result){
            Map<String, Object> item=new HashMap<>();
            item.putAll(record);
            item.put("name",record.get("CategoryName"));
            item.put("code",record.get("CategoryID"));
            String propertiesString = neo4jUtil.getFilterPropertiesJson(JSON.toJSONString(item));
            String cypherSql = String.format("merge (n:`%s`%s) return n", "交通规划设计", propertiesString);
            neo4jUtil.excuteCypherSql(cypherSql);
        }
    }
    @Test
    public void updateJiaotongSearchCategory() {
        String domain="交通规划设计";
        String sqlcount = "SELECT count(*)  FROM searchcategory ";
        Integer totalCount =jdbcTemplate.queryForObject(sqlcount,Integer.class);
        Integer y=totalCount%500;
        Integer totalPage=y>0?totalCount/500+1:totalCount/500;
        createnodes();//创建节点
        //创建关系
        for(Integer i=0;i<=totalPage;i++){
            System.out.println("i第"+i+"页");
            String sql = String.format("SELECT *  FROM searchcategory  limit %s,500",i*500);
            List<Map<String, Object>> result=jdbcTemplate.queryForList(sql);
            for (Map<String, Object> record:result){
                String parentCode=record.get("CategoryID").toString();
                String sqlchild = String.format("SELECT *  FROM searchcategory  where ParentID='%s' ",parentCode);
                List<Map<String, Object>> resultChild=jdbcTemplate.queryForList(sqlchild);
                for (Map<String, Object> item:resultChild){
                    String childCode=item.get("CategoryID").toString();
                    String cypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE n.code=%s AND m.code =%s "
                            + "merge (n)-[r:RE{name:'%s',sourcecode:%s,targetcode:%s}]->(m)" + "RETURN r", domain.trim(), domain.trim(), parentCode, childCode, "",parentCode,childCode);
                    neo4jUtil.excuteCypherSql(cypherSql);
                }

            }
        }
        System.out.println("success");
    }
    @Test
    public void updateJiaotong2()
    {
        String sqlAll = "SELECT *  FROM searchcategory  ";
        List<Map<String, Object>> resultChild=jdbcTemplate.queryForList(sqlAll);
        getTree(0,"0","",resultChild);
    }
    @Test
    public void updateJiaotong()
    {
        String sqlAll = "SELECT *  FROM searchcategory  ";
        List<Map<String, Object>> resultChild=jdbcTemplate.queryForList(sqlAll);
        getTree(0,"0","",resultChild);
    }
    private void getTree(Integer parentId,String newparentId,String newparentCode,List<Map<String, Object>> nodelList) {
        Iterator<Map<String, Object>> treeList=nodelList.stream().filter(m->m.get("ParentID").equals(parentId)).iterator();
        while (treeList.hasNext()) {
            Map<String, Object> item = (Map<String, Object>) treeList.next();
            Integer pid=item.get("CategoryID")!=null?Integer.parseInt(item.get("CategoryID").toString()):0;
            String CategoryName=item.get("CategoryName")!=null?item.get("CategoryName").toString():"";
            String IsLeaf=item.get("IsLeaf")!=null?item.get("IsLeaf").toString():"";
            String TableCode=item.get("TableCode")!=null?item.get("TableCode").toString():"";
            String TableName=item.get("TableName")!=null?item.get("TableName").toString():"";
            String TableField=item.get("TableField")!=null?item.get("TableField").toString():"";
            String TreeLevel=item.get("TreeLevel")!=null?item.get("TreeLevel").toString():"";
            Integer IsNav=item.get("IsNav")!=null?Integer.parseInt(item.get("IsNav").toString()):0;
            String sql = String.format("insert into searchcategory2(CategoryName,TreeLevel,IsLeaf,TableName,TableCode,TableField,IsNav)" +
                    "values('%s','%s','%s','%s','%s','%s','%s')",CategoryName,TreeLevel,IsLeaf,TableName,TableCode,TableField,IsNav);
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update( new PreparedStatementCreator(){
                                     @Override
                                     public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
                                         PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                                         return ps;
                                     }
                                 },
                    keyHolder);
            Integer id=keyHolder.getKey().intValue();
            String code=newparentCode+String.format("%03d", id);
            String updateSql=String.format("update searchcategory2 set CategoryCode='%s',ParentID='%s',ParentCode='%s' where CategoryID=%s", String.format("%03d", id),newparentId,code,id);
            jdbcTemplate.execute(updateSql);
            getTree( pid,id.toString(),code,nodelList);
        }
    }
    private  void insertCategory(String parentId){
        String sqlchild = String.format("SELECT *  FROM searchcategory  where ParentID='%s' ",parentId);
        List<Map<String, Object>> resultChild=jdbcTemplate.queryForList(sqlchild);
        if(resultChild!=null&&resultChild.size()>0){
            for (Map<String, Object> item:resultChild){
                String CategoryName=item.get("CategoryName")!=null?item.get("CategoryName").toString():"";
                String IsLeaf=item.get("IsLeaf")!=null?item.get("IsLeaf").toString():"";
                String TableCode=item.get("TableCode")!=null?item.get("TableCode").toString():"";
                String TableName=item.get("TableName")!=null?item.get("TableName").toString():"";
                String TableField=item.get("TableField")!=null?item.get("TableField").toString():"";
                String TreeLevel=item.get("TreeLevel")!=null?item.get("TreeLevel").toString():"";
                String sql = String.format("insert into searchcategory2(CategoryName,ParentID,TreeLevel,IsLeaf,TableName,TableCode,TableField)" +
                        "values('%s','%s','%s','%s','%s','%s','%s')",CategoryName,parentId,TreeLevel,IsLeaf,TableName,TableCode,TableField);
                KeyHolder keyHolder = new GeneratedKeyHolder();
                jdbcTemplate.update( new PreparedStatementCreator(){
                                         @Override
                                         public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
                                             PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                                             return ps;
                                         }
                                     },
                        keyHolder);
                Integer id=keyHolder.getKey().intValue();
                String code=parentId+String.format("%03d", id);
                String updateSql=String.format("update searchcategory2 set ParentID='%s',ParentCode='%s'", parentId,code);
                jdbcTemplate.execute(updateSql);
                insertCategory(id.toString());
            }
        }
    }
    @Test
    public void importKG() {
        String domain="zhctwh";
        String sqlcount = "SELECT count(*)  FROM WHBT_CLS ";
        Integer totalCount =jdbcTemplate.queryForObject(sqlcount,Integer.class);
        Integer y=totalCount%500;
        Integer totalPage=y>0?totalCount/500+1:totalCount/500;
        for(Integer i=0;i<=totalPage;i++){
            System.out.println("i第"+i+"页");
            String sql = String.format("SELECT *  FROM WHBT_CLS  limit %s,500",i*500);
            List<Map<String, Object>> result=jdbcTemplate.queryForList(sql);
            for (Map<String, Object> record:result){
                String parentCode=record.get("SYS_FLD_CLASS_CODE").toString();
                String sqlchild = String.format("SELECT *  FROM WHBT_CLS  where SYS_FLD_PARENT_CODE='%s' and  SYS_FLD_PARENT_CODE!=SYS_FLD_CLASS_CODE",parentCode);
                List<Map<String, Object>> resultChild=jdbcTemplate.queryForList(sqlchild);
                for (Map<String, Object> item:resultChild){
                    String childCode=item.get("SYS_FLD_CLASS_CODE").toString();
                    String cypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE n.code='%s' AND m.code = '%s' "
                            + "merge (n)-[r:RE{name:'%s',sourcecode:'%s',targetcode:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), parentCode, childCode, "",parentCode,childCode);
                    neo4jUtil.excuteCypherSql(cypherSql);
                }

            }
        }
        System.out.println("success");
    }
    @Test
    public void getRecordGroup() {
        String domain="zhctwh";
        List<String> nodeArray=new ArrayList<>();
        nodeArray.add("218");
        HashMap<String, Object> nsModel = new HashMap<String, Object>();
        String cypher = String.format("with [ '218'] as name_list\n" +
                "  match (nd:`%s`) where nd.code in name_list\n" +
                "  with collect(nd) as nodes\n" +
                "  unwind nodes as n\n" +
                "  match (n)-[r*..2]->(m:`%s`) " +
                "  with n,r,m\n" +
                "  return n,r,m limit 1000",  domain, domain);
        // nsModel = neo4jUtil.GetGraphNodeAndShip(cypher);
        StatementResult rs=neo4jUtil.excuteCypherSql(cypher);
        List<Map<String,Object>> nodes=new ArrayList<>();
        List<Map<String,Object>> ships=new ArrayList<>();
        for (Record r:rs.list()) {
            Value sourceNode = r.get("m");
            Value targetNode = r.get("n");
            nodes.add(sourceNode.asMap());
            nodes.add(targetNode.asMap());
            Value ship = r.get("r");
            if(ship!=null&&ship.asList().size()>0){
                for(Object s:ship.asList()){
                    Map<String, Object> map = ((Relationship)s).asMap();
                    ships.add(map);
                }
            }

        }
        nsModel.put("node", nodes);
        nsModel.put("relationship", ships);
        Map<String, Object> groups = new HashMap<>();
        if (nsModel != null && nsModel.get("node") != null) {
            List<Map<String, Object>> nodeList = (List<Map<String, Object>>) nsModel.get("node");
            List<Map<String, Object>> nodeShips = (List<Map<String, Object>>) nsModel.get("relationship");
            //没有关系的情况下考虑补一个上位导航，从word的导航类别属性里找
            for (String entityCode : nodeArray) {
                List<String> firstNavCodeList=nodeShips.stream().filter(n->n.get("sourcecode").equals(entityCode)).map(n->n.get("targetcode").toString()).distinct().collect(Collectors.toList());
                if(firstNavCodeList!=null&&firstNavCodeList.size()>0){
                    for (String firstNavCode : firstNavCodeList) {
                        //第一级导航节点
                        List<Map<String, Object>> firstNavModelList = nodeList.stream().filter(n->n.get("code").equals(firstNavCode)).distinct().collect(Collectors.toList());
                        if(firstNavModelList!=null&&firstNavModelList.size()>0){
                            for (Map<String, Object> firstNavModel : firstNavModelList) {
                                //找到二级节点
                                String firstSourceCode=firstNavModel.get("code").toString();
                                String firstSourceName=firstNavModel.get("name").toString();
                                List<Map<String, Object>> firstChildList=new ArrayList<>();
                                List<String> secondNavCodeList=nodeShips.stream().filter(n->n.get("sourcecode").toString().equals(firstSourceCode)).map(n->n.get("targetcode").toString()).distinct().collect(Collectors.toList());
                                if(secondNavCodeList!=null&&secondNavCodeList.size()>0){
                                    for (String secondNavCode : secondNavCodeList) {
                                        //第二级导航节点
                                        List<Map<String, Object>> secondNavModelList = nodeList.stream().filter(n->n.get("code").equals(secondNavCode)).map(n->coverToGroupMap(n)).collect(Collectors.toList());
                                        if(secondNavModelList!=null&&secondNavModelList.size()>0){
                                            firstChildList.addAll(secondNavModelList);
                                        }
                                    }
                                }
                                groups.put("name",firstSourceName);
                                groups.put("code",firstSourceCode);
                                groups.put("chlid",firstChildList);
                            }
                        }
                    }
                }
            }
        }
        System.out.println(JSON.toJSON(groups));
    }
    private Map<String, Object> coverToGroupMap(Map<String, Object> obj) {
        if (obj == null || obj.size() == 0) return null;
        Map<String, Object> mp = new HashMap<>();
        mp.put("code", obj.get("code"));
        mp.put("name", obj.get("name"));
        return mp;
    }
    @Test
    public void addDic2() {
        String sqlcount = "SELECT count(*)  FROM WHBT_CLS ";
        Integer totalCount =jdbcTemplate.queryForObject(sqlcount,Integer.class);
        Integer y=totalCount%500;
        Integer totalPage=y>0?totalCount/500+1:totalCount/500;
        for(Integer i=0;i<=totalPage;i++){
            System.out.println("i第"+i+"页");
            String sql = String.format("SELECT *  FROM WHBT_CLS  limit %s,500",i*500);
            List<Map<String, Object>> result=jdbcTemplate.queryForList(sql);
            for (Map<String, Object> record:result){
                String mainword=record.get("SYS_FLD_CLASS_NAME").toString();
                String sqltongyi = String.format("SELECT *  FROM 同义词  where 主题词='%s' ",mainword);
                List<Map<String, Object>> resultTongyi=jdbcTemplate.queryForList(sqltongyi);
                String t="";
                String b="";
                if(resultTongyi!=null&&resultTongyi.size()>0){
                    t=resultTongyi.get(0).get("等同词").toString();
                }
                String sqlbaohan = String.format("SELECT *  FROM 包含关系  where name='%s' ",mainword);
                List<Map<String, Object>> resultbaohan=jdbcTemplate.queryForList(sqlbaohan);
                if(resultbaohan!=null&&resultbaohan.size()>0){
                    b=resultbaohan.get(0).get("word").toString();
                }
                String sqlInsert=String.format("insert into subject_word_item (ID,DICID,CONTENT,RELATIONCONTENT,SYNONYMCONTENT,CREATEUSER,CREATEDATE,STATE) " +
                        "values('%s','%s','%s','%s','%s','%s','%s','%s')"
                        ,UUID.randomUUID(),"DE8FFA45-D988-44CF-ACC2-5A20BFCC489E",mainword,b,t,"sa","2019-09-18 10:45:02",1);
                jdbcTemplate.execute(sqlInsert);
            }
        }
        System.out.println("success");

    }
    @Test
    public void initsearchdata() {

        String filePath = "D:\\项目\\造价监督\\造价监督系统数据梳理0806.xlsx";
        File file = new File(filePath);
        String fileName = file.getName();
        InputStream in = null;
        try {
            in = new FileInputStream(file);
            Workbook workbook = null;
            if (ExcelUtil.isExcel2007(fileName)) {
                workbook = new XSSFWorkbook(in);
            } else {
                workbook = new HSSFWorkbook(in);
            }
            // 有多少个sheet
            int sheets = workbook.getNumberOfSheets();
            for (int i = 0; i < sheets; i++) {
                Sheet sheet = workbook.getSheetAt(i);
                String tableName=sheet.getSheetName();
                String tableCode="";
                Cell tnameCell = sheet.getRow(0).getCell(0);
                String firstCellStr=tnameCell.getStringCellValue();
                if(firstCellStr.contains("(")&&firstCellStr.contains(")")){
                    int leftIndex=firstCellStr.indexOf("(");
                    int rightIndex=firstCellStr.indexOf(")");
                    tableCode=firstCellStr.substring(leftIndex,rightIndex);
                }
                Map<String,Integer> cloumnItem=new HashMap<>();
                Row headerRow = sheet.getRow(0);
                for (int columnNum=0;i< headerRow.getPhysicalNumberOfCells();i++){
                    Cell cell= headerRow.getCell(columnNum);
                    if(cell!=null&&(cell.getCellType()!=Cell.CELL_TYPE_BLANK)){
                        String columnName=cell.getStringCellValue();
                        cloumnItem.put(columnName,columnNum);
                    }
                }

                int rowSize = sheet.getPhysicalNumberOfRows();
                for (int j = 0; j < rowSize; j++) {
                    if(j<=1) continue;//过滤头两行
                    Row row = sheet.getRow(j);



                }
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }


    }
    private FiledItem getCellValue(Row row, Map<String,Integer> cellItem){
        FiledItem item=new FiledItem();
        if(row!=null&&cellItem.size()>0){
            for (String fieldName:cellItem.keySet()){
                Integer index=cellItem.get(fieldName);
                if(index!=null&&index>-1){
                    row.getCell(index).setCellType(Cell.CELL_TYPE_STRING);
                    Cell cell = row.getCell(index);
                    String cellValue = cell.getStringCellValue();
                    if(fieldName.equals("字段名称")){
                        item.setFieldName(cellValue);
                    }else if(fieldName.equals("中文字段名")){
                        item.setFieldAliaName(cellValue);
                    }
                    else if(fieldName.equals("是否唯一标识字段")){
                        boolean isPk=cellValue.equals("是");
                        item.setPk(isPk);
                    }
                    else if(fieldName.equals("是否作为属性融入图谱")){
                        boolean isKg=cellValue.equals("是");
                        item.setInKG(isKg);
                    }
                    else if(fieldName.equals("字段标记")){

                    }
                    else if(fieldName.equals("是否概览字段")){
                        boolean isOverView=cellValue.equals("是");
                        item.setOverView(isOverView);
                    }
                    else if(fieldName.equals("是否细览字段")){
                        boolean isDetaiOverView=cellValue.equals("是");
                        item.setDetailView(isDetaiOverView);
                    }
                }
            }

        }
        return  item;
    }
}
