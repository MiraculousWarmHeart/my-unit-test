package cnki.kg.demo;

import cnki.kg.demo.util.Neo4jUtil;
import cnki.kg.demo.util.SqlserverUtils;
import cnki.kg.demo.util.StringUtil;
import org.junit.jupiter.api.Test;

import org.neo4j.driver.v1.StatementResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@SpringBootTest
@WebAppConfiguration
public class TestCreateGraph2 {
    @Autowired
    Neo4jUtil neo4jUtil;
    @Autowired
    SqlserverUtils utils;
    @Autowired
    @Qualifier("mysqlJdbcTemplate")
    protected JdbcTemplate mysqlJdbcTemplate;

    @Autowired
    @Qualifier("sqlserverJdbcTemplate")
    protected JdbcTemplate sqlserverJdbcTemplate;
    @Test
    public void create() {
        //创建根节点
        String domain="交通业务图谱2";
        String cypherSqlroot = String.format("merge (n:`%s`{name:'%s',prefix:'%s'}) return n", domain, "项目","");
        List<HashMap<String, Object>>  rootnodes=neo4jUtil.GetGraphNode(cypherSqlroot);
        for (HashMap<String, Object> rootnode : rootnodes) {
            String rootId=rootnode.get("uuid").toString();
            String rootName=rootnode.get("name").toString();
            String[] level1data=new String[]{"项目业主","投资模式","建设状态"};
            for (String litem : level1data) {
                HashMap<String, Object> nodeItem= createNL(domain,litem,rootId,rootName);
                if(litem.equals("项目业主")){
                    //创建业主数据
                    createXMYZ(domain, nodeItem.get("uuid").toString(),litem);
                }
                if(litem.equals("投资模式")){
                    createTZMS(domain, nodeItem.get("uuid").toString(),litem);
                }
                if(litem.equals("建设状态")){
                    createJSZT(domain, nodeItem.get("uuid").toString(),litem);
                }
            }
        }
    }
    public void createTZMS(String domain,String pid,String pname){
        //获取所有的项目业主
        String[] TZMS=new String[]{"BOT","PPP"};
        for (String m : TZMS) {
            HashMap<String, Object> nodeItem= createNL(domain,m,pid,pname);
            //创建投资模式为xxx的项目
           String tzmssql=String.format("SELECT XMJC from XMJBXX WHERE TZMS='%s'",m);
            List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(tzmssql);
            if(mapList==null||mapList.size()==0) return;
            List<String> xmjcs=mapList.stream().map(n->n.get("XMJC").toString()).collect(Collectors.toList());
            for (String xmjc : xmjcs) {
                linkNode(domain, xmjc, m, nodeItem.get("uuid").toString());
            }
        }
    }
    public void createJSZT(String domain,String pid,String pname){
        //获取所有的项目业主
        String[] TZMS=new String[]{"交工","在建"};
        for (String m : TZMS) {
            HashMap<String, Object> nodeItem= createNL(domain,m,pid,pname);
            //创建投资模式为xxx的项目
            String tzmssql=String.format("SELECT XMJC from XMJBXX WHERE JSZT='%s'",m);
            List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(tzmssql);
            if(mapList==null||mapList.size()==0) return;
            List<String> xmjcs=mapList.stream().map(n->n.get("XMJC").toString()).collect(Collectors.toList());
            for (String xmjc : xmjcs) {
                linkNode(domain, xmjc, m, nodeItem.get("uuid").toString());
            }
        }
    }
    public void createXMYZ(String domain,String pid,String linkName){
        //获取所有的项目业主
        String[] xmyzs=new String[]{"贵州高速公路集团有限公司","贵州江习古高速公路开发有限公司","贵州交通建设集团有限公司", "贵州省公路局高速公路建设办公室","中交集团","中铁建集团"};
        for (String xmyz : xmyzs) {
            HashMap<String, Object> nodeItem= createNL(domain,xmyz,pid,linkName);
            //创建业主负责的项目
            createYZXM(domain,nodeItem.get("uuid").toString(),"项目",xmyz);
            //todo 创建项目业主的企业信息
        }
    }
    public void createYZXM(String domain,String pid,String pname,String xmyz){
        //获取业主负责的项目
        String sqlBdmc = String.format("select DISTINCT(XMJC) as XMJC from xmjbxx where XMYZ='%s' ",xmyz);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        List<String> xmjcs=mapList.stream().map(n->n.get("XMJC").toString()).collect(Collectors.toList());
        for (String xmjc : xmjcs) {
            HashMap<String, Object> nodeItem= createNL(domain,xmjc,pid,pname);
            //创建路线基本信息
            createRoterInformation(domain,nodeItem.get("uuid").toString(),xmjc);
        }
    }
    public void createRoterInformation(String domain,String pid,String xmjc) {
        String [] items=new String[]{"建设状态", "工可批复金额","投资模式","标段","桥梁","隧道","基本信息","规划设计","招投标","实施","变更","运营","管理"};
        String xmxxsql=String.format("SELECT JSZT as 建设状态,GKGSPFJE as 工可批复金额,TZMS as 投资模式 from XMJBXX WHERE XMJC='%s'",xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(xmxxsql);
        if(mapList==null||mapList.size()==0) return;
        Map<String, Object> xmjbxxModel=mapList.get(0);
        for (String item : items) {
            if(item.equals("建设状态")||item.equals("工可批复金额")||item.equals("投资模式")){
                String nname=xmjbxxModel.get(item)!=null?xmjbxxModel.get(item).toString():"";
                createNL(domain,nname,pid,item);
            }
            else if(item.equals("标段")){
                //创建标段基本信息
                HashMap<String, Object> node= createNL(domain,item,pid,item);
                createBiaoduanInformation(domain,node.get("uuid").toString(),item,xmjc);
            }
            else if(item.equals("桥梁")){
                //创建桥梁信息
                HashMap<String, Object> node= createNL(domain,item,pid,item);
                createQiaoliangInformation(domain,node.get("uuid").toString(),item,xmjc);
            }
            else if(item.equals("隧道")){
                //创建隧道信息
                HashMap<String, Object> node= createNL(domain,item,pid,item);
                createSuiDaoInformation(domain,node.get("uuid").toString(),item,xmjc);
            }
            else if(item.equals("基本信息")){
                HashMap<String, Object> node= createNL(domain,item,pid,item);
                createBaseInformation(domain,node.get("uuid").toString(),item,xmjc);
            }
            else if(item.equals("规划设计")){
                HashMap<String, Object> node= createNL(domain,item,pid,item);
                createGuiHuaShejiInformation(domain,node.get("uuid").toString(),item,xmjc);
            }
            else if(item.equals("招投标")){
                HashMap<String, Object> node= createNL(domain,item,pid,item);
                createZhaotoubiaoInformation(domain,node.get("uuid").toString(),item,xmjc);
            }
            else if(item.equals("变更")){
                HashMap<String, Object> node= createNL(domain,item,pid,pid);
                createBianGengInformation(domain,node.get("uuid").toString(),item,xmjc);
            }
            else if(item.equals("实施")){
                HashMap<String, Object> node= createNL(domain,item,pid,item);
            }
            else if(item.equals("运营")){
                HashMap<String, Object> node= createNL(domain,item,pid,item);
            }
            else if(item.equals("管理")){
                HashMap<String, Object> node= createNL(domain,item,pid,item);
            }
        }
    }
    public void createBiaoduanInformation(String domain,String pid,String linkname,String xmjc) {
        String sqlBdmc = String.format("select DISTINCT(BDBH) as BDBH from BDJBXX where XMJC='%s'",xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        List<String> items=mapList.stream().map(n->n.get("BDBH").toString()).collect(Collectors.toList());
        for (String bdbh : items) {
            HashMap<String, Object> node= createNL(domain,bdbh,pid,linkname);
            createBiaoduanDetail(domain,node.get("uuid").toString(),xmjc,bdbh);
        }
    }
    public void createBiaoduanDetail(String domain,String pid,String xmjc,String bdbh) {
        String bdsql=String.format("SELECT XMJC,BDBH,JH_KGRQ as 计划开工日期,JH_WGRQ as 计划完工日期,QY_HTJE as 合同金额,SGDW as 施工单位,JFDW as 甲方单位,YFDW as 乙方单位, JLDW as 监理单位 " +
                "from BDJBXX WHERE BDBH='%s' and XMJC='%s'",bdbh,xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(bdsql);
        if(mapList==null||mapList.size()==0) return;
        Map<String, Object> bditem=mapList.get(0);
        String XMsql=String.format("SELECT XMJC,XMMC from xmjbxx WHERE XMJC='%s' ",xmjc);
        List<Map<String, Object>> XmmapList = sqlserverJdbcTemplate.queryForList(XMsql);
        if(XmmapList==null||XmmapList.size()==0) return;
        Map<String, Object> xmitem=XmmapList.get(0);
        String xmmc=xmitem.get("XMMC").toString();
        String[] items=new String[]{"计划开工日期","计划完工日期","合同金额","施工单位","甲方单位","乙方单位","监理单位","变更","本期未完成金额","标段基本信息"};
        for (String item : items) {
            String linkName=item;
            //HashMap<String, Object> node= createNodeAndLink(domain,item,prefix,parentId);
            if(item.equals("变更")){
                createBiaoduanBiangengInfo(domain,pid,item,xmmc,bdbh);
            }
            else if(item.equals("本期未完成金额")){
                createBenqiweiwanchengjinge(domain,pid,item,xmmc,bdbh);
            }
            else if(item.equals("标段基本信息")){
                createBiaoduanBaseInfo(domain,pid,item,xmjc,bdbh);
            }
            else if(item.equals("甲方单位")||item.equals("乙方单位")||item.equals("施工单位")||item.equals("监理单位")){//甲方单位，乙方单位，施工单位，监理单位
                String dw=bditem.get(item)!=null?bditem.get(item).toString():"";
                if(StringUtil.isNotBlank(dw)) {
                    HashMap<String, Object> enode= createNL(domain,dw,pid,item);
                    String field="";
                    if(item.equals("甲方单位")){
                        field="JFDW";
                    }
                    if(item.equals("乙方单位")){
                        field="YFDW";
                    }
                    if(item.equals("施工单位")){
                        field="SGDW";
                    }
                    if(item.equals("监理单位")){
                        field="JLDW";
                    }
                    if(StringUtil.isNotBlank(field)){
                        //参与的项目，直接和项目连线
                        createXMXX(domain,field,enode.get("uuid").toString(),enode.get("name").toString(),dw);
                        //todo 企业信息 暂无数据
                    }
                }

            }
        }
    }
    public void createQiaoliangInformation(String domain,String pid,String pname,String xmjc) {
        String sqlBdmc = String.format("select * from XMJBXX where XMJC='%s'",xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        Map<String, Object> item=mapList.get(0);
        String[] nodes=new String []{"桥梁总长度","桥梁总数","涵洞道数","特大桥长度","特大桥数量"
                ,"大桥长度","大桥数量","小桥长度","小桥数量","中桥长度","中桥数量"};
        for (String node : nodes) {
            String nodename=item.get(node)!=null?item.get(node).toString():"";
            if(StringUtil.isNotBlank(nodename)){
                createNL(domain,nodename,pid,node);
            }
        }
    }
    public void createSuiDaoInformation(String domain,String pid,String pname,String xmjc) {
        String sqlBdmc = String.format("select * from XMJBXX where XMJC='%s'",xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        Map<String, Object> item=mapList.get(0);
        String[] nodes=new String []{"隧道总长度","隧道总数","特长隧道长度","特长隧道数量"
                ,"长隧道长度","长隧道数量","中隧道长度","中隧道数量","短隧道长度","短隧道数量","桥隧比"};
        for (String node : nodes) {
            String nodename=item.get(node)!=null?item.get(node).toString():"";
            if(StringUtil.isNotBlank(nodename)){
                createNL(domain,nodename,pid,node);
            }

        }
    }
    public void createBaseInformation(String domain,String pid,String pname,String xmjc) {
        String sqlBdmc = String.format("SELECT XMMC as 项目名称,起点,终点,途经,CD as 车道,SJSU as 设计速度,ZTLJ as 整体路基,ZHXJ as 载荷新建,ZXQC as 主线全长,ZXLMJGLX as 主线路面结构类型,PZKGRQ as 批准开工日期,SJKGRQ as 实际开工日期 FROM XMJBXX where XMJC='%s'",xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        Map<String, Object> item=mapList.get(0);
        String[] nodes=new String []{"项目名称","起点","终点","途经"
                ,"车道","设计速度","整体路基","载荷新建","主线全长","主线路面结构类型","批准开工日期","实际开工日期"};
        for (String node : nodes) {
            String nodename=item.get(node)!=null?item.get(node).toString():"0";
            if(StringUtil.isNotBlank(nodename)){
                createNL(domain,nodename,pid,node);
            }
        }
    }
    public void createGuiHuaShejiInformation(String domain,String pid,String pname,String xmjc) {
        String sqlBdmc = String.format("SELECT TZGS as 投资估算,ZBJ as 资本金,ZZZBL as 资本金占总投资比例 ,YHDK as 银行贷款,YZZBL as 银行贷款占总投资比例," +
                "GKGSPFJE as 工可批复金额,GSPFDW as 概算批复单位,GSPFWH as 概算批复文号,GSGSPFJE as 概算批复金额,SJDW as 设计单位,XMYZ as 项目业主 FROM XMJBXX where XMJC='%s'",xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        Map<String, Object> item=mapList.get(0);
        String[] nodes=new String []{"投资估算","资本金","资本金占总投资比例","银行贷款"
                ,"银行贷款占总投资比例","工可批复金额","概算批复单位","概算批复文号","概算批复金额","设计单位","项目业主"};
        for (String node : nodes) {
            String nodename=item.get(node)!=null?item.get(node).toString():"";
            if(StringUtil.isNotBlank(nodename)){
                createNL(domain,nodename,pid,node);
            }
        }
    }
    public void createZhaotoubiaoInformation(String domain,String pid,String pname,String xmjc) {
        String sqlBdmc = String.format("SELECT distinct(HTH) as 合同号 FROM ZTBJDSJ where XMJC='%s' GROUP BY HTH ",xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        for (Map<String, Object> om : mapList) {
            String hth=om.get("合同号")!=null?om.get("合同号").toString():"";
            HashMap<String, Object> htModel= createNL(domain,hth,pid,"合同号");
            String[] nodes=new String []{"合同名称","甲方单位","乙方单位","签约合同金额","清单金额"};
            String htdetail = String.format("SELECT top 1 HTH as 合同号,HTMC as 合同名称 ,JFDW as 甲方单位,YFDW as 乙方单位,QY_HTJE as 签约合同金额,HTLX as 合同类型 FROM ZTBJDSJ where XMJC='%s' and hth='%s' ",xmjc,hth);
            List<Map<String, Object>> htdetailMap = sqlserverJdbcTemplate.queryForList(sqlBdmc);
            if(htdetailMap==null||mapList.size()==0) return;
            Map<String, Object> htmap=htdetailMap.get(0);
            for (String node : nodes) {
                if(node.equals("清单金额")){
                    String htjesql=String.format("SELECT HTH,SUM(QDJE) as 清单金额 FROM ZTBJDSJ where XMJC='%s' and hth='%s'  GROUP BY HTH",xmjc,hth);
                    List<Map<String, Object>> htjeMap = sqlserverJdbcTemplate.queryForList(sqlBdmc);
                    if(htjeMap==null||htjeMap.size()==0) return;
                    Map<String, Object> htjeModel=htjeMap.get(0);
                    String nodeVal=htmap.get("清单金额")!=null?htmap.get("清单金额").toString():"";
                    if(StringUtil.isNotBlank(nodeVal)){
                        createNL(domain,nodeVal,htModel.get("uuid").toString(),node);
                    }
                }else{
                    String nodeVal=htmap.get(node)!=null?htmap.get(node).toString():"";
                    if(StringUtil.isNotBlank(nodeVal)){
                        createNL(domain,nodeVal,htModel.get("uuid").toString(),node);
                    }
                }
            }
        }
    }
    public void createBianGengInformation(String domain,String pid,String pname,String xmjc) {
        String sqlBdmc = String.format("SELECT XMMC as 项目名称,XMJC as 项目简称,BGZFY as 变更总费用,XMYZ as 项目业主 from XMBGHZ where XMJC='%s'",xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        String[] nodes=new String []{"项目名称","变更总费用","项目业主","项目简称"};
        Map<String, Object> item=mapList.get(0);
        for (String node : nodes) {
            if(node.equals("项目简称")){
                linkNode(domain,node,pname,pid);
            }else{
                String nodename=item.get(node)!=null?item.get(node).toString():"";
                if(StringUtil.isNotBlank(nodename)){
                    createNL(domain,nodename,pid,node);
                }
            }
        }
    }
    private HashMap<String, Object> createNL(String domain,String nodeName,String pid,String linkName){
        String cypherSql = String.format("merge (n:`%s`{name:'%s',pid:'%s'}) return n", domain, nodeName,pid);
        List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
        HashMap<String, Object> nodeql=nodesQl.get(0);
        String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND id(m) = %s "
                + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), pid, nodeql.get("uuid"), linkName);
        neo4jUtil.excuteCypherSql(LinkcypherSql);
        return nodeql;
    }
    private HashMap<String, Object> createNodeAndLink(String domain,String nodeName,String prefix,String pid){
        String cypherSql = String.format("merge (n:`%s`{name:'%s',prefix:'%s',pid:'%s'}) return n", domain, nodeName,prefix,pid);
        List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
        HashMap<String, Object> nodeql=nodesQl.get(0);
        String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND id(m) = %s "
                + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), pid, nodeql.get("uuid"), "");
        neo4jUtil.excuteCypherSql(LinkcypherSql);
        return nodeql;
    }
    private HashMap<String, Object> linkNode(String domain,String nodeName,String prefix,String pid){
        HashMap<String, Object> nodeql=new HashMap<>();
        String cypherSql = String.format("match (n:`%s`) where n.name='%s' return n", domain, nodeName);
        List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
        if(nodesQl!=null&&nodesQl.size()>0) {
            nodeql=nodesQl.get(0);
            String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND id(m) = %s "
                    + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), pid, nodeql.get("uuid"), "");
            neo4jUtil.excuteCypherSql(LinkcypherSql);
        }
        return nodeql;
    }

    public void createXMXX(String domain,String field,String parentId,String prefix,String dwmc) {
        //获取施工、监理、甲方、乙方单位为xxx单位的项目列表
        String sqlBdmc = String.format("select DISTINCT(XMJC) as XMJC from BDJBXX where %s='%s' ",field,dwmc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        List<String> items=mapList.stream().map(n->n.get("XMJC").toString()).collect(Collectors.toList());
        //创建单位参与项目信息
        for (String item : items) {
            String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND m.name = '%s' "
                    + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), parentId, item, "相关项目");
            neo4jUtil.excuteCypherSql(LinkcypherSql);
        }
    }

    /**
     * 创建标段基本信息 起点桩号 终点桩号 合同段长度 合同工期 合同金额
     * @param domain
     * @param parentId
     * @param prefix
     * @param xmjc
     * @param bdbh
     */
    public void createBiaoduanBaseInfo(String domain,String parentId,String prefix,String xmjc,String bdbh) {
        String sqlBdmc = String.format("SELECT QDZH as 起点桩号,ZDZH as 终点桩号 ,HTDCD as 合同段长度,HTGQ as 合同工期,QY_HTJE as 合同金额 " +
                "from BDJBXX where BDBH='%s' AND XMMC='%s' ",bdbh,xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        Map<String, Object> biaoDuanItem=mapList.get(0);
        String[] items=new String[]{"起点桩号","终点桩号","合同段长度","合同工期","合同金额"};
        for (String item : items) {
            String nodeValue=biaoDuanItem.get(item)!=null?biaoDuanItem.get(item).toString():"";
            if(StringUtil.isNotBlank(nodeValue)){
                createNL(domain, nodeValue, parentId,item );
            }
        }

    }
    public void createBiaoduanBiangengInfo(String domain,String parentId,String prefix,String xmmc,String bdbh) {
        //变更总费用
        String sqlBgzfy = String.format("SELECT BGZFY as 变更总费用 from BDBGHZ where BDBH='%s' AND XMMC='%s' ",bdbh,xmmc);
        List<Map<String, Object>> BgzfymapList = sqlserverJdbcTemplate.queryForList(sqlBgzfy);
        if(BgzfymapList==null||BgzfymapList.size()==0) return;
        String bgzfyvlaue=BgzfymapList.get(0).get("变更总费用").toString();
        HashMap<String, Object> nodeBgzfy = createNL(domain, bgzfyvlaue, parentId, "变更总费用");
        //变更令
        //HashMap<String, Object> node = createNodeAndLink(domain, "变更令", prefix, parentId);
        String sqlBdlbh = String.format("SELECT DISTINCT(BGLBH) as 变更令 from BGMXB where BDBH='%s' AND XMMC='%s' ",bdbh,xmmc);
        List<Map<String, Object>> bglbhmapList = sqlserverJdbcTemplate.queryForList(sqlBdlbh);
        if(bglbhmapList==null||bglbhmapList.size()==0) return;
        String[] bgl=new String[]{"变更数量","变更费用","变更令","工程部位","清单名称","变更申请号","变更理由"};
        for (Map<String, Object> bglbhItem : bglbhmapList){
            String bglNo=bglbhItem.get("变更令").toString();
            HashMap<String, Object> realGengling = createNL(domain, bglNo, parentId, "变更令");
            /*String sqlBdl = String.format("SELECT BGSL as 变更数量,BGFY as 变更费用,BGLBH as 变更令,GCBW as 工程部位,QDMC as 清单名称,BGSQH as 变更申请号,BGLY as 变更理由 from BGMXB where BDBH='%s' AND XMMC='%s' and BGLBH='%s'",bdbh,xmmc,bglNo);
            List<Map<String, Object>> bglmapList = sqlserverJdbcTemplate.queryForList(sqlBdl);
            if(bglmapList==null||bglmapList.size()==0) return;
            for (Map<String, Object> bglItem : bglmapList){
                for (String s : bgl) {
                    HashMap<String, Object> realProp =createNL(domain, s, realGengling.get("uuid").toString(),s);
                    if(bglItem.get(s)==null)continue;
                    String bglGcbw=bglItem.get(s).toString();
                    createNL(domain, bglGcbw, realProp.get("uuid").toString(), s);
                }

            }*/
        }
    }

    /**
     * 本期未完成金额
     * @param domain
     * @param parentId
     * @param prefix
     * @param xmjc
     * @param bdbh
     */
    public void createBenqiweiwanchengjinge(String domain,String parentId,String prefix,String xmjc,String bdbh) {
        //变更总费用
        HashMap<String, Object> qici = createNL(domain, prefix, parentId, prefix);
        String sql=String.format("SELECT DISTINCT(JLQC) as 期次 from SSJDSJ where  BDBH='%s' AND XMJC='%s' ",bdbh,xmjc);
        List<Map<String, Object>> qicimapList = sqlserverJdbcTemplate.queryForList(sql);
        if(qicimapList==null||qicimapList.size()==0) return;
        for (Map<String, Object> qiciMap : qicimapList) {
            String qiciValue=qiciMap.get("期次").toString();
            HashMap<String, Object> qiciNode = createNL(domain, qiciValue, qici.get("uuid").toString(), "期次");
            String sqlBgzfy = String.format("SELECT SUM(BQM_JE) as  金额  from SSJDSJ where BDBH='%s' AND XMJC='%s' and JLQC='%s'  ",bdbh,xmjc,qiciValue);
            List<Map<String, Object>> BgzfymapList = sqlserverJdbcTemplate.queryForList(sqlBgzfy);
            if(BgzfymapList==null||BgzfymapList.size()==0) return;
            for (Map<String, Object> bglItem : BgzfymapList){
                String je=bglItem.get("金额").toString();
                createNL(domain, je, qiciNode.get("uuid").toString(), "金额");
            }
        }
    }

    @Test
    public void delete() {
        String [] xmjcs=new String[]{"六六线", "毕生线", "毕都线", "余凯线", "遵贵扩容", "惠罗线", "尖小线", "凯羊线", "凯雷线", "织纳线", "望安线", "清织线", "六威线", "罗望线", "麻驾线", "都安高速"};
        String domain="贵州交通全网检索数据图谱";
        for (String xmjc : xmjcs) {
            String cypherSql = String.format("match (n:`%s`) where n.name='%s' detach delete n", domain, xmjc);
            neo4jUtil.excuteCypherSql(cypherSql);
        }

    }
    @Test
    public void deleter() {
        String [] xmjcs=new String[]{"六六线", "毕生线", "毕都线", "余凯线", "遵贵扩容", "惠罗线", "尖小线", "凯羊线", "凯雷线", "织纳线", "望安线", "清织线", "六威线", "罗望线", "麻驾线", "都安高速"};
        String domain="贵州交通全网检索数据图谱";
        for (String xmjc : xmjcs) {
            String cypherSql = String.format("match (n:`%s`)-[r]->(m) where m.name='%s' return id(r) as uuid", domain, xmjc);
            StatementResult res=neo4jUtil.excuteCypherSql(cypherSql);
            if(res.hasNext()){
               Integer uuid= res.list().stream().map(n->Integer.parseInt(n.get("uuid").toString())).max(Integer::max).get();
                System.out.println(uuid);
                String cypherSql2 = String.format("match (n:`%s`)-[r]->(m)  where id(r)=%s detach delete r", domain, uuid);
                neo4jUtil.excuteCypherSql(cypherSql2);
            }
        }

    }
    @Test
    public void testalia(){
        String sqlBdmc = String.format("SELECT XMMC as 项目名称,起点,终点,途经,CD as 车道,SJSU as 设计速度,ZTLJ as 整体路基,ZHXJ as 载荷新建,ZXQC as 主线全长,ZXLMJGLX as 主线路面结构类型,PZKGRQ as 批准开工日期,SJKGRQ as 实际开工日期 FROM XMJBXX where XMJC='%s'","务正线");
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        System.out.println("s");
    }

    @Test
    public void deleteall() {
        String cypherSql ="match (n:`贵州交通全网检索数据图谱`)-[r]-(m) detach delete n,r,m";
        neo4jUtil.excuteCypherSql(cypherSql);

    }
}
