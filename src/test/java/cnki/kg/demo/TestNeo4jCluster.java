package cnki.kg.demo;

import cnki.kg.demo.util.Neo4jUtil;
import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.List;


@SpringBootTest
@WebAppConfiguration
public class TestNeo4jCluster {
    @Autowired
    Neo4jUtil neo4jUtil;
    @Test
    public void Test(){
        String cypher="match(n) return n limit 10";
        List<HashMap<String, Object>> nodes= neo4jUtil.GetGraphNode(cypher);
        System.out.println(JSON.toJSON(nodes));
    }
}
