package cnki.kg.demo;

import cnki.kg.demo.util.Neo4jUtil;
import cnki.kg.demo.util.StringUtil;
import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


@SpringBootTest
@WebAppConfiguration
public class CreateKeXieCategory {

    @Autowired
    Neo4jUtil neo4jUtil;
    @Autowired
    @Qualifier("mysqlJdbcTemplate")
    JdbcTemplate mysqlJdbcTemplate;
    private final String domain = "a44b8cae-d756-4808-8a14-1b7fb3e814cb";//科协图谱

    private enum GraphNodeType {
        Conceptual("概念实体", 0),
        Entity("实体", 1),
        Property("属性", 2),
        Method("方法", 3),
        Model("模型", 4),
        Cockpit("驾驶舱", 5);

        private final String name;

        private Integer value;

        public String getName() {
            return name;
        }

        public final Integer getValue() {
            return value;
        }

        private GraphNodeType(String name, Integer value) {
            this.name = name;
            this.value = value;
        }
    }

    @Test
    public void createMaster() {
        String cr = String.format("match(n:`%s`)-[r]-(m:`%s`) detach delete n,r,m", domain, domain);
        String cp = String.format("match(n:`%s`) detach delete n", domain);
        neo4jUtil.excuteCypherSql(cr);
        neo4jUtil.excuteCypherSql(cp);
        String centerName = "科协";
        String cypherSql = String.format("create (n:`%s`{name:'%s',type:%s,sortCode:999,isLeaf:%s,tableName:'%s'}) return n", domain, centerName, GraphNodeType.Conceptual.getValue(), 0, "");
        List<HashMap<String, Object>> nodesQl = neo4jUtil.GetGraphNode(cypherSql);
        if (nodesQl != null && nodesQl.size() > 0) {
            HashMap<String, Object> nodeql = nodesQl.get(0);
            String[] dataList = new String[]{"全国学会", "地方科协", "大事记", "领导讲话", "人物", "重要文件", "特色专题", "表彰奖励", "年度事件"};
            if (dataList == null || dataList.length == 0) return;
            for (String mp : dataList) {
                createNodeAndLink(GraphNodeType.Conceptual, domain, mp.trim(), "下级", "", 0, nodeql.get("uuid").toString(), "");
            }
        }
    }
    private void deleteMain(String tableName) {
        String cp = String.format("match(n:`%s`) where n.tableName='%s' detach  delete n", domain,tableName);
        neo4jUtil.excuteCypherSql(cp);
    }
    @Test
    public void createQGXH() {
        deleteMain("全国学会");
        // 1. 全国学会-》学会名称
        String cp = String.format("match(n:`%s`) where n.name='全国学会' return n", domain);
        List<HashMap<String, Object>> keXieNode = neo4jUtil.GetGraphNode(cp);
        if (keXieNode == null || keXieNode.size() == 0) return;
        String xieHuiUuid = keXieNode.get(0).get("uuid").toString();
        String sql1 = "SELECT id,科协组织,学会名称,综述,业务分类,业务事件名称,时间,地点,人物姓名,主承办单位,与会主要人员,与会人数,年份 from 全国学会";
        List<Map<String, Object>> dataList = mysqlJdbcTemplate.queryForList(sql1);
        if (dataList == null || dataList.size() == 0) return;
        for (Map<String, Object> mp : dataList) {
            for (String s : mp.keySet()) {
                Object val = mp.get(s);
                if (val == null || StringUtil.isBlank(val.toString())) {
                    mp.replace(s, "未知");
                }
            }
        }
        List<String> xueHuiNameList = dataList.stream().filter(n -> n.get("学会名称") != null).map(n -> n.get("学会名称").toString()).distinct().collect(Collectors.toList());
        if (xueHuiNameList == null || xueHuiNameList.size() == 0) return;
        for (String xieHuiName : xueHuiNameList) {
            HashMap<String, Object> xieHuiNode = createNodeAndLink(GraphNodeType.Entity, domain, xieHuiName.trim(), "学会名称", "学会协会", 0, xieHuiUuid, "");
            //2.学会名称-》综述
            List<String> zongSuList = dataList.stream().filter(n -> n.get("学会名称") != null && n.get("学会名称").toString().equals(xieHuiName.trim())).map(n -> n.get("综述").toString()).distinct().collect(Collectors.toList());
            if (zongSuList == null || zongSuList.size() == 0) continue;
            for (String zongSuName : zongSuList) {
                HashMap<String, Object> zongSuNode = createNodeAndLink(GraphNodeType.Entity, domain, zongSuName.trim(), "综述", "学会协会", 0, xieHuiNode.get("uuid").toString(), "");
                //2.综述-》业务分类
                List<String> ywList = dataList.stream().filter(n -> n.get("业务分类") != null
                        && n.get("学会名称") != null && n.get("学会名称").toString().equals(xieHuiName.trim())
                        && n.get("综述") != null && n.get("综述").toString().equals(zongSuName.trim())).map(n -> n.get("业务分类").toString()).distinct().collect(Collectors.toList());
                if (ywList == null || ywList.size() == 0) continue;
                for (String ywName : ywList) {
                    HashMap<String, Object> ywNode = createNodeAndLink(GraphNodeType.Entity, domain, ywName.trim(), "业务分类", "学会协会", 0, zongSuNode.get("uuid").toString(), "");
                    //3.业务类别-》业务事件名称          【时间,地点,业务事件名称,主承办单位,与会主要人员,与会人数,年份】
                    List<HashMap<String, Object>> sjNameList = dataList.stream().filter(n -> n.get("学会名称") != null && n.get("学会名称").toString().equals(xieHuiName.trim())
                            && n.get("综述").toString().equals(zongSuName.trim())
                            && n.get("业务分类").toString().equals(ywName.trim())
                    ).map(n ->{
                        HashMap<String, Object> detail = new HashMap<>();
                        detail.put("dataId", n.get("id").toString());
                        detail.put("业务事件名称", n.get("业务事件名称").toString());
                        return detail;
                    } ).distinct().collect(Collectors.toList());
                    if (sjNameList == null || sjNameList.size() == 0) continue;
                    for (HashMap<String, Object> sjItem : sjNameList) {
                        String sjName=sjItem.get("业务事件名称").toString();
                        String dataId=sjItem.get("dataId").toString();
                        HashMap<String, Object> sjNode = createNodeAndLink(GraphNodeType.Entity, domain, sjName.trim(), "业务事件名称", "学会协会", 0, ywNode.get("uuid").toString(), dataId);
                        //4.业务事件名称-》【时间,地点,主承办单位,与会主要人员,与会人数,年份】
                        List<Map<String, Object>> sjItemList = dataList.stream().filter(n -> n.get("学会名称").toString().equals(xieHuiName.trim())
                                && n.get("综述").toString().equals(zongSuName.trim())
                                && n.get("业务分类").toString().equals(ywName.trim())
                                && n.get("业务事件名称").toString().equals(sjName.trim())
                        ).map(n -> {
                            HashMap<String, Object> detail = new HashMap<>();
                            detail.put("dataId", n.get("id").toString());
                            detail.put("时间", n.get("时间").toString());
                            detail.put("地点", n.get("地点").toString());
                            detail.put("人物姓名", n.get("人物姓名").toString());
                            detail.put("主承办单位", n.get("主承办单位").toString());
                            detail.put("与会主要人员", n.get("与会主要人员").toString());
                            detail.put("与会人数", n.get("与会人数").toString());
                            detail.put("年份", n.get("年份").toString());
                            return detail;
                        }).distinct().collect(Collectors.toList());
                        if (sjItemList == null || sjItemList.size() == 0) continue;
                        for (Map<String, Object> sjDetail : sjItemList) {
                            for (String key : sjDetail.keySet()) {
                                if (key.equals("dataId")) continue;
                                //创建键
                                HashMap<String, Object> firstNode = createNodeAndLink(GraphNodeType.Property, domain, key.trim(), key.trim(), "学会协会", 0, sjNode.get("uuid").toString(), sjDetail.get("dataId").toString());
                                //创建值
                                //HashMap<String, Object> secondNode = createNodeAndLink(GraphNodeType.Entity, domain, sjDetail.get(key).toString(), key.trim(), "学会协会", 1, firstNode.get("uuid").toString(), sjDetail.get("dataId").toString());
                                //System.out.println(key + ":" + secondNode.get("name"));
                                if (key.equals("人物姓名")) {
                                    String [] zwList=new String[]{};
                                    if(sjDetail.get(key)!=null&&StringUtil.isNotBlank(sjDetail.get(key).toString())){
                                        zwList=sjDetail.get(key).toString().split(";",-1);
                                    }
                                    for (String xm : zwList) {
                                        HashMap<String, Object> thirdNode = createNodeAndLink(GraphNodeType.Entity, domain, xm, key.trim(),"学会协会", 1, firstNode.get("uuid").toString(), sjDetail.get("dataId").toString());
                                        System.out.println(xm + ":" + thirdNode.get("name"));
                                    }
                                }else{
                                    HashMap<String, Object> secondNode = createNodeAndLink(GraphNodeType.Entity, domain, sjDetail.get(key).toString(), key.trim(), "学会协会", 1, firstNode.get("uuid").toString(), sjDetail.get("dataId").toString());
                                    System.out.println(key + ":" + secondNode.get("name"));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    @Test
    public void createDFXH() {
        // 1. 地方科协-》协会名称
        String cp = String.format("match(n:`%s`) where n.name='地方科协' return n", domain);
        List<HashMap<String, Object>> keXieNode = neo4jUtil.GetGraphNode(cp);
        if (keXieNode == null || keXieNode.size() == 0) return;
        String xieHuiUuid = keXieNode.get(0).get("uuid").toString();
        String sql1 = "SELECT id,科协组织,协会名称,综述,业务分类,会议名称,时间,地点,人物姓名,人物职务,与会主要人员,与会人数,会议全文,年份 from 地方科协";
        List<Map<String, Object>> dataList = mysqlJdbcTemplate.queryForList(sql1);
        if (dataList == null || dataList.size() == 0) return;
        for (Map<String, Object> mp : dataList) {
            for (String s : mp.keySet()) {
                Object val = mp.get(s);
                if (val == null || StringUtil.isBlank(val.toString())) {
                    mp.replace(s, "未知");
                }
            }
        }
        List<String> xueHuiNameList = dataList.stream().filter(n -> n.get("协会名称") != null).map(n -> n.get("协会名称").toString()).distinct().collect(Collectors.toList());
        if (xueHuiNameList == null || xueHuiNameList.size() == 0) return;
        for (String xieHuiName : xueHuiNameList) {
            HashMap<String, Object> xieHuiNode = createNodeAndLink(GraphNodeType.Entity, domain, xieHuiName.trim(), "协会名称", "协会名称", 0, xieHuiUuid, "");
            //2.协会名称-》综述
            List<String> zongSuList = dataList.stream().filter(n -> n.get("协会名称") != null && n.get("协会名称").toString().equals(xieHuiName.trim())).map(n -> n.get("综述").toString()).distinct().collect(Collectors.toList());
            if (zongSuList == null || zongSuList.size() == 0) continue;
            for (String zongSuName : zongSuList) {
                HashMap<String, Object> zongSuNode = createNodeAndLink(GraphNodeType.Entity, domain, zongSuName.trim(), "综述", "地方科协", 0, xieHuiNode.get("uuid").toString(), "");
                //2.综述-》业务分类
                List<String> ywList = dataList.stream().filter(n -> n.get("业务分类") != null
                        && n.get("协会名称") != null && n.get("协会名称").toString().equals(xieHuiName.trim())
                        && n.get("综述") != null && n.get("综述").toString().equals(zongSuName.trim())).map(n -> n.get("业务分类").toString()).distinct().collect(Collectors.toList());
                if (ywList == null || ywList.size() == 0) continue;
                for (String ywName : ywList) {
                    HashMap<String, Object> ywNode = createNodeAndLink(GraphNodeType.Entity, domain, ywName.trim(), "业务分类", "地方科协", 0, zongSuNode.get("uuid").toString(), "");
                    //3.业务类别-》会议名称          【时间,地点,会议名称,主承办单位,与会主要人员,与会人数,年份】
                    List<HashMap<String, Object>> sjNameList = dataList.stream().filter(n -> n.get("协会名称") != null && n.get("协会名称").toString().equals(xieHuiName.trim())
                            && n.get("综述").toString().equals(zongSuName.trim())
                            && n.get("业务分类").toString().equals(ywName.trim())
                    ).map(n ->{
                        HashMap<String, Object> detail = new HashMap<>();
                        detail.put("dataId", n.get("id").toString());
                        detail.put("会议名称", n.get("会议名称").toString());
                        return detail;
                    } ).distinct().collect(Collectors.toList());
                    if (sjNameList == null || sjNameList.size() == 0) continue;
                    for (HashMap<String, Object> sjItem : sjNameList) {
                        String sjName=sjItem.get("会议名称").toString();
                        String dataId=sjItem.get("dataId").toString();
                        HashMap<String, Object> sjNode = createNodeAndLink(GraphNodeType.Entity, domain, sjName.trim(), "会议名称", "地方科协", 0, ywNode.get("uuid").toString(), dataId);
                        //4.会议名称-》【时间,地点,与会主要人员,与会人数,年份】
                        List<Map<String, Object>> sjItemList = dataList.stream().filter(n -> n.get("协会名称").toString().equals(xieHuiName.trim())
                                && n.get("综述").toString().equals(zongSuName.trim())
                                && n.get("业务分类").toString().equals(ywName.trim())
                                && n.get("会议名称").toString().equals(sjName.trim())
                        ).map(n -> {
                            HashMap<String, Object> detail = new HashMap<>();
                            detail.put("dataId", n.get("id").toString());
                            detail.put("时间", n.get("时间").toString());
                            detail.put("地点", n.get("地点").toString());
                            detail.put("与会主要人员", n.get("与会主要人员").toString());
                            detail.put("与会人数", n.get("与会人数").toString());
                            detail.put("年份", n.get("年份").toString());
                            return detail;
                        }).distinct().collect(Collectors.toList());
                        if (sjItemList == null || sjItemList.size() == 0) continue;
                        for (Map<String, Object> sjDetail : sjItemList) {
                            for (String key : sjDetail.keySet()) {
                                if (key.equals("dataId")) continue;
                                //创建键
                                HashMap<String, Object> firstNode = createNodeAndLink(GraphNodeType.Property, domain, key.trim(), key.trim(), "地方科协", 0, sjNode.get("uuid").toString(), sjDetail.get("dataId").toString());
                                //创建值
                                HashMap<String, Object> secondNode = createNodeAndLink(GraphNodeType.Entity, domain, sjDetail.get(key).toString(), key.trim(), "地方科协", 1, firstNode.get("uuid").toString(), sjDetail.get("dataId").toString());
                                System.out.println(key + ":" + secondNode.get("name"));
                            }
                        }
                    }
                }
            }
        }
    }
    @Test
    public void createRW() {
        deleteMain("人物");
        String cp = String.format("match(n:`%s`) where n.name='人物' return n", domain);
        List<HashMap<String, Object>> keXieNode = neo4jUtil.GetGraphNode(cp);
        if (keXieNode == null || keXieNode.size() == 0) return;
        //1.人物姓名-》姓名
        String rwUuid = keXieNode.get(0).get("uuid").toString();
        String sql1 = "SELECT id,姓名,职务,出生日期,户籍所在地,分类,人物全文,年份 from 人物";
        List<Map<String, Object>> dataList = mysqlJdbcTemplate.queryForList(sql1);
        if (dataList == null || dataList.size() == 0) return;
        for (Map<String, Object> mp : dataList) {
            for (String s : mp.keySet()) {
                Object val = mp.get(s);
                if (val == null || StringUtil.isBlank(val.toString())) {
                    mp.replace(s, "未知");
                }
            }
        }
        List<String> rwNameList = dataList.stream().filter(n -> n.get("姓名") != null).map(n -> n.get("姓名").toString()).distinct().collect(Collectors.toList());
        if (rwNameList == null || rwNameList.size() == 0) return;
        for (String rwName : rwNameList) {
            HashMap<String, Object> rwNode = createNodeAndLink(GraphNodeType.Entity, domain, rwName.trim(), "姓名", "人物", 0, rwUuid, "");
            //2.姓名-》【职务,出生日期,户籍所在地,分类人物全文】
            List<Map<String, Object>> sjItemList = dataList.stream().filter(n -> n.get("姓名").toString().equals(rwName.trim())
            ).map(n -> {
                HashMap<String, Object> detail = new HashMap<>();
                detail.put("dataId", n.get("id").toString());
                detail.put("职务", n.get("职务").toString());
                detail.put("出生日期", n.get("出生日期").toString());
                detail.put("户籍所在地", n.get("户籍所在地").toString());
                detail.put("分类", n.get("分类").toString());
                detail.put("人物全文", n.get("人物全文").toString());
                return detail;
            }).distinct().collect(Collectors.toList());
            if (sjItemList == null || sjItemList.size() == 0) continue;
            for (Map<String, Object> sjDetail : sjItemList) {
                for (String key : sjDetail.keySet()) {
                    if (key.equals("dataId")) continue;
                    //创建键
                    HashMap<String, Object> firstNode = createNodeAndLink(GraphNodeType.Property, domain, key.trim(), key.trim(), "人物", 0, rwNode.get("uuid").toString(), sjDetail.get("dataId").toString());
                    //创建值
                    String[] nodeArr = new String[]{sjDetail.get(key).toString()};
                    if (key.equals("职务")) {
                        nodeArr = sjDetail.get(key).toString().split("，", -1);
                    }
                    for (String s : nodeArr) {
                        String format = s;
                        if (key.equals("职务") && nodeArr.length > 1) {
                            format = s.replace("，", "").replace("。", "");
                        }
                        HashMap<String, Object> secondNode = createNodeAndLink(GraphNodeType.Entity, domain, format, key.trim(), "人物", 1, firstNode.get("uuid").toString(), sjDetail.get("dataId").toString());
                        System.out.println(key + ":" + secondNode.get("name"));
                    }

                }
            }
        }
    }
    @Test
    public void createRWToXieHui() {
        String cp=String.format("match(`%s`) where n.name='%s' and n.tableName='人物'");
    }
    @Test
    public void createDSJ() {
        // 1. 大事记-》年份
        String cp = String.format("match(n:`%s`) where n.name='大事记' return n", domain);
        List<HashMap<String, Object>> keXieNode = neo4jUtil.GetGraphNode(cp);
        if (keXieNode == null || keXieNode.size() == 0) return;
        String dsjUuid = keXieNode.get(0).get("uuid").toString();
        String sql1 = "SELECT id,事件名称,年份,月份,日期,人物姓名,人物职务,地区,事件全文 from 大事记";
        List<Map<String, Object>> dataList = mysqlJdbcTemplate.queryForList(sql1);
        if (dataList == null || dataList.size() == 0) return;
        for (Map<String, Object> mp : dataList) {
            for (String s : mp.keySet()) {
                Object val = mp.get(s);
                if (val == null || StringUtil.isBlank(val.toString())) {
                    mp.replace(s, "未知");
                }
            }
        }
        List<String> yearList = dataList.stream().map(n -> n.get("年份").toString()).distinct().collect(Collectors.toList());
        if (yearList == null || yearList.size() == 0) return;
        for (String yearName : yearList) {
            HashMap<String, Object> yearNode = createNodeAndLink(GraphNodeType.Entity, domain, yearName.trim(), "年份", "大事记", 0, dsjUuid, "");
            String yearUuid = yearNode.get("uuid").toString();
            //2.年份-》月份
            List<String> monthList = dataList.stream().filter(n -> n.get("年份").toString().equals(yearName.trim())
            ).map(n -> n.get("月份").toString()).distinct().collect(Collectors.toList());
            if (monthList == null || monthList.size() == 0) continue;
            for (String monthName : monthList) {
                HashMap<String, Object> monthNode = createNodeAndLink(GraphNodeType.Entity, domain, monthName.trim(), "月份", "大事记", 0, yearUuid, "");
                String monthUuid = monthNode.get("uuid").toString();
                //3.月份-》日期
                List<String> dateList = dataList.stream().filter(n -> n.get("年份").toString().equals(yearName.trim())
                        && n.get("月份").toString().equals(monthName.trim())
                ).map(n -> n.get("日期").toString()).distinct().collect(Collectors.toList());
                for (String dayName : dateList) {
                    HashMap<String, Object> dayNode = createNodeAndLink(GraphNodeType.Entity, domain, dayName.trim(), "日期", "大事记", 0, monthUuid, "");
                    String dayUuid = dayNode.get("uuid").toString();
                    //4.日期-》事件名称
                    List<HashMap<String, Object>> eventList = dataList.stream().filter(n -> n.get("年份").toString().equals(yearName.trim())
                            && n.get("月份").toString().equals(monthName.trim())
                            && n.get("日期").toString().equals(dayName.trim())
                    ).map(n -> {
                        HashMap<String, Object> e=new HashMap<>();
                        e.put("dataId",n.get("id").toString());
                        e.put("事件名称",n.get("事件名称").toString());
                        return e;
                    }).distinct().collect(Collectors.toList());
                    for (HashMap<String, Object> event : eventList) {
                        String eventName=event.get("事件名称").toString();
                        String dataId=event.get("dataId").toString();
                        HashMap<String, Object> eventNode = createNodeAndLink(GraphNodeType.Entity, domain, eventName.trim(), "事件名称", "大事记", 0, dayUuid, dataId);
                        String eventUuid = eventNode.get("uuid").toString();
                        //5.事件名称-》【时间,地点,人物姓名,年份】
                        List<Map<String, Object>> sjItemList = dataList.stream().filter(n -> n.get("id").toString().equals(dataId)
                        ).map(n -> {
                            HashMap<String, Object> detail = new HashMap<>();
                            detail.put("dataId", n.get("id").toString());
                            detail.put("时间", n.get("年份").toString()+n.get("日期").toString());
                            detail.put("地点", n.get("地区").toString());
                            detail.put("人物姓名", n.get("人物职务").toString());
                            return detail;
                        }).distinct().collect(Collectors.toList());
                        if (sjItemList == null || sjItemList.size() == 0) continue;
                        for (Map<String, Object> sjDetail : sjItemList) {
                            for (String key : sjDetail.keySet()) {
                                if (key.equals("dataId")) continue;
                                //创建键
                                HashMap<String, Object> firstNode = createNodeAndLink(GraphNodeType.Property, domain, key.trim(), key.trim(), "大事记", 0, eventUuid, sjDetail.get("dataId").toString());
                                //创建值
                                if (key.equals("人物姓名")) {
                                    Map<String,List<String>> rAndz= getRwAndZw(sjDetail.get(key).toString());
                                    for (String name : rAndz.keySet()) {
                                        HashMap<String, Object> secondNode = createNodeAndLink(GraphNodeType.Entity, domain, name, key.trim(), "大事记", 0, firstNode.get("uuid").toString(), sjDetail.get("dataId").toString());
                                        System.out.println(key + ":" + secondNode.get("name"));
                                        List<String> zwList=rAndz.get(name);
                                        for (String zw : zwList) {
                                            HashMap<String, Object> thirdNode = createNodeAndLink(GraphNodeType.Entity, domain, zw, "职务", "大事记", 1, secondNode.get("uuid").toString(), sjDetail.get("dataId").toString());
                                            System.out.println(name + ":" + thirdNode.get("name"));
                                        }
                                    }
                                }else{
                                    HashMap<String, Object> secondNode = createNodeAndLink(GraphNodeType.Entity, domain, sjDetail.get(key).toString(), key.trim(), "大事记", 1, firstNode.get("uuid").toString(), sjDetail.get("dataId").toString());
                                    System.out.println(key + ":" + secondNode.get("name"));
                                }

                            }
                        }
                    }
                }

            }
        }
    }

    @Test
    public void createLDJH() {
        String cp = String.format("match(n:`%s`) where n.name='领导讲话' return n", domain);
        List<HashMap<String, Object>> keXieNode = neo4jUtil.GetGraphNode(cp);
        if (keXieNode == null || keXieNode.size() == 0) return;
        String dsjUuid = keXieNode.get(0).get("uuid").toString();
        String sql1 = "SELECT id,主题名称,讲话类型,人物姓名,时间,讲话全文 from 领导讲话";
        List<Map<String, Object>> dataList = mysqlJdbcTemplate.queryForList(sql1);
        if (dataList == null || dataList.size() == 0) return;
        for (Map<String, Object> mp : dataList) {
            for (String s : mp.keySet()) {
                Object val = mp.get(s);
                if (val == null || StringUtil.isBlank(val.toString())) {
                    mp.replace(s, "未知");
                }
            }
        }
        // 1. 领导讲话-》讲话类型
        List<String> typeList = dataList.stream().map(n -> n.get("讲话类型").toString()).distinct().collect(Collectors.toList());
        if (typeList == null || typeList.size() == 0) return;
        for (String typeName : typeList) {
            HashMap<String, Object> typeNode = createNodeAndLink(GraphNodeType.Entity, domain, typeName.trim(), "讲话类型", "领导讲话", 0, dsjUuid, "");
            String typeUuid = typeNode.get("uuid").toString();
            //2.讲话类型-》人物姓名
            List<String> rwList = dataList.stream().filter(n -> n.get("讲话类型").toString().equals(typeName.trim())
            ).map(n -> n.get("人物姓名").toString()).distinct().collect(Collectors.toList());
            if (rwList == null || rwList.size() == 0) continue;
            for (String rwName : rwList) {
                HashMap<String, Object> rwNode = createNodeAndLink(GraphNodeType.Entity, domain, rwName.trim(), "人物姓名", "领导讲话", 0, typeUuid, "");
                String rwUuid = rwNode.get("uuid").toString();
                //2.人物姓名-》主题名称
                List<HashMap<String,Object>> ztList = dataList.stream().filter(n -> n.get("讲话类型").toString().equals(typeName.trim())
                        &&n.get("人物姓名").toString().equals(rwName.trim())
                ).map(n -> {
                    HashMap<String,Object> mp=new HashMap<>();
                    mp.put("id",n.get("id").toString());
                    mp.put("主题名称",n.get("主题名称").toString());
                    return mp;
                }).distinct().collect(Collectors.toList());
                if (ztList == null || ztList.size() == 0) continue;
                for (HashMap<String,Object>  mp: ztList) {
                    String ztName=mp.get("主题名称").toString();
                    String ztId=mp.get("id").toString();
                    HashMap<String, Object> ztNode = createNodeAndLink(GraphNodeType.Entity, domain, ztName.trim(), "主题名称", "领导讲话", 0, rwUuid, ztId);
                    String ztUuid = ztNode.get("uuid").toString();
                    //3.主题名称-》时间
                    List<String> sjList = dataList.stream().filter(n -> n.get("讲话类型").toString().equals(typeName.trim())
                            &&n.get("人物姓名").toString().equals(rwName.trim())
                            &&n.get("主题名称").toString().equals(ztName.trim())
                    ).map(n -> n.get("时间").toString()).distinct().collect(Collectors.toList());
                    if (sjList == null || sjList.size() == 0) continue;
                    for (String sjName : sjList) {
                        createNodeAndLink(GraphNodeType.Entity, domain, sjName.trim(), "时间", "领导讲话", 1, ztUuid, ztId);
                    }

                }
            }
        }
    }

    @Test
    public void TestRule(){
        String text="王兆国（中共中央政治局委员,全国人大常委会副委员长）；韩启德（全国人大常委会副委员长,中国科协主席）；陈希（中国科协常务副主席,书记处第一书记）";
        Map<String,List<String>> ob=getRwAndZw(text);
        System.out.println(JSON.toJSONString(ob));
    }
    @Test
    public void TestUpdateLdjhDate(){
        String text="select id,时间 from 领导讲话";
        List<Map<String, Object>> dataList = mysqlJdbcTemplate.queryForList(text);
        if (dataList == null || dataList.size() == 0) return;
        for (Map<String, Object> mp : dataList) {
            String date=mp.get("时间").toString();
            String[] arr=date.split("-",-1);
            String newDate=String.format("%s-%s-%s",arr[1],arr[0],arr[2]);
            String x=String.format("update 领导讲话 set 时间='%s' where id=%s",newDate,mp.get("id"));
            mysqlJdbcTemplate.execute(x);
        }
    }
    @Test
    public void TestUpdateLdXueHuiNode(){
        String text="select 学会名称 from 学会协会";
        List<Map<String, Object>> dataList = mysqlJdbcTemplate.queryForList(text);
        if (dataList == null || dataList.size() == 0) return;
        List<String> dataNames=dataList.stream().map(n->n.get("学会名称").toString()).distinct().collect(Collectors.toList());
        int i=0;
        for (String mp : dataNames) {
            i++;
            String x=String.format("MATCH (n:`%s`) where n.name='%s'  set n.sortCode=%s",domain,mp,100+i);
            neo4jUtil.excuteCypherSql(x);
        }
    }
    private Map<String,List<String>> getRwAndZw(String text){
        Map<String,List<String>> mp=new HashMap<>();
        String regex ="([^；].*?)（(.*?)）";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            String key=matcher.group(1);
            String val=matcher.group(2);
            String[] arr=val.split(",",-1);
            List<String> valArr= Arrays.asList(arr);
            mp.put(key,valArr);
        }
        return mp;
    }
    private HashMap<String, Object> createNodeAndLink(GraphNodeType type, String domain, String nodeName, String linkName, String tableName, Integer isLeaf, String pid, String dataId) {
        HashMap<String, Object> nodeql = new HashMap<>();
        String cypherSql = String.format("create (n:`%s`{name:'%s',alia:'%s',type:%s,sortCode:999,isLeaf:%s,tableName:'%s',fieldName:'%s',dataId:'%s'}) return n", domain, nodeName, nodeName, type.getValue(), isLeaf, tableName, linkName, dataId);
        List<HashMap<String, Object>> nodesQl = neo4jUtil.GetGraphNode(cypherSql);
        if (nodesQl != null && nodesQl.size() > 0) {
            nodeql = nodesQl.get(0);
            String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND id(m) = %s "
                    + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain, domain, pid, nodeql.get("uuid"), linkName);
            neo4jUtil.excuteCypherSql(LinkcypherSql);
        }
        return nodeql;
    }

    private HashMap<String, Object> mergeNodeAndLink(GraphNodeType type, String domain, String nodeName, String linkName, String tableName, Integer isLeaf, String pid, String dataId) {
        HashMap<String, Object> nodeql = new HashMap<>();
        String cypherSql = String.format("merge (n:`%s`{name:'%s',alia:'%s',type:%s,sortCode:999,isLeaf:%s,tableName:'%s',fieldName:'%s',dataId:'%s'}) return n", domain, nodeName, nodeName, type.getValue(), isLeaf, tableName, linkName, dataId);
        List<HashMap<String, Object>> nodesQl = neo4jUtil.GetGraphNode(cypherSql);
        if (nodesQl != null && nodesQl.size() > 0) {
            nodeql = nodesQl.get(0);
            String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND id(m) = %s "
                    + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain, domain, pid, nodeql.get("uuid"), linkName);
            neo4jUtil.excuteCypherSql(LinkcypherSql);
        }
        return nodeql;
    }

}
