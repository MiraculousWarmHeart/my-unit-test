package cnki.kg.demo;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.*;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.IOException;


@SpringBootTest
@WebAppConfiguration
public class TestNum {
    @Test
    public void update(){
        String a=String.format("%60d",12);//其中0表示补零而不是补空格，6表示至少6位
        System.out.println(a);
        String a1=String.format("%06d",12);//其中0表示补零而不是补空格，6表示至少6位
        System.out.println(a1);
        String aa=StringUtils.rightPad("0054", 6, "0");
        System.out.println(aa);
        /*String a1=String.format("%06s","0054");//其中0表示补零而不是补空格，6表示至少6位
        System.out.println(a1);
        String a2=String.format("%06d",12);//其中0表示补零而不是补空格，6表示至少6位
        System.out.println(a2);
        String a3=String.format("%06d",12);//其中0表示补零而不是补空格，6表示至少6位
        System.out.println(a3);*/
    }
    @Test
    public void TestHttpPost(){
        String url="http://192.168.20.26:9200/_opendistro/_sql";
        String postData="{\n" +
                "    \"query\":\"select name,createYear,category from bdms_index_370_school where (  name ='沙流小学'  )   \"\n" +
                "}";
        String deliverResponse = null;
        CloseableHttpClient httpclient = HttpClients.createDefault();

        HttpHost targetHost = new HttpHost("192.168.20.26", 9200, "http");
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(new AuthScope(targetHost.getHostName(),
                targetHost.getPort()), new UsernamePasswordCredentials(
                "admin", "admin"));
        // Add AuthCache to the execution context
        HttpClientContext context = HttpClientContext.create();
        context.setCredentialsProvider(credsProvider);
        HttpPost httppost = new HttpPost(url);
        //设置超时时间
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(60)
                .setConnectTimeout(60).setSocketTimeout(60).build();
        httppost.setConfig(requestConfig);
        StringEntity postentity = new StringEntity(postData,"utf-8");
        postentity.setContentType("application/json");
        postentity.setContentEncoding("utf-8");
        httppost.setEntity(postentity);
        CloseableHttpResponse response = null;

        try {
            response = httpclient.execute(targetHost, httppost, context);

            HttpEntity entity = response.getEntity();
            deliverResponse = EntityUtils.toString(entity);
            httpclient.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("aa");
    }
    @Test
    public void TestHttpPost2(){
        String url="http://192.168.20.26:9200/_opendistro/_sql";
        String postData="{\n" +
                "    \"query\":\"select name,createYear,category from bdms_index_370_school where (  name ='沙流小学'  )   \"\n" +
                "}";
        HttpResponse response=null;
        CredentialsProvider provider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials= new UsernamePasswordCredentials("admin", "admin");
        provider.setCredentials(AuthScope.ANY, credentials);
        // 创建客户端的时候进行身份验证
        HttpClient httpClient = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();
        String aa="";
        try {
            HttpPost httpPost = new HttpPost(url);
            StringEntity stringEntity = new StringEntity(postData, "UTF-8");
            httpPost.setEntity(stringEntity);
            httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
            // 执行请求
            response = httpClient.execute(httpPost);
            // 判断返回状态是否为200
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                aa= EntityUtils.toString(response.getEntity(), "UTF-8");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("aa");
    }
}
