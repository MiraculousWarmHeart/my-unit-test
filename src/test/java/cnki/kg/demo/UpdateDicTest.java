package cnki.kg.demo;

import cnki.kg.demo.util.DateUtil;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@SpringBootTest
@WebAppConfiguration
public class UpdateDicTest {

    @Test
    public void update() {
        final String url = "jdbc:mysql://localhost/kg_jt?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=Asia/Shanghai";
        final String name = "com.mysql.cj.jdbc.Driver";
        final String user = "tan";
        final String password = "123456";
        Connection conn = null;
        // 开始时间
        Long begin = new Date().getTime();
        String prefix = "INSERT INTO kg_word_item_temp (`DICID`,`SOURCE`,`CONTENT`,`CREATEDATE`,`CREATEUSER`,`STATE`) values ";
        try {
            Class.forName(name);//指定连接类型
            conn = DriverManager.getConnection(url, user, password);//获取连接
            if (conn != null) {
                System.out.println("获取连接成功");
                // 保存sql后缀
                List<String> suffixList=new ArrayList<>();
                // 设置事务为非自动提交
                conn.setAutoCommit(false);
                // 比起st，pst会更好些
                PreparedStatement pst = conn.prepareStatement("INSERT INTO kg_word_item_temp (`DICID`,`SOURCE`,`CONTENT`,`CREATEDATE`,`CREATEUSER`,`STATE`) values");//准备执行语句
                for (int i = 0; i < 200; i++) {
                    Long onceStart = new Date().getTime();
                    for (int j = 0; j < 50000; j++) {
                        // 构建SQL后缀
                        String content = String.format("%s-%s-%s", "谭超", i, j);
                        suffixList.add(String.format("('49','321_school','%s',now(),'sa',6)", content));
                    }
                    // 构建完整SQL
                    String param = String.join(",",suffixList);
                    String sqlFull = prefix + param;
                    // 执行操作
                    pst.execute(sqlFull);
                    // 清空上一次添加的数据
                    suffixList.clear();
                    Long onceEnd = new Date().getTime();
                    System.out.println("第【" + i + "】次插入，每次50000条");
                    System.out.println("本次50000条数据插入花费时间 : " + (onceEnd - onceStart) + " ms");
                }
                // 提交事务
                conn.commit();
                pst.close();
                conn.close();
            } else {
                System.out.println("获取连接失败");
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        // 结束时间
        Long end = new Date().getTime();
        // 耗时
        System.out.println("1000万条数据插入花费时间 : " + (end - begin) / 1000 + " s");
        System.out.println("插入完成");
    }
    @Test
    public void update3() {
        final String url = "jdbc:mysql://localhost/kg_jt?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=Asia/Shanghai";
        final String name = "com.mysql.cj.jdbc.Driver";
        final String user = "tan";
        final String password = "123456";
        Connection conn = null;
        // 开始时间
        Long begin = new Date().getTime();
        String prefix = "INSERT INTO kg_word_item_temp2 (`DICID`,`SOURCE`,`CONTENT`,`CREATEDATE`,`CREATEUSER`,`STATE`) values ";
        try {
            Class.forName(name);//指定连接类型
            conn = DriverManager.getConnection(url, user, password);//获取连接
            if (conn != null) {
                System.out.println("获取连接成功");
                // 保存sql后缀
                List<String> suffixList=new ArrayList<>();
                // 设置事务为非自动提交
                conn.setAutoCommit(false);
                // 比起st，pst会更好些
                PreparedStatement pst = conn.prepareStatement("INSERT INTO kg_word_item_temp2 (`DICID`,`SOURCE`,`CONTENT`,`CREATEDATE`,`CREATEUSER`,`STATE`) values ");//准备执行语句
                for (int i = 0; i < 200; i++) {
                    Long onceStart = new Date().getTime();
                    for (int j = 0; j < 50000; j++) {
                        // 构建SQL后缀
                        String content = String.format("%s-%s-%s", "谭超", i, j);
                        suffixList.add(String.format("('49','321_school','%s',now(),'sa',6)", content));
                    }
                    // 构建完整SQL
                    String param = String.join(",",suffixList);
                    String sqlFull = prefix + param;
                    // 执行操作
                    pst.executeBatch();
                    // 清空上一次添加的数据
                    suffixList.clear();
                    Long onceEnd = new Date().getTime();
                    System.out.println("第【" + i + "】次插入，每次50000条");
                    System.out.println("本次50000条数据插入花费时间 : " + (onceEnd - onceStart) + " ms");
                }
                // 提交事务
                conn.commit();
                pst.close();
                conn.close();
            } else {
                System.out.println("获取连接失败");
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        // 结束时间
        Long end = new Date().getTime();
        // 耗时
        System.out.println("1000万条数据插入花费时间 : " + (end - begin) / 1000 + " s");
        System.out.println("插入完成");
    }
    @Test
    public void update4() {
        final String url = "jdbc:mysql://192.168.52.64:3306/kg_jt?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=Asia/Shanghai";
        final String name = "com.mysql.cj.jdbc.Driver";
        final String user = "root";
        final String password = "123456";
        Connection conn = null;
        // 开始时间
        Long begin = new Date().getTime();
        String prefix = "INSERT INTO kgwordstc_batch_3 (`CONTENT`) values ";
        try {
            Class.forName(name);//指定连接类型
            conn = DriverManager.getConnection(url, user, password);//获取连接
            if (conn != null) {
                System.out.println("获取连接成功");
                // 保存sql后缀
                List<String> suffixList=new ArrayList<>();
                // 设置事务为非自动提交
                conn.setAutoCommit(false);
                // 比起st，pst会更好些
                PreparedStatement pst = conn.prepareStatement("INSERT INTO kgwordstc_batch_3 (`CONTENT`) values ");//准备执行语句
                for (int i = 0; i < 200; i++) {
                    Long onceStart = new Date().getTime();
                    for (int j = 0; j < 50000; j++) {
                        // 构建SQL后缀
                        String content = String.format("%s-%s-%s", "谭超", i, j);
                        suffixList.add(String.format("('%s')", content));
                    }
                    // 构建完整SQL
                    String param = String.join(",",suffixList);
                    String sqlFull = prefix + param;
                    // 执行操作
                    pst.execute(sqlFull);
                    // 清空上一次添加的数据
                    suffixList.clear();
                    Long onceEnd = new Date().getTime();
                    System.out.println("第【" + i + "】次插入，每次50000条");
                    System.out.println("本次50000条数据插入花费时间 : " + (onceEnd - onceStart) + " ms");
                }
                // 提交事务
                conn.commit();
                pst.close();
                conn.close();
            } else {
                System.out.println("获取连接失败");
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        // 结束时间
        Long end = new Date().getTime();
        // 耗时
        System.out.println("1000万条数据插入花费时间 : " + (end - begin) / 1000 + " s");
        System.out.println("插入完成");
    }
    private void batchInsert(Connection conn, String sql) {
        try {
            if (conn != null) {
                System.out.println("获取连接成功");
                // 设置事务为非自动提交
                conn.setAutoCommit(false);
                // 比起st，pst会更好些
                PreparedStatement pst = conn.prepareStatement("");//准备执行语句
                pst.execute(sql);
                // 提交事务
                conn.commit();
                pst.close();
                conn.close();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Test
    public void update2() {
        final String url = "jdbc:mysql://192.168.52.64:3306/kg_jt?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=Asia/Shanghai";
        final String name = "com.mysql.cj.jdbc.Driver";
        final String user = "root";
        final String password = "123456";
        Connection conn = null;
        // 开始时间
        Long begin = new Date().getTime();
        try {
            Class.forName(name);//指定连接类型
            conn = DriverManager.getConnection(url, user, password);//获取连接
            if (conn != null) {
                System.out.println("获取连接成功");
                // 设置事务为非自动提交
                conn.setAutoCommit(false);
                // 比起st，pst会更好些
                PreparedStatement pst = conn.prepareStatement("INSERT INTO kgwordstc (`CONTENT`,`CREATEDATE`) values (?,?)");//准备执行语句
                for (int i = 0; i < 1000; i++) {
                    for (int j = 0; j < 10000; j++) {
                        String content = String.format("%s-%s-%s", "tc", i, j);
                        pst.setString(1, content);
                        pst.setString(2, DateUtil.getFormatDate(DateUtil.getDateNow()));

                        // “积攒”sql语句
                        pst.addBatch();
                    }
                    // 执行操作
                    pst.executeBatch();
                    // 提交事务
                    conn.commit();
                    // 清空上一次添加的数据
                    pst.clearBatch();
                }
                pst.close();
                conn.close();
            } else {
                System.out.println("获取连接失败");
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        // 结束时间
        Long end = new Date().getTime();
        // 耗时
        System.out.println("1000万条数据插入花费时间 : " + (end - begin) / 1000 + " s");
        System.out.println("插入完成");
    }
}
