package cnki.kg.demo;

import cnki.kg.demo.util.KBaseUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.List;


@SpringBootTest
@WebAppConfiguration
public class CreateZTClass {
    @Autowired
    @Qualifier("mysqlJdbcTemplate")
    JdbcTemplate mysqlJdbcTemplate;
    @Autowired
    private KBaseUtils kBaseUtil;
    public void create(){
        String sql="select * from ZJCLS";
        List<HashMap<String, String>> result=kBaseUtil.excuteQuery(sql);
        for (HashMap<String,String> ob:result){
            for(String key:ob.keySet()){
                System.out.println(String.format("%s:%s",key,ob.get(key)));
            }

        }
    }
}
