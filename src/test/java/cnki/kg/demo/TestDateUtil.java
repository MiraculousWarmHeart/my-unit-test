package cnki.kg.demo;

import cnki.kg.demo.util.ArrayUtils;
import cnki.kg.demo.util.DateUtil;
import cnki.kg.demo.util.HttpClientUtil;
import org.junit.jupiter.api.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@SpringBootTest
@WebAppConfiguration
public class TestDateUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpClientUtil.class);

    @Test
    public void Test() {
        //String dateStr="2019-12-22 15:42:02,2020-12-22 15:42:17";
        String dateStr = "2019-04-22 15:42:02,2020-04-22 15:42:17";
        String[] dateRange = dateStr.split(",");
        String format = "yyyy-MM-dd";

        String dateStart = DateUtil.strToSqlDate(dateRange[0], format);
        String dateEnd = DateUtil.strToSqlDate(dateRange[1], format);
        String dateYear = DateUtil.strToSqlDate(dateRange[1], "yyyy");
        String dateMonth = DateUtil.strToSqlDate(dateRange[1], "yyyyMM");
        String dateDay = DateUtil.strToSqlDate(dateRange[1], "yyyyMMdd");
        //String dateMonth2=DateUtil.strToSqlDate(dateRange[1],"MM");
        String dateMonth2 = DateUtil.strToSqlDate(dateRange[1], "M");
        String dateDay2 = DateUtil.strToSqlDate(dateRange[1], "dd");
        System.out.println(dateStart);
        System.out.println(dateEnd);
        System.out.println(dateYear);
        System.out.println(dateMonth);
        System.out.println(dateMonth2);
        System.out.println(dateDay);
        System.out.println(dateDay2);
    }

    @Test
    public void TestCombinArr() {
        String[] arr1 = new String[]{"A1", "A2", ""};
        String[] arr2 = new String[]{"B1", "B2", ""};
        String[] arr3 = new String[]{"C1", "C2", ""};
        List<List<String>> dataList = new ArrayList<List<String>>();
        dataList.add(Arrays.asList(arr1));
        dataList.add(Arrays.asList(arr2));
        dataList.add(Arrays.asList(arr3));
        List<String> descartes = ArrayUtils.getPermutations(dataList, "", "|");
        descartes.forEach(System.out::println);
    }

    public enum RegionFormat {
        ProvCityDistTownVillage("Prov|City|Dist|Town|Village", "省市县乡村(街道)"),
        ProvCityDistTownVillageFuzzy("Prov|City|Dist|Town|Village|Fuzzy", "省市县乡村(街道)模糊"),
        ProvCityDist("Prov|City|Dist", "省市县"),
        ProvCityDistFuzzy("Prov|City|Dist|Fuzzy", "省市县模糊"),
        ProvCityDistTown("Prov|City|Dist|Town", "省市县乡"),
        ProvCityDistTownFuzzy("Prov|City|Dist|Town|Fuzzy", "省市县乡模糊"),

        ProvCity("Prov|City", "省市"),
        ProvCityFuzzy("Prov|City|Fuzzy", "省市模糊"),
        CityDist("City|Dist", "市县"),
        CityDistFuzzy("City|Dist|Fuzzy", "市县模糊"),
        Prov("Prov", "省"),
        ProvFuzzy("Prov|Fuzzy", "省模糊"),
        City("City", "市"),
        CityFuzzy("City|Fuzzy", "市模糊"),
        Dist("Dist", "县"),
        DistFuzzy("Dist|Fuzzy", "县模糊"),
        Town("Town", "乡镇"),
        TownFuzzy("Town|Fuzzy", "乡镇模糊"),
        Village("Village", "村/街道"),
        VillageFuzzy("Village|Fuzzy", "村/街道模糊");

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }

        private final String name;
        private final String value;


        private RegionFormat(String value, String name) {
            this.name = name;
            this.value = value;
        }
    }

    @Test
    public void TestStringToEnum() {
        RegionFormat aa = Enum.valueOf(RegionFormat.class, "ProvCityDistTownVillage");
        RegionFormat aaa = Enum.valueOf(RegionFormat.class, "Prov|City|Dist|Town|Village");
        System.out.println(aaa);
    }
}
