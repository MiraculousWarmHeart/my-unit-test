package cnki.kg.demo;

import org.junit.jupiter.api.Test;

import org.neo4j.driver.v1.*;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.types.Path;
import org.neo4j.driver.v1.types.Relationship;
import org.neo4j.driver.v1.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.*;


@SpringBootTest
@WebAppConfiguration
public class TestSearchWeightGraph {
    @Autowired
    private Driver neo4jDriver;
    @Test
    public void searchWeight(){
        String cypher="match (n:`测试权重`{name:'项目名称'}),(m:`测试权重`{name:'设计速度'})\n" +
                "CALL apoc.algo.dijkstra(n,m,'RE','weight')yield path as path,weight as weight\n" +
                "return path,weight";
        StatementResult result = null;
        try (Session session = neo4jDriver.session()) {
            System.out.println(cypher);
            result = session.run(cypher);
            if (result.hasNext()) {
                List<Record> records = result.list();
                List<HashMap<String, Object>> ents = new ArrayList<HashMap<String, Object>>();
                List<HashMap<String, Object>> ships = new ArrayList<HashMap<String, Object>>();
                List<String> uuids = new ArrayList<String>();
                List<String> shipids = new ArrayList<String>();
                for (Record recordItem : records) {
                    List<Pair<String, Value>> f = recordItem.fields();
                    for (Pair<String, Value> pair : f) {
                        HashMap<String, Object> rships = new HashMap<String, Object>();
                        HashMap<String, Object> rss = new HashMap<String, Object>();
                        String typeName = pair.value().type().name();
                        if (typeName.equals("PATH")) {
                            Path path = pair.value().asPath();
                            Map<String, Object> startNodemap = path.start().asMap();
                            String startNodeuuid = String.valueOf(path.start().id());
                            if (!uuids.contains(startNodeuuid)) {
                                for (Map.Entry<String, Object> entry : startNodemap.entrySet()) {
                                    String key = entry.getKey();
                                    rss.put(key, entry.getValue());
                                }
                                rss.put("uuid", startNodeuuid);
                                uuids.add(startNodeuuid);
                                if (rss != null && !rss.isEmpty()) {
                                    ents.add(rss);
                                }
                            }
                            Map<String, Object> endNodemap = path.end().asMap();
                            String endNodeuuid = String.valueOf(path.end().id());
                            if (!uuids.contains(endNodeuuid)) {
                                for (Map.Entry<String, Object> entry : endNodemap.entrySet()) {
                                    String key = entry.getKey();
                                    rss.put(key, entry.getValue());
                                }
                                rss.put("uuid", endNodeuuid);
                                uuids.add(endNodeuuid);
                                if (rss != null && !rss.isEmpty()) {
                                    ents.add(rss);
                                }
                            }
                            Iterator<Relationship> reships = path.relationships().iterator();
                            while (reships.hasNext()) {
                                Relationship next = reships.next();
                                String uuid = String.valueOf(next.id());
                                if (!shipids.contains(uuid)) {
                                    String sourceid = String.valueOf(next.startNodeId());
                                    String targetid = String.valueOf(next.endNodeId());
                                    Map<String, Object> map = next.asMap();
                                    for (Map.Entry<String, Object> entry : map.entrySet()) {
                                        String key = entry.getKey();
                                        rships.put(key, entry.getValue());
                                    }
                                    rships.put("uuid", uuid);
                                    rships.put("sourceid", sourceid);
                                    rships.put("targetid", targetid);
                                    if (rships != null && !rships.isEmpty()) {
                                        ships.add(rships);
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            throw e;
        }
    }

}
