package cnki.kg.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

@SpringBootTest
public class TestLRU3 {
    @Test
    public void TestLru() {
        LRUCache<Integer, String> lru = new LRUCache<Integer, String>(5);
        lru.put(1, "a");
        lru.put(2, "b");
        lru.put(3, "c");
        lru.put(4, "d");
        lru.put(5, "e");
        System.out.println("原始链表为:" + lru.toString());

        lru.get(4);
        System.out.println("获取key为4的元素之后的链表:" + lru.toString());

        lru.put(6, "f");
        System.out.println("新添加一个key为6之后的链表:" + lru.toString());

        lru.remove(3);
        System.out.println("移除key=3的之后的链表:" + lru.toString());
    }


    public class LRUCache<K, V> {
        public class Node<K, V> {
            private K key;
            private V value;
            Node<K, V> prev;
            Node<K, V> next;

            private Node() {
                this.next = null;
                this.prev = null;
            }

            private Node(K key, V value) {
                this.key = key;
                this.value = value;
                this.next = null;
                this.prev = null;
            }
        }

        public class DoubleLinkList<K, V> {
            Node<K, V> head;
            Node<K, V> tail;

            DoubleLinkList() {
                this.head = new Node<>();
                this.tail = new Node<>();
                this.head.next = this.tail;
                this.tail.prev = this.head;
            }

            public void addHead(Node<K, V> node) {
                if (node == null) return;
                node.next = this.head.next;
                node.prev = this.head;
                this.head.next.prev = node;
                this.head.next = node;
            }

            public void removeNode(Node<K, V> node) {
                if (node == null) return;
                node.next.prev = node.prev;
                node.prev.next = node.next;
                node.prev = null;
                node.next = null;
            }

            public Node<K, V> getLastNode() {
                return this.tail.prev;
            }
            @Override
            public String toString() {
                StringBuilder sb = new StringBuilder();
                Node node = head.next;
                while (node != null&&node.next!=null) {
                    sb.append(String.format("%s:%s ", node.key, node.value));
                    node = node.next;
                }
                return sb.toString();
            }
        }

        private int capacity;

        private HashMap<K, Node<K, V>> map;
        private DoubleLinkList<K, V> list;

        private LRUCache(int capacity) {
            this.map = new HashMap<>();
            this.list = new DoubleLinkList<>();
            this.capacity = capacity;
        }

        public Node<K, V> get(K key) {
            if (!map.containsKey(key)) {
                return null;
            }
            Node<K, V> node = map.get(key);
            list.removeNode(node);
            list.addHead(node);
            return node;
        }

        public void put(K key, V value) {
            if (map.containsKey(key)) {
                Node<K, V> node = map.get(key);
                node.value = value;
                map.put(key, node);
                list.removeNode(node);
                list.addHead(node);
            } else {
                Node<K, V> newNode = new Node<>(key, value);
                if (map.size() == this.capacity) {
                    Node<K, V> lastNode = list.getLastNode();
                    map.remove(lastNode.key);
                    list.removeNode(lastNode);
                }
                map.put(key, newNode);
                list.addHead(newNode);

            }
        }

        public Node<K, V> remove(K key) {
            Node<K, V> node = map.get(key);
            if (node != null) {
                list.removeNode(node);
            }
            return node;
        }

        @Override
        public String toString() {
           return list.toString();
        }
    }
}
