package cnki.kg.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.locks.ReentrantLock;

@SpringBootTest
public class TestVolatile {
    @Test
    public void main(){
        Thread th=new Thread(new Runnable() {
            @Override
            public void run() {
                MyThread myThread = new MyThread();
                // 开启线程
                myThread.start();

                // 主线程执行
                for (; ; ) {
                    /*synchronized (myThread){
                        if (myThread.isFlag()) {
                            System.out.println("主线程访问到 flag 变量");
                        }
                    }*/
                    if (myThread.isFlag()) {
                        System.out.println("主线程访问到 flag 变量");
                    }
                }
            }
        });
        th.run();

    }

}
class MyThread extends Thread {

    private volatile boolean flag = false;

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 修改变量值
        flag = true;
        System.out.println("flag = " + flag);
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}