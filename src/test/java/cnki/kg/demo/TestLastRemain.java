package cnki.kg.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.stream.Collectors;

@SpringBootTest
public class TestLastRemain {
    @Test
    public void test(){
        int i = lastRemaining(100, 3);
        System.out.println(i);
    }
    public int lastRemaining(int n, int m) {
        ArrayList<Integer> list = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            list.add(i);
        }
        int idx = 0;
        while (n > 1) {
            idx = (idx + m - 1) % n;
            list.remove(idx);
            n--;
        }
        return list.get(0);
    }
    @Test
    public void search(){
        int[] nums={5,7,7,8,8,10};
        int target=8;
        int left =0,right = nums.length-1;
        int count = 0;
        while(left<right){
            int mid = (left+right)/2;
            if(nums[mid]>=target)
                right=mid;
            if(nums[mid]<target)
                left = mid+1;
        }
        while(left<nums.length&&nums[left++]==target){
            count++;
        }
        System.out.println(count);
    }
@Test
    public void ttk(){
    isStraight(new int[]{1,2,3,4,5});
    }
    public boolean isStraight(int[] nums) {
       /* int max=0;int min=13;
        Set<Integer> sets=new HashSet<Integer>();
        for (int i = 0; i < nums.length; i++) {
            if(nums[i]==0) continue;
            if(sets.contains(nums[i])) return false;
            sets.add(nums[i]);
            max=Math.max(nums[i],max);
            min=Math.min(nums[i],min);
        }
        return max-min<5;*/

        int joker = 0;
        Arrays.sort(nums); // 数组排序
        for(int i = 0; i < 4; i++) {
            if(nums[i] == 0) joker++; // 统计大小王数量
            else if(nums[i] == nums[i + 1]) return false; // 若有重复，提前返回 false
        }
        return nums[4] - nums[joker] < 5; // 最大牌 - 最小牌 < 5 则可构成顺子
    }
    @Test
    public void myPow(){
        myPow(2.00000F,-2);
    }
    public double myPow(double x, int n) {
        /*if(n==0) return 1.00000F;
        if(x==0) return 0;
        double res=1.00000F;
        if(n>0){
            for(int i=1;i<=n;i++){
                res*=x;
            }
        }else{
            for(int i=0;i>n;i--){
                res=res/x;
            }
        }
        return res;*/
        if(x == 0) return 0;
        long b = n;
        double res = 1.0;
        if(b < 0) {
            x = 1 / x;
            b = -b;
        }
        while(b > 0) {
            if((b & 1) == 1) res *= x;
            x *= x;
            b >>= 1;
        }
        return res;
    }
}
