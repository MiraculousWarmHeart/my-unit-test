package cnki.kg.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

@SpringBootTest
public class TestLRU {
    @Test
    public void lru() {
        LRUCache<Integer, String> lru = new LRUCache<Integer, String>(5);
        lru.put(1, "a");
        lru.put(2, "b");
        lru.put(3, "c");
        lru.put(4,"d");
        lru.put(5,"e");
        System.out.println("原始链表为:"+lru.toString());

        lru.get(4);
        System.out.println("获取key为4的元素之后的链表:"+lru.toString());

        lru.put(6,"f");
        System.out.println("新添加一个key为6之后的链表:"+lru.toString());

        lru.remove(3);
        System.out.println("移除key=3的之后的链表:"+lru.toString());
    }

    public class LRUCache<K, V> {
        public class Node<K, V> {
            K key;
            V value;
            Node<K, V> prev;
            Node<K, V> next;

            Node() {
                this.prev = this.next = null;
            }

            Node(K key, V value) {
                this.prev = this.next = null;
                this.key = key;
                this.value = value;
            }
        }

        public class DLinkList<K, V> {
            Node<K, V> head;
            Node<K, V> tail;

            DLinkList() {
                head = new Node<>();
                tail = new Node<>();
                head.next = tail;
                tail.prev = head;
            }

            public void addHead(Node<K, V> node) {
                if (node == null) return;
                node.next = this.head.next;
                node.prev = this.head;
                this.head.next.prev = node;
                this.head.next = node;

            }

            public void removeNode(Node<K, V> node) {
                if (node == null) return;
                node.prev.next = node.next;
                node.next.prev = node.prev;
                node.prev = null;
                node.next = null;

            }

            public Node<K, V> getLast() {
                return this.tail.prev;
            }

            public String toString() {
                Node<K, V> head = this.head;
                StringBuilder builder = new StringBuilder();
                while (head != null) {
                    builder.append(String.format("{%s:%s}", head.key, head.value));
                    head = head.next;
                }
                return builder.toString();
            }
        }

        private int size;
        private Map<K, Node<K, V>> maps;
        private DLinkList<K, V> list;

        LRUCache(int size) {
            this.size = size;
            maps = new HashMap<>();
            list = new DLinkList<>();
        }

        public Node<K, V> get(K key) {
            if (!maps.containsKey(key)) {
                return null;
            }
            Node<K, V> node = maps.get(key);
            list.removeNode(node);
            list.addHead(node);
            return node;
        }

        public void put(K key, V value) {
            //存在则更新
            if (maps.containsKey(key)) {
                Node<K, V> oldNode = maps.get(key);
                oldNode.value = value;
                maps.put(key, oldNode);
                list.removeNode(oldNode);
                list.addHead(oldNode);
            } else {
                Node<K, V> newNode = new Node<>(key, value);
                if (maps.size() == size) {
                    Node<K, V> lastNode = list.getLast();
                    list.removeNode(lastNode);
                }
                list.addHead(newNode);
                maps.put(key, newNode);
            }
        }

        public void remove(K key) {
            if(maps.containsKey(key)){
                Node<K, V> node = maps.get(key);
                if(node!=null){
                    list.removeNode(node);
                }
            }

        }

        public String toString() {
            return list.toString();
        }
    }
    class LRUCache2 extends LinkedHashMap<Integer, Integer> {
        private int capacity;

        public LRUCache2(int capacity) {
            super(capacity, 0.75F, true);
            this.capacity = capacity;
        }

        public int get(int key) {
            return super.getOrDefault(key, -1);
        }

        public void put(int key, int value) {
            super.put(key, value);
        }

        @Override
        protected boolean removeEldestEntry(Map.Entry<Integer, Integer> eldest) {
            return size() > capacity;
        }
    }

}
