package cnki.kg.demo;

import cnki.kg.demo.util.Neo4jUtil;
import cnki.kg.demo.util.SqlserverUtils;
import cnki.kg.demo.util.StringUtil;
import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.Test;

import org.neo4j.driver.v1.StatementResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@SpringBootTest
@WebAppConfiguration
public class TestCreateGraph3 {
    @Autowired
    Neo4jUtil neo4jUtil;
    @Autowired
    SqlserverUtils utils;
    @Autowired
    @Qualifier("mysqlJdbcTemplate")
    protected JdbcTemplate mysqlJdbcTemplate;

    @Autowired
    @Qualifier("sqlserverJdbcTemplate")
    protected JdbcTemplate sqlserverJdbcTemplate;

    private final Integer concept=1;
    private final Integer entity=0;
    private final Integer attribute=2;
    private final Integer entityValue=3;
    @Test
    public void createXMXX() {
        String domain="交通全网检索业务图谱";
       String sqlBdmc="SELECT XMJC,TZMS,JSZT,XMYZ from xmjbxx ";
        String[] xmyzs=new String[]{"贵州高速公路集团有限公司","贵州江习古高速公路开发有限公司","贵州交通建设集团有限公司", "贵州省公路局高速公路建设办公室","中交集团","中铁建集团"};
        String[] jszt=new String[]{"交工","在建"};
        String[] TZMS=new String[]{"BOT","PPP"};
        //String sqlBdmc = "select DISTINCT(XMJC) as XMJC from xmjbxx";
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        //创建项目简称
        /* for (Map<String, Object> xmItem : mapList) {//创建项目简称
            String xmjc=xmItem.get("XMJC")!=null?xmItem.get("XMJC").toString():"";
            String tzms=xmItem.get("TZMS")!=null?xmItem.get("TZMS").toString():"";
            String jszt=xmItem.get("JSZT")!=null?xmItem.get("JSZT").toString():"";
            String xmyz=xmItem.get("XMYZ")!=null?xmItem.get("XMYZ").toString():"";
            HashMap<String, Object> nodeql=new HashMap<>();
            String  cypherSql = String.format("create (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'%s',tzms:'%s',jszt:'%s',xmyz:'%s'}) return n", domain, xmjc,entity,xmjc,tzms,jszt,xmyz);
            List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
        }
        //创建项目业主

        for (String xmyz : xmyzs) {
            String  cypherSql = String.format("create (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'%s'}) return n", domain, xmyz,entity,"");
            List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
        }
        //创建投资模式
        String[] TZMS=new String[]{"BOT","PPP"};
        for (String m : TZMS) {
            String cypherSql = String.format("create (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'%s'}) return n", domain, m, entity, "");
            List<HashMap<String, Object>> nodesQl = neo4jUtil.GetGraphNode(cypherSql);
        }
        //创建建设状态
        String[] jszt=new String[]{"交工","在建"};
        for (String m : jszt) {
            String cypherSql = String.format("create (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'%s'}) return n", domain, m, entity, "");
            List<HashMap<String, Object>> nodesQl = neo4jUtil.GetGraphNode(cypherSql);
        }
        //根据项目简称分别创建项目的 "标段","桥梁","隧道","基本信息","规划设计","招投标","实施","变更","运营","管理"
        //先查询项目简称节点
        String cyp="MATCH (n:`交通全网检索业务图谱`) where n.xmjc is not null and n.xmjc <>''  return * ";
        List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cyp);
        if(nodesQl!=null&&nodesQl.size()>0){
            for (HashMap<String, Object> mp : nodesQl) {
                String uuid=mp.get("uuid").toString();
                String name=mp.get("name").toString();
                System.out.println("开始创建："+uuid+":"+name);
                createRoterInformation(domain,uuid,name);
            }
        }*/
        String cypherSqlroot = String.format("merge (n:`%s`{name:'%s',type:%s,sortcode:0,isleaf:0}) return n", domain, "项目",concept);
        List<HashMap<String, Object>>  rootnodes=neo4jUtil.GetGraphNode(cypherSqlroot);
        for (HashMap<String, Object> rootnode : rootnodes) {
            String rootId=rootnode.get("uuid").toString();
            String rootName=rootnode.get("name").toString();
            String[] level1data=new String[]{"项目业主","投资模式","建设状态"};
            //String[] level1data=new String[]{"投资模式","建设状态"};

            for (String litem : level1data) {

                //HashMap<String, Object> nodeItem= createNodeAndLink(attribute,domain,litem,rootId,"");
                if(litem.equals("项目业主")){
                    //创建业主数据
                    String  cypherSql = String.format("create (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'%s'}) return n", domain, litem,attribute,"");
                    List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
                    if(nodesQl!=null&&nodesQl.size()>0) {
                        HashMap<String, Object> nodeItem = nodesQl.get(0);
                        for (String xmyz : xmyzs) {
                            String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND m.name = '%s' "
                                    + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), nodeItem.get("uuid"),xmyz, "");
                            neo4jUtil.excuteCypherSql(LinkcypherSql);
                            List<Map<String, Object>> xmyzItems = mapList.stream().filter(n->n.get("xmyz")!=null&&n.get("xmyz").toString().equals(xmyz)).collect(Collectors.toList());
                            if(xmyzItems!=null&&xmyzItems.size()>0){
                                for (Map<String, Object> xmyzItem : xmyzItems) {
                                    String LinkcypherSql2 = String.format("MATCH (n:`%s`),(m:`%s`) WHERE n.name='%s' AND m.name = '%s' "
                                            + "merge (n)<-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), xmyz, xmyzItem.get("xmjc"), "");
                                    neo4jUtil.excuteCypherSql(LinkcypherSql2);
                                }
                            }
                        }
                    }

                }
                if(litem.equals("投资模式")){ String  cypherSql = String.format("create (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'%s'}) return n", domain, litem,attribute,"");
                    List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
                    if(nodesQl!=null&&nodesQl.size()>0) {
                        HashMap<String, Object> nodeItem = nodesQl.get(0);
                        for (String tzms : TZMS) {
                            String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND m.name = '%s' "
                                    + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), nodeItem.get("uuid"), tzms, "");
                            neo4jUtil.excuteCypherSql(LinkcypherSql);
                            List<Map<String, Object>> tzmsItems = mapList.stream().filter(n -> n.get("tzms") != null && n.get("tzms").toString().equals(tzms)).collect(Collectors.toList());
                            if (tzmsItems != null && tzmsItems.size() > 0) {
                                for (Map<String, Object> tzmsItem : tzmsItems) {
                                    String LinkcypherSql2 = String.format("MATCH (n:`%s`),(m:`%s`) WHERE n.name='%s' AND m.name = '%s' "
                                            + "merge (n)<-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), tzms, tzmsItem.get("xmjc"), "");
                                    neo4jUtil.excuteCypherSql(LinkcypherSql2);
                                }
                            }
                        }
                    }
                }
                if(litem.equals("建设状态")){
                    String  cypherSql = String.format("create (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'%s'}) return n", domain, litem,attribute,"");
                    List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
                    if(nodesQl!=null&&nodesQl.size()>0) {
                        HashMap<String, Object> nodeItem = nodesQl.get(0);
                        for (String jzt : jszt) {
                            String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND m.name = '%s' "
                                    + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), nodeItem.get("uuid"), jzt, "");
                            neo4jUtil.excuteCypherSql(LinkcypherSql);
                            List<Map<String, Object>> jsztItems = mapList.stream().filter(n -> n.get("jszt") != null && n.get("jszt").toString().equals(jzt)).collect(Collectors.toList());
                            if (jsztItems != null && jsztItems.size() > 0) {
                                for (Map<String, Object> jsztItem : jsztItems) {
                                    String LinkcypherSql2 = String.format("MATCH (n:`%s`),(m:`%s`) WHERE n.name='%s' AND m.name = '%s' "
                                            + "merge (n)<-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), jzt, jsztItem.get("xmjc"), "");
                                    neo4jUtil.excuteCypherSql(LinkcypherSql2);
                                }
                            }
                        }
                    }
                }
            }
        }
        //,"工可批复单位"
        createGKPFDW(domain);
    }
    @Test
    public void createXMYZBaseInfo() {
        String domain = "交通全网检索业务图谱";
        String[] xmyzs = new String[]{"贵州高速公路集团有限公司", "贵州江习古高速公路开发有限公司", "贵州交通建设集团有限公司", "贵州省公路局高速公路建设办公室", "中交集团", "中铁建集团"};
        String[] baseInfo = new String[]{"企业名称", "电话", "法人", "地址"};
        for (String xmyz : xmyzs) {
            String cypherSql1 = String.format("merge (n:`%s`{name:'企业基本信息',type:%s,sortcode:999,isleaf:0,xmjc:'',gsmc:'%s'}) return n", domain, concept,xmyz);
            List<HashMap<String, Object>> nodesQl1 = neo4jUtil.GetGraphNode(cypherSql1);
            for (String gs : baseInfo) {
                if (nodesQl1 != null && nodesQl1.size() > 0) {
                    HashMap<String, Object> nodeItem1 = nodesQl1.get(0);
                    String LinkcypherSql1 = String.format("MATCH (n:`%s`),(m:`%s`) WHERE n.name = '%s' AND id(m)=%s "
                            + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), xmyz,nodeItem1.get("uuid"), "");
                    neo4jUtil.excuteCypherSql(LinkcypherSql1);
                    String cypherSql = String.format("create (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'',gsmc:'%s'}) return n", domain, gs, attribute,xmyz);
                    List<HashMap<String, Object>> nodesQl = neo4jUtil.GetGraphNode(cypherSql);
                    if (nodesQl != null && nodesQl.size() > 0) {
                        HashMap<String, Object> nodeItem = nodesQl.get(0);
                        String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n) = %s AND id(m)=%s "
                                + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), nodeItem1.get("uuid"),nodeItem.get("uuid"), "");
                        neo4jUtil.excuteCypherSql(LinkcypherSql);
                    }
                }


            }
        }
    }
    @Test
    public void deleteXMYZRelation() {
        String domain = "交通全网检索业务图谱";
        String sqlBdmc = "SELECT XMJC,TZMS,JSZT,XMYZ from xmjbxx ";
        String[] xmyzs = new String[]{"贵州高速公路集团有限公司", "贵州江习古高速公路开发有限公司", "贵州交通建设集团有限公司", "贵州省公路局高速公路建设办公室", "中交集团", "中铁建集团"};
        for (String xmyz : xmyzs) {
            String LinkcypherSql = String.format("MATCH (n:`%s`)-[r]->(m) WHERE  n.name = '%s' detach delete r",domain.trim(),xmyz);
            neo4jUtil.excuteCypherSql(LinkcypherSql);
        }
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        String  cypherSql = String.format("merge (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'%s'}) return n", domain, "项目业主",attribute,"");
        List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
        if(nodesQl!=null&&nodesQl.size()>0) {
            HashMap<String, Object> nodeItem = nodesQl.get(0);
            for (String xmyz : xmyzs) {
                String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND m.name = '%s' "
                        + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), nodeItem.get("uuid"),xmyz, "");
                neo4jUtil.excuteCypherSql(LinkcypherSql);
                List<Map<String, Object>> xmyzItems = mapList.stream().filter(n->n.get("xmyz")!=null&&n.get("xmyz").toString().equals(xmyz)).collect(Collectors.toList());
                if(xmyzItems!=null&&xmyzItems.size()>0){
                    for (Map<String, Object> xmyzItem : xmyzItems) {
                        String LinkcypherSql2 = String.format("MATCH (n:`%s`),(m:`%s`) WHERE n.name='%s' AND m.name = '%s' "
                                + "merge (n)<-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), xmyz, xmyzItem.get("xmjc"), "");
                        neo4jUtil.excuteCypherSql(LinkcypherSql2);
                    }
                }
            }
        }
    }
    @Test
    public void deleteJSZTRelation() {
        String domain = "交通全网检索业务图谱";
        String sqlBdmc = "SELECT XMJC,TZMS,JSZT,XMYZ from xmjbxx ";
        String[] jszt=new String[]{"交工","在建"};
        for (String xmyz : jszt) {
            String LinkcypherSql = String.format("MATCH (n:`%s`)-[r]->(m) WHERE  n.name = '%s' detach delete r",domain.trim(),xmyz);
            neo4jUtil.excuteCypherSql(LinkcypherSql);
        }
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        //String  cypherSql = String.format("merge (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'%s'}) return n", domain, "建设状态",attribute,"");
        String  cypherSql = "MATCH (n:`交通全网检索业务图谱`)-[r]->(m)  where n.name='项目' and m.name='建设状态'   return m";
        List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
        if(nodesQl!=null&&nodesQl.size()>0) {
            HashMap<String, Object> nodeItem = nodesQl.get(0);
            for (String xmyz : jszt) {
                String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND m.name = '%s' "
                        + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), nodeItem.get("uuid"),xmyz, "");
                neo4jUtil.excuteCypherSql(LinkcypherSql);
                List<Map<String, Object>> xmyzItems = mapList.stream().filter(n->n.get("jszt")!=null&&n.get("jszt").toString().equals(xmyz)).collect(Collectors.toList());
                if(xmyzItems!=null&&xmyzItems.size()>0){
                    for (Map<String, Object> xmyzItem : xmyzItems) {
                        String LinkcypherSql2 = String.format("MATCH (n:`%s`),(m:`%s`) WHERE n.name='%s' AND m.name = '%s' "
                                + "merge (n)<-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), xmyz, xmyzItem.get("xmjc"), "");
                        neo4jUtil.excuteCypherSql(LinkcypherSql2);
                    }
                }
            }
        }
    }
    @Test
    public void deletetzmsRelation() {
        String domain = "交通全网检索业务图谱";
        String sqlBdmc = "SELECT XMJC,TZMS,JSZT,XMYZ from xmjbxx ";
        String[] TZMS=new String[]{"BOT","PPP"};
        for (String xmyz : TZMS) {
            String LinkcypherSql = String.format("MATCH (n:`%s`)-[r]->(m) WHERE  n.name = '%s' detach delete r",domain.trim(),xmyz);
            neo4jUtil.excuteCypherSql(LinkcypherSql);
        }
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        //String  cypherSql = String.format("merge (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'%s'}) return n", domain, "建设状态",attribute,"");
        String  cypherSql = "MATCH (n:`交通全网检索业务图谱`)-[r]->(m)  where n.name='项目' and m.name='投资模式'   return m";
        List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
        if(nodesQl!=null&&nodesQl.size()>0) {
            HashMap<String, Object> nodeItem = nodesQl.get(0);
            for (String xmyz : TZMS) {
                String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND m.name = '%s' "
                        + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), nodeItem.get("uuid"),xmyz, "");
                neo4jUtil.excuteCypherSql(LinkcypherSql);
                List<Map<String, Object>> xmyzItems = mapList.stream().filter(n->n.get("tzms")!=null&&n.get("tzms").toString().equals(xmyz)).collect(Collectors.toList());
                if(xmyzItems!=null&&xmyzItems.size()>0){
                    for (Map<String, Object> xmyzItem : xmyzItems) {
                        String LinkcypherSql2 = String.format("MATCH (n:`%s`),(m:`%s`) WHERE n.name='%s' AND m.name = '%s' "
                                + "merge (n)<-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), xmyz, xmyzItem.get("xmjc"), "");
                        neo4jUtil.excuteCypherSql(LinkcypherSql2);
                    }
                }
            }
        }
    }
    @Test
    public void respairJSZTRelation() {
        String domain = "交通全网检索业务图谱";
        String sqlBdmc = "SELECT XMJC,TZMS,JSZT,XMYZ from xmjbxx ";
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        //String  cypherSql = String.format("merge (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'%s'}) return n", domain, "建设状态",attribute,"");
        String  cypherSql = "MATCH (n:`交通全网检索业务图谱`) where n.name='建设状态'  and id(n)<>96008960  return *";
        List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
        if(nodesQl!=null&&nodesQl.size()>0) {
            for (HashMap<String, Object> nodeItem : nodesQl) {
                String xmjc=nodeItem.get("xmjc")!=null?nodeItem.get("xmjc").toString():"";
                String uuid=nodeItem.get("uuid").toString();
                if(StringUtil.isBlank(xmjc)) continue;
                List<Map<String, Object>> xmyzItems = mapList.stream().filter(n->n.get("xmjc")!=null&&n.get("xmjc").toString().equals(xmjc)).collect(Collectors.toList());
                if(xmyzItems!=null&&xmyzItems.size()>0){
                    if(xmyzItems.get(0).get("JSZT")!=null){
                        String jsz=xmyzItems.get(0).get("JSZT").toString();
                        String LinkcypherSql2 = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND m.name = '%s' "
                                + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), uuid, jsz, "");
                        neo4jUtil.excuteCypherSql(LinkcypherSql2);
                    }

                }
            }
        }
    }
    @Test
    public void respairTZMSRelation() {
        String domain = "交通全网检索业务图谱";
        String sqlBdmc = "SELECT XMJC,TZMS,JSZT,XMYZ from xmjbxx ";
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;

        String  cypherSql = "MATCH (n:`交通全网检索业务图谱`) where n.name='投资模式' and id(n)<>96009634    return *";
        List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
        if(nodesQl!=null&&nodesQl.size()>0) {
            for (HashMap<String, Object> nodeItem : nodesQl) {
                String xmjc=nodeItem.get("xmjc")!=null?nodeItem.get("xmjc").toString():"";
                String uuid=nodeItem.get("uuid").toString();
                if(StringUtil.isBlank(xmjc)) continue;
                List<Map<String, Object>> xmyzItems = mapList.stream().filter(n->n.get("xmjc")!=null&&n.get("xmjc").toString().equals(xmjc)).collect(Collectors.toList());
                if(xmyzItems!=null&&xmyzItems.size()>0){
                    if(xmyzItems.get(0).get("TZMS")!=null){
                        String jsz=xmyzItems.get(0).get("TZMS").toString();
                        String LinkcypherSql2 = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND m.name = '%s' "
                                + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), uuid, jsz, "");
                        neo4jUtil.excuteCypherSql(LinkcypherSql2);
                    }

                }
            }
        }
    }
    public void createRoterInformation(String domain,String pid,String xmjc) {
        String [] items=new String[]{"基本信息","规划设计","桥梁","隧道","标段","招投标","实施","变更","运营","管理"};
        String xmxxsql = String.format("SELECT JSZT as 建设状态,GKGSPFJE as 工可批复金额,TZMS as 投资模式,XMMC as 项目名称,起点,终点,途经,CD as 车道,SJSU as 设计速度,ZTLJ as 整体路基,ZHXJ as 载荷新建,ZXQC as 主线全长,ZXLMJGLX as 主线路面结构类型,PZKGRQ as 批准开工日期,SJKGRQ as 实际开工日期 FROM XMJBXX where XMJC='%s'",xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(xmxxsql);
        if(mapList==null||mapList.size()==0) return;
        Map<String, Object> xmjbxxModel=mapList.get(0);
        for (String item : items) {
            HashMap<String, Object> node= createNodeAndLink(concept,domain,item,pid,xmjc);
            if(item.equals("基本信息")){
                createBaseInformation(xmjbxxModel,domain,node.get("uuid").toString(),item,xmjc);
            }
            else if(item.equals("桥梁")){
                //创建桥梁信息
                createQiaoliangInformation(domain,node.get("uuid").toString(),item,xmjc);
            }
            else if(item.equals("隧道")){
                //创建隧道信息
                createSuiDaoInformation(domain,node.get("uuid").toString(),item,xmjc);
            }
            else if(item.equals("标段")){
                //创建标段基本信息
                createBiaoduanInformation(domain,node.get("uuid").toString(),item,xmjc);
            }
            else if(item.equals("规划设计")){
                createGuiHuaShejiInformation(domain,node.get("uuid").toString(),item,xmjc);
            }
            else if(item.equals("招投标")){
                createZhaotoubiaoInformation(domain,node.get("uuid").toString(),item,xmjc);
            }
            else if(item.equals("变更")){
                createBianGengInformation(domain,node.get("uuid").toString(),item,xmjc);
            }
            else if(item.equals("实施")){

            }
            else if(item.equals("运营")){

            }
            else if(item.equals("管理")){

            }
        }
    }

    public void createGKPFDW(String domain){
        //获取所有的项目业主
        String[] TZMS=new String[]{"国高","省高","地高"};
        String sqlGKPWDW="'HB省发改委员会','贵州省发展改革委员会','贵州省发展和改革委员会','贵州省发展与改革委员会','贵州省发展与改革委员会','省发改委'";
        for (String m : TZMS) {
            String cypherSqlroot = String.format("merge (n:`%s`{name:'%s',type:%s}) return n", domain, m,concept);
            List<HashMap<String, Object>> nodeItemList=neo4jUtil.GetGraphNode(cypherSqlroot);
            if(nodeItemList!=null&&nodeItemList.size()>0){
                HashMap<String, Object> nodeItem=nodeItemList.get(0);
                //创建投资模式为xxx的项目
                if(m.equals("国高")){
                    sqlGKPWDW="'国家发展和改革委员会','中国人民共和国国家发展和改革委员会','中华人民共和国国家发展和改革委员会'";
                }
                if(m.equals("地高")){
                    //sqlGKPWDW="'国家发展和改革委员会','中国人民共和国国家发展和改革委员会','中华人民共和国国家发展和改革委员会'";
                    continue;
                }
                String tzmssql=String.format("SELECT XMJC from XMJBXX WHERE GKPFDW in (%s) ",sqlGKPWDW);
                List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(tzmssql);
                if(mapList==null||mapList.size()==0) return;
                List<String> xmjcs=mapList.stream().map(n->n.get("XMJC").toString()).collect(Collectors.toList());
                for (String xmjc : xmjcs) {
                    linkNode(domain, xmjc, m, nodeItem.get("uuid").toString());
                }
            }

        }
    }
    public void createXMYZ(String domain,String pid,String pname){
        //获取所有的项目业主
        String[] xmyzs=new String[]{"贵州高速公路集团有限公司","贵州江习古高速公路开发有限公司","贵州交通建设集团有限公司", "贵州省公路局高速公路建设办公室","中交集团","中铁建集团"};
        for (String xmyz : xmyzs) {
            HashMap<String, Object> nodeItem= mergeNodeAndLink(entity,domain,xmyz,pid,"");
            //创建业主负责的项目
            createYZXM(domain,nodeItem.get("uuid").toString(),xmyz);
            //todo 创建项目业主的企业信息
        }
    }
    public void createYZXM(String domain,String pid,String pname){
        //获取业主负责的项目
        String sqlBdmc = String.format("select DISTINCT(XMJC) as XMJC from xmjbxx where XMYZ='%s' ",pname);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        List<String> xmjcs=mapList.stream().map(n->n.get("XMJC").toString()).collect(Collectors.toList());
        for (String xmjc : xmjcs) {
            HashMap<String, Object> nodeItem= mergeNodeAndLink(entity,domain,xmjc,pid,xmjc);
            //创建路线基本信息
            createRoterInformation(domain,nodeItem.get("uuid").toString(),xmjc);
        }
    }

    public void createBiaoduanInformation(String domain,String pid,String pname,String xmjc) {
        //todo 标段编号换名称
        String sqlBdmc = String.format("select DISTINCT(BDMC) as BDMC,BDBH,XMMC from BDJBXX where XMJC='%s'",xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        Integer i=1;
        for (Map<String, Object> item : mapList) {
            String bdbh=item.get("BDBH").toString();
            String bdmc=item.get("BDMC").toString();
            String xmmc=item.get("XMMC").toString();
            HashMap<String, Object> node= mergeNodeAndLink(entity,domain,bdmc,pid,xmjc);
            createBiaoduanDetail(domain,node.get("uuid").toString(),xmjc,bdbh,xmmc);
        }
    }
    public void createQiaoliangInformation(String domain,String pid,String pname,String xmjc) {
        String sqlBdmc = String.format("select * from XMJBXX where XMJC='%s'",xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        Map<String, Object> item=mapList.get(0);
        String[] nodes=new String []{"桥梁总长度","桥梁总数","涵洞道数","特大桥长度","特大桥数量"
                ,"大桥长度","大桥数量","小桥长度","小桥数量","中桥长度","中桥数量"};
        for (String node : nodes) {
            HashMap<String, Object> nodeItem= createNodeAndLink(attribute,domain,node,pid,xmjc);
            String parentname=nodeItem.get("name").toString();
            String parentuuid=nodeItem.get("uuid").toString();
            String nodename=item.get(node)!=null?item.get(node).toString():"暂无数据";
            createNodeAndLink(entity,domain,nodename,parentuuid,xmjc);
        }

    }
    public void createSuiDaoInformation(String domain,String pid,String pname,String xmjc) {
        String sqlBdmc = String.format("select * from XMJBXX where XMJC='%s'",xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        Map<String, Object> item=mapList.get(0);
        String[] nodes=new String []{"隧道总长度","隧道总数","特长隧道长度","特长隧道数量"
                ,"长隧道长度","长隧道数量","中隧道长度","中隧道数量","短隧道长度","短隧道数量","桥隧比"};
        for (String node : nodes) {
            HashMap<String, Object> nodeItem= createNodeAndLink(attribute,domain,node,pid,xmjc);
            String parentname=nodeItem.get("name").toString();
            String parentuuid=nodeItem.get("uuid").toString();
            String nodename=item.get(node)!=null?item.get(node).toString():"暂无数据";
            createNodeAndLink(entity,domain,nodename,parentuuid,xmjc);
        }
    }
    public void createBaseInformation( Map<String, Object> xmjbxxModel,String domain,String pid,String pname,String xmjc) {
        String[] nodes=new String []{"项目名称","起点","终点","途经","投资模式","建设状态"
                ,"车道","设计速度","整体路基","载荷新建","主线全长","主线路面结构类型","批准开工日期","实际开工日期"};
        for (String node : nodes) {
            HashMap<String, Object> nodeItem= createNodeAndLink(attribute,domain,node,pid,xmjc);
            String parentuuid=nodeItem.get("uuid").toString();
            String nodename=xmjbxxModel.get(node)!=null?xmjbxxModel.get(node).toString():"暂无数据";
            if(node.equals("投资模式")||node.equals("建设状态")){
                String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND id(m) = %s "
                        + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), pid, parentuuid, "");
                neo4jUtil.excuteCypherSql(LinkcypherSql);
            }else{
                createNodeAndLink(entity,domain,nodename,parentuuid,xmjc);
            }
        }
    }
    public void createGuiHuaShejiInformation(String domain,String pid,String pname,String xmjc) {
        String sqlBdmc = String.format("SELECT TZGS as 投资估算,ZBJ as 资本金,ZZZBL as 资本金占总投资比例 ,YHDK as 银行贷款,YZZBL as 银行贷款占总投资比例," +
                "GKGSPFJE as 工可批复金额,GSPFDW as 概算批复单位,GSPFWH as 概算批复文号,GSGSPFJE as 概算批复金额,SJDW as 设计单位,XMYZ as 项目业主 FROM XMJBXX where XMJC='%s'",xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        Map<String, Object> item=mapList.get(0);
        String[] nodes=new String []{"投资估算","资本金","资本金占总投资比例","银行贷款"
                ,"银行贷款占总投资比例","工可批复金额","概算批复单位","概算批复文号","概算批复金额","设计单位","项目业主"};
        for (String node : nodes) {
            HashMap<String, Object> nodeItem= createNodeAndLink(attribute,domain,node,pid,xmjc);
            String parentname=nodeItem.get("name").toString();
            String parentuuid=nodeItem.get("uuid").toString();
            String nodename=item.get(node)!=null?item.get(node).toString():"暂无数据";
            createNodeAndLink(entity,domain,nodename,parentuuid,xmjc);
        }
    }
    public void createZhaotoubiaoInformation(String domain,String pid,String pname,String xmjc) {
        String cypherSql = String.format("merge (n:`%s`{name:'%s',pid:'%s'}) return n", domain, "合同名称",pid);
        List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
        HashMap<String, Object> nodeql=nodesQl.get(0);
       String hthpid=nodeql.get("uuid").toString();
       String hthname=nodeql.get("name").toString();
        //String sqlBdmc = String.format("SELECT HTH as 合同号 FROM ZTBJDSJ where XMJC='%s' GROUP BY HTH ",xmjc);
        String sqlBdmc = String.format("SELECT HTMC as 合同名称 FROM ZTBJDSJ where XMJC='%s' GROUP BY HTMC ",xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        for (Map<String, Object> om : mapList) {
            String hth=om.get("合同名称")!=null?om.get("合同名称").toString():"";
            HashMap<String, Object> htModel= createNodeAndLink(entity,domain,hth,hthpid,xmjc);
            String hthDetailpid=htModel.get("uuid").toString();
            String hthDetailname=htModel.get("name").toString();
            String[] nodes=new String []{"合同名称","甲方单位","乙方单位","签约合同金额","清单金额"};
            for (String node : nodes) {
                String htdetail = String.format("SELECT top 1 HTH as 合同号,HTMC as 合同名称 ,JFDW as 甲方单位,YFDW as 乙方单位,QY_HTJE as 签约合同金额,HTLX as 合同类型 FROM ZTBJDSJ where XMJC='%s' and hth='%s' ",xmjc,hth);
                List<Map<String, Object>> htdetailMap = sqlserverJdbcTemplate.queryForList(sqlBdmc);
                if(htdetailMap==null||mapList.size()==0) return;
                Map<String, Object> htmap=htdetailMap.get(0);
                HashMap<String, Object> nodeItem= createNodeAndLink(attribute,domain,node,hthDetailpid,xmjc);
                String nodename="";
                if(node.equals("清单金额")){
                    String htjesql=String.format("SELECT HTH,SUM(QDJE) as 清单金额 FROM ZTBJDSJ where XMJC='%s' and hth='%s'  GROUP BY HTH",xmjc,hth);
                    List<Map<String, Object>> htjeMap = sqlserverJdbcTemplate.queryForList(sqlBdmc);
                    if(htjeMap==null||htjeMap.size()==0) return;
                    Map<String, Object> htjeModel=htjeMap.get(0);
                    nodename=htmap.get("清单金额")!=null?htmap.get("清单金额").toString():"";
                    createNodeAndLink(entity,domain,nodename,nodeItem.get("uuid").toString(),xmjc);
                }else{
                    nodename=htmap.get(node)!=null?htmap.get(node).toString():"";
                    if(node.equals("签约合同金额")||node.equals("合同名称")){
                        createNodeAndLink(entity,domain,nodename,nodeItem.get("uuid").toString(),xmjc);
                    }else{
                        mergeNodeAndLink(entity,domain,nodename,nodeItem.get("uuid").toString(),"");
                    }
                }

            }
        }
    }
    public void createBianGengInformation(String domain,String pid,String pname,String xmjc) {
        String sqlBdmc = String.format("SELECT XMMC as 项目名称,XMJC as 项目简称,BGZFY as 变更总费用,XMYZ as 项目业主 from XMBGHZ where XMJC='%s'",xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        Map<String, Object> item=mapList.get(0);
        String[] nodes=new String []{"项目名称","变更总费用","项目业主","项目简称"};
        for (String node : nodes) {
            if(node.equals("项目简称")){
                linkNode(domain,node,pname,pid);
            }else{
                HashMap<String, Object> nodeItem= createNodeAndLink(attribute,domain,node,pid,xmjc);
                String nodename=item.get(node)!=null?item.get(node).toString():"暂无数据";
                createNodeAndLink(entity,domain,nodename,nodeItem.get("uuid").toString(),xmjc);
            }
        }
    }
    private HashMap<String, Object> createNodeAndLink(Integer type,String domain,String nodeName,String pid,String xmjc){
        HashMap<String, Object> nodeql=new HashMap<>();
        String  cypherSql = String.format("create (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'%s'}) return n", domain, nodeName,type,xmjc);
        List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
        if(nodesQl!=null&&nodesQl.size()>0){
            nodeql=nodesQl.get(0);
            String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND id(m) = %s "
                    + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), pid, nodeql.get("uuid"), "");
            neo4jUtil.excuteCypherSql(LinkcypherSql);
        }
        return nodeql;
    }
    private HashMap<String, Object> mergeNodeAndLink(Integer type,String domain,String nodeName,String pid,String xmjc){
        HashMap<String, Object> nodeql=new HashMap<>();
        String cypherSql = String.format("merge (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'%s'}) return n", domain, nodeName,type,xmjc);
        List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
        if(nodesQl!=null&&nodesQl.size()>0) {
            nodeql=nodesQl.get(0);
            String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND id(m) = %s "
                    + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), pid, nodeql.get("uuid"), "");
            neo4jUtil.excuteCypherSql(LinkcypherSql);
        }
        return nodeql;
    }
    private HashMap<String, Object> linkNode(String domain,String nodeName,String prefix,String pid){
        HashMap<String, Object> nodeql=new HashMap<>();
        String cypherSql = String.format("match (n:`%s`) where n.name='%s' return n", domain, nodeName);
        List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
        if(nodesQl!=null&&nodesQl.size()>0) {
            nodeql=nodesQl.get(0);
            String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND id(m) = %s "
                    + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), pid, nodeql.get("uuid"), "");
            neo4jUtil.excuteCypherSql(LinkcypherSql);
        }
        return nodeql;
    }
    public void createBiaoduanDetail(String domain,String pid,String xmjc,String bdbh,String xmmc) {
       String bdsql=String.format("SELECT XMJC,BDBH,JH_KGRQ as 计划开工日期,JH_WGRQ as 计划完工日期,QY_HTJE as 合同金额,SGDW as 施工单位,JFDW as 甲方单位,YFDW as 乙方单位, JLDW as 监理单位 " +
               "from BDJBXX WHERE BDBH='%s' and XMJC='%s'",bdbh,xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(bdsql);
        if(mapList==null||mapList.size()==0) return;
        Map<String, Object> bditem=mapList.get(0);
        String[] items=new String[]{"计划开工日期","计划完工日期","合同金额","施工单位","甲方单位","乙方单位","监理单位","变更","本期未完成金额","标段基本信息"};
        for (String item : items) {
            HashMap<String, Object> node= createNodeAndLink(attribute,domain,item,pid,xmjc);
            if(item.equals("变更")){
                createBiaoduanBiangengInfo(domain,node.get("uuid").toString(),item,xmmc,bdbh,xmjc);
            }
            else if(item.equals("本期未完成金额")){
                createBenqiweiwanchengjinge(domain,node.get("uuid").toString(),item,xmmc,bdbh);
            }
            else if(item.equals("标段基本信息")){
                createBiaoduanBaseInfo(domain,node.get("uuid").toString(),item,xmjc,bdbh);
            }
            else if(item.equals("甲方单位")||item.equals("乙方单位")||item.equals("施工单位")||item.equals("监理单位")){//甲方单位，乙方单位，施工单位，监理单位
                String dw=bditem.get(item)!=null?bditem.get(item).toString().replace("\\","、").replace("/","、"):"";
                if(StringUtil.isNotBlank(dw)) {
                    HashMap<String, Object> enode= mergeNodeAndLink(entity,domain,dw,node.get("uuid").toString(),xmjc);
                    String field="";
                    if(item.equals("甲方单位")){
                        field="JFDW";
                    }
                    if(item.equals("乙方单位")){
                        field="YFDW";
                    }
                    if(item.equals("施工单位")){
                        field="SGDW";
                    }
                    if(item.equals("监理单位")){
                        field="JLDW";
                    }
                    if(StringUtil.isNotBlank(field)){
                        //参与的项目，直接和项目连线
                        createXMXX(domain,field,enode.get("uuid").toString(),enode.get("name").toString(),dw);
                        //todo 企业信息 暂无数据
                    }
                }

            }
        }
    }
    public void createXMXX(String domain,String field,String pid,String prefix,String dwmc) {
        //获取施工、监理、甲方、乙方单位为xxx单位的项目列表
        String sqlBdmc = String.format("select DISTINCT(XMJC) as XMJC from BDJBXX where %s='%s' ",field,dwmc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        List<String> items=mapList.stream().map(n->n.get("XMJC").toString()).collect(Collectors.toList());
        //创建单位参与项目信息
        //HashMap<String, Object> nodeItem=createNodeAndLink(attribute,domain,"相关项目",pid);
        for (String item : items) {
            String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND m.name = '%s' "
                    + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), pid, item, "");
            neo4jUtil.excuteCypherSql(LinkcypherSql);
        }
    }
    public void createBiaoduanBaseInfo(String domain,String pid,String prefix,String xmjc,String bdbh) {
        String sqlBdmc = String.format("SELECT QDZH as 起点桩号,ZDZH as 终点桩号 ,HTDCD as 合同段长度,HTGQ as 合同工期,QY_HTJE as 合同金额 " +
                "from BDJBXX where BDBH='%s' AND XMMC='%s' ",bdbh,xmjc);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        Map<String, Object> biaoDuanItem=mapList.get(0);
        String[] items=new String[]{"起点桩号","终点桩号","合同段长度","合同工期","合同金额"};
        for (String item : items) {
            HashMap<String, Object> node = createNodeAndLink(attribute,domain, item, pid,xmjc);
            if(biaoDuanItem.get(item)!=null){
                String nextNode=biaoDuanItem.get(item).toString().replace("\\","、").replace("/","、");;
                createNodeAndLink(entity,domain, nextNode, node.get("uuid").toString(),xmjc);
            }

        }

    }
    public void createBiaoduanBiangengInfo(String domain,String pid,String prefix,String xmmc,String bdbh,String xmjc) {
        //变更总费用
        String sqlBgzfy = String.format("SELECT BGZFY as 变更总费用 from BDBGHZ where BDBH='%s' AND XMMC='%s' ",bdbh,xmmc);
        List<Map<String, Object>> BgzfymapList = sqlserverJdbcTemplate.queryForList(sqlBgzfy);
        if(BgzfymapList==null||BgzfymapList.size()==0) return;
        Map<String, Object> first=BgzfymapList.get(0);
        String bgzfyvlaue=first.get("变更总费用")!=null?first.get("变更总费用").toString():"暂无数据";
        HashMap<String, Object> nodeBgzfy = createNodeAndLink(attribute,domain, "变更总费用",pid,xmjc);
        //创建变更总费用
        createNodeAndLink(entity,domain, bgzfyvlaue,nodeBgzfy.get("uuid").toString(),xmjc);
        //清单名称
        HashMap<String, Object> node = createNodeAndLink(attribute,domain, "清单名称",pid,xmjc);
        //变更明细
        String sqlBdlbh = String.format("SELECT DISTINCT(QDMC) as 清单名称 from BGMXB where BDBH='%s' AND XMMC='%s' ",bdbh,xmmc);
        List<Map<String, Object>> bglbhmapList = sqlserverJdbcTemplate.queryForList(sqlBdlbh);
        if(bglbhmapList==null||bglbhmapList.size()==0) return;
        String[] bgl=new String[]{"变更费用","工程部位","变更申请号","变更令编号","变更理由","变更数量"};
        //清单名称
        for (Map<String, Object> bglbhItem : bglbhmapList){
            String bglNo=bglbhItem.get("清单名称").toString();
            HashMap<String, Object> realGengling = createNodeAndLink(entity,domain, bglNo, node.get("uuid").toString(),xmjc);
            String sqlBdl = String.format("SELECT BGLBH as 变更令编号,BGSL as 变更数量,BGFY as 变更费用,GCBW as 工程部位,QDMC as 清单名称,BGSQH as 变更申请号,BGLY as 变更理由 from BGMXB where BDBH='%s' AND XMMC='%s' and QDMC='%s'",bdbh,xmmc,bglNo);
            List<Map<String, Object>> bglmapList = sqlserverJdbcTemplate.queryForList(sqlBdl);
            if(bglmapList==null||bglmapList.size()==0) return;
            Map<String, Object> one=bglmapList.get(0);
            for (String s : bgl) {
                HashMap<String, Object> realProp =createNodeAndLink(attribute,domain, s, realGengling.get("uuid").toString(),xmjc);
                String bglGcbw=one.get(s)==null?"暂无数据":one.get(s).toString();
                if(StringUtil.isNotBlank(bglGcbw)){
                    bglGcbw=bglGcbw.replace("\\","、").replace("/","、");
                }
                createNodeAndLink(entity,domain, bglGcbw, realProp.get("uuid").toString(),xmjc);
            }
        }
    }
    public void createBenqiweiwanchengjinge(String domain,String pid,String prefix,String xmjc,String bdbh) {
        //变更总费用
        HashMap<String, Object> qici = createNodeAndLink(attribute,domain, "期次", pid,xmjc);
        String sql=String.format("SELECT DISTINCT(JLQC) as 期次 from SSJDSJ where  BDBH='%s' AND XMJC='%s' ",bdbh,xmjc);
        List<Map<String, Object>> qicimapList = sqlserverJdbcTemplate.queryForList(sql);
        if(qicimapList==null||qicimapList.size()==0) return;
        for (Map<String, Object> qiciMap : qicimapList) {
            String qiciValue=qiciMap.get("期次").toString();
            HashMap<String, Object> qiciNode = createNodeAndLink(attribute,domain, qiciValue,qici.get("uuid").toString(),xmjc);
            String sqlBgzfy = String.format("SELECT SUM(BQM_JE) as  金额  from SSJDSJ where BDBH='%s' AND XMJC='%s' and JLQC='%s'  ",bdbh,xmjc,qiciValue);
            List<Map<String, Object>> BgzfymapList = sqlserverJdbcTemplate.queryForList(sqlBgzfy);
            if(BgzfymapList==null||BgzfymapList.size()==0){
                createNodeAndLink(entity,domain, "0",qiciNode.get("uuid").toString(),xmjc);
            }else{
                for (Map<String, Object> bglItem : BgzfymapList){
                    String je=bglItem.get("金额").toString();
                    createNodeAndLink(entity,domain, je,qiciNode.get("uuid").toString(),xmjc);
                }
            }

        }
    }

    @Test
    public void delete() {
        String [] xmjcs=new String[]{"六六线", "毕生线", "毕都线", "余凯线", "遵贵扩容", "惠罗线", "尖小线", "凯羊线", "凯雷线", "织纳线", "望安线", "清织线", "六威线", "罗望线", "麻驾线", "都安高速"};
        String domain="交通全网检索业务图谱";
        for (String xmjc : xmjcs) {
            String cypherSql = String.format("match (n:`%s`) where n.name='%s' detach delete n", domain, xmjc);
            neo4jUtil.excuteCypherSql(cypherSql);
        }

    }
    @Test
    public void deleter() {
        String [] xmjcs=new String[]{"六六线", "毕生线", "毕都线", "余凯线", "遵贵扩容", "惠罗线", "尖小线", "凯羊线", "凯雷线", "织纳线", "望安线", "清织线", "六威线", "罗望线", "麻驾线", "都安高速"};
        String domain="交通全网检索业务图谱";
        for (String xmjc : xmjcs) {
            String cypherSql = String.format("match (n:`%s`)-[r]->(m) where m.name='%s' return id(r) as uuid", domain, xmjc);
            StatementResult res=neo4jUtil.excuteCypherSql(cypherSql);
            if(res.hasNext()){
               Integer uuid= res.list().stream().map(n->Integer.parseInt(n.get("uuid").toString())).max(Integer::max).get();
                System.out.println(uuid);
                String cypherSql2 = String.format("match (n:`%s`)-[r]->(m)  where id(r)=%s detach delete r", domain, uuid);
                neo4jUtil.excuteCypherSql(cypherSql2);
            }
        }

    }
    @Test
    public void testalia(){
        String sqlBdmc = String.format("SELECT XMMC as 项目名称,起点,终点,途经,CD as 车道,SJSU as 设计速度,ZTLJ as 整体路基,ZHXJ as 载荷新建,ZXQC as 主线全长,ZXLMJGLX as 主线路面结构类型,PZKGRQ as 批准开工日期,SJKGRQ as 实际开工日期 FROM XMJBXX where XMJC='%s'","务正线");
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        System.out.println("s");
    }

    @Test
    public void deleteall() {
        String cypherSql ="match (n:`交通全网检索业务图谱`)-[r]-(m) detach delete n,r,m";
        neo4jUtil.excuteCypherSql(cypherSql);

    }
    @Test
    public void respairXMNode()  {
        String cypher="MATCH (n:`交通全网检索业务图谱`)-[r]->(m) where n.name='项目业主'  return m ";
        List<HashMap<String, Object>> nodes= neo4jUtil.GetGraphNode(cypher);
        for (HashMap<String, Object> node : nodes) {
            //找到这个业主下所有的项目
            String cypherxm=String.format("MATCH (n:`交通全网检索业务图谱`)-[r]->(m) where id(n)=%s return m ",node.get("uuid").toString());
            List<HashMap<String, Object>> nodesxm= neo4jUtil.GetGraphNode(cypherxm);
            for (HashMap<String, Object> xmItem : nodesxm) {
                String xmjc=xmItem.get("name").toString();
                String XMsql=String.format("SELECT XMJC,TZMS,JSZT,XMYZ from xmjbxx WHERE XMJC='%s' ",xmjc);
                List<Map<String, Object>> XmmapList = sqlserverJdbcTemplate.queryForList(XMsql);
                if(XmmapList==null||XmmapList.size()==0) continue;
                Map<String, Object> xmitem=XmmapList.get(0);
                String tzms=xmitem.get("TZMS")!=null?xmitem.get("TZMS").toString():"";
                String jszt=xmitem.get("JSZT")!=null?xmitem.get("JSZT").toString():"";
                String xmyz=xmitem.get("XMYZ")!=null?xmitem.get("XMYZ").toString():"";
                String cypherSql = String.format("MATCH (n:`%s`) where id(n)=%s set n.tzms='%s',n.jszt='%s',n.xmyz='%s' ", "交通全网检索业务图谱",xmItem.get("uuid").toString(), tzms,jszt,xmyz);
                neo4jUtil.excuteCypherSql(cypherSql);
            }
        }


    }
    @Test
    public void respairXGXMNode()  {
        String domain="交通全网检索业务图谱";
        String cypher="match (n:`交通全网检索业务图谱`)-[r]->(m) where m.name='相关项目' return n ";
        List<HashMap<String, Object>> nodes= neo4jUtil.GetGraphNode(cypher);
        for (HashMap<String, Object> xmItem : nodes) {
            String name=xmItem.get("name").toString();
            String uuid=xmItem.get("uuid").toString();
            String item=xmItem.get("prefix").toString();
            String field="";
            if(item.equals("甲方单位")){
                field="JFDW";
            }
            if(item.equals("乙方单位")){
                field="YFDW";
            }
            if(item.equals("施工单位")){
                field="SGDW";
            }
            if(item.equals("监理单位")){
                field="JLDW";
            }
            //获取施工、监理、甲方、乙方单位为xxx单位的项目列表
            String sqlBdmc = String.format("select DISTINCT(XMJC) as XMJC from BDJBXX where %s='%s' ",field,name);
            List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
            if(mapList==null||mapList.size()==0) return;
            List<String> items=mapList.stream().map(n->n.get("XMJC").toString()).collect(Collectors.toList());
            //创建单位参与项目信息
            for (String s : items) {
                String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND m.name = '%s' "
                        + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), uuid, s, "参与项目");
                neo4jUtil.excuteCypherSql(LinkcypherSql);
            }
        }


    }
    //MATCH (n:`交通全网检索业务图谱`)  set n.sortcode=999


    @Test
    public void respairXMBase()  {
String domain="交通全网检索业务图谱";
        String cypherxm="SELECT distinct(XMJC) as name from xmjbxx";
        List<Map<String, Object>> nodesxm= sqlserverJdbcTemplate.queryForList(cypherxm);
        String[] props=new String[]{"建设状态","投资模式"};
        for (Map<String, Object> xmItem : nodesxm) {
            String xmjc=xmItem.get("name").toString();
            String XMsql=String.format("SELECT TZMS as 投资模式,JSZT as 建设状态 from xmjbxx WHERE XMJC='%s' ",xmjc);
            List<Map<String, Object>> XmmapList = sqlserverJdbcTemplate.queryForList(XMsql);
            if(XmmapList==null||XmmapList.size()==0) continue;
            Map<String, Object> xmitem=XmmapList.get(0);

            String cypher=String.format("MATCH (n:`交通全网检索业务图谱`)-[r]->(m) where n.name='%s' and m.name='基本信息'  return m ",xmjc);
            List<HashMap<String, Object>> nodes= neo4jUtil.GetGraphNode(cypher);
            for (HashMap<String, Object> node : nodes) {
                String uuid=node.get("uuid").toString();
                for (String prop : props) {
                    String  nodePropSql = String.format("create (n:`%s`{name:'%s',sortcode:999,type:2,isleaf:1}) return n", domain, prop);
                    List<HashMap<String, Object>>  nodePropMap=neo4jUtil.GetGraphNode(nodePropSql);
                    HashMap<String, Object> nodeProp=nodePropMap.get(0);
                    String nodePropUUid=nodeProp.get("uuid").toString();
                    String nodeLinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND id(m) = %s "
                            + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), uuid, nodePropUUid, "");
                    neo4jUtil.excuteCypherSql(nodeLinkcypherSql);

                    String propvalue=xmitem.get(prop)!=null?xmitem.get(prop).toString():"";
                    String  cypherSql2 = String.format("create (n:`%s`{name:'%s',sortcode:999,type:0,isleaf:1}) return n", domain, propvalue);
                    List<HashMap<String, Object>> nodesvalues= neo4jUtil.GetGraphNode(cypherSql2);
                    String LinkcypherSql = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND id(m) = %s "
                            + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), nodePropUUid, nodesvalues.get(0).get("uuid"), "");
                    neo4jUtil.excuteCypherSql(LinkcypherSql);
                }

            }
           /* String cypher=String.format("MATCH (n:`交通全网检索业务图谱`)-[r]->(m) where n.name='%s' and m.name='基本信息'  return m ",xmjc);
            List<HashMap<String, Object>> nodes= neo4jUtil.GetGraphNode(cypher);*/
        }



    }
    @Test
    public void respairXMTZBase()  {
        String domain="交通全网检索业务图谱";
        String cypherxm="SELECT distinct(XMJC) as name from xmjbxx";
        List<Map<String, Object>> nodesxm= sqlserverJdbcTemplate.queryForList(cypherxm);
        for (Map<String, Object> xmItem : nodesxm) {
            String xmjc=xmItem.get("name").toString();
            String cypher=String.format("MATCH (n:`交通全网检索业务图谱`)-[r]->(m)-[rr]->(mm) where n.name='%s' and (m.name='工可批复金额' ) detach delete m",xmjc);//m.name='投资模式' or m.name='建设状态' or m.name='工可批复金额'
             neo4jUtil.GetGraphNode(cypher);
        }
    }
    @Test
    public void createWithoutXMYZ(){
        String domain="交通全网检索业务图谱";
        String sqlBdmc = "select DISTINCT(XMJC) as XMJC from xmjbxx where XMYZ is null ";
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
        if(mapList==null||mapList.size()==0) return;
        List<String> xmjcs=mapList.stream().map(n->n.get("XMJC").toString()).collect(Collectors.toList());
        for (String xmjc : xmjcs) {
            String  cypherSql = String.format("create (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0}) return n", domain, xmjc,0);
            List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
            HashMap<String, Object> nodeItem=nodesQl.get(0);
            //创建路线基本信息
            createRoterInformation(domain,nodeItem.get("uuid").toString(),xmjc);
        }
    }
    @Test
    public void createWithoutXMYZ2(){
        String domain="交通全网检索业务图谱";
        String sqlBdmc = "select DISTINCT(XMJC) as XMJC from xmjbxx where XMYZ is null ";
        String[] xmjcs = new String[]{
                "白黔线",
                "贵瓮线",
                "贵州省都匀至安顺公路",
                "贵州织金至普定高速",
                "赫六线",
                "赫镇线",
                "机场支线（测试）",
                "江瓮线",
                "开息线",
                "盘兴线",
                "清织2016",
                "仁赤线",
                "息黔线",
                "新石高速",
                "沿德线",
                "正习线",
                "织普线",
                "遵毕线"
        };
        for (String xmjc : xmjcs) {
            String  cypherSql = String.format("create (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0}) return n", domain, xmjc,0);
            List<HashMap<String, Object>>  nodesQl=neo4jUtil.GetGraphNode(cypherSql);
            HashMap<String, Object> nodeItem=nodesQl.get(0);
            //创建路线基本信息
            createRoterInformation(domain,nodeItem.get("uuid").toString(),xmjc);
        }
    }
    @Test
    public void createBiaoduanInformationByBDMC() {
        String domain="交通全网检索业务图谱";
        //todo 标段编号换名称
        String cypher="MATCH (n:`交通全网检索业务图谱`)-[r]->(m) where n.name='标段' return m ";

        List<HashMap<String, Object>> nodes= neo4jUtil.GetGraphNode(cypher);
        for (HashMap<String, Object> node : nodes) {
            String uuid = node.get("uuid").toString();
            String name = node.get("name").toString();
            String cp=String.format("MATCH (n:`交通全网检索业务图谱`)-[r]->(m)-[rr]->(x) where id(x)=%s return n limit  1",uuid);
            List<HashMap<String, Object>> node2s= neo4jUtil.GetGraphNode(cp);
            if(node2s!=null&&node2s.size()>0){
                HashMap<String, Object> nn=node2s.get(0);
                String xmjc=nn.get("name").toString();
                String sqlBdmc = String.format("select BDBH from BDJBXX where XMJC='%s' and BDMC='%s'",xmjc,name);
                List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlBdmc);
                if(mapList==null||mapList.size()==0) return;
                List<String> items=mapList.stream().map(n->n.get("BDBH").toString()).collect(Collectors.toList());
                createBiaoduanDetail(domain,uuid,name,xmjc,items.get(0));
            }
        }

    }
    @Test
    public void createAGGInfo() {
        String domain= "交通全网检索业务图谱";
        //String domain= "测试权重";
        //String json="[{\"field\":\"CD\",\"alia\":\"车道\",\"table\":\"XMJBXX\"},{\"field\":\"SJSU\",\"alia\":\"设计速度\",\"table\":\"XMJBXX\"},{\"field\":\"ZTLJ\",\"alia\":\"整体路基\",\"table\":\"XMJBXX\"},{\"field\":\"ZHXJ\",\"alia\":\"载荷新建\",\"table\":\"XMJBXX\"},{\"field\":\"ZZZBL\",\"alia\":\"资本金占总投资比例\",\"table\":\"XMJBXX\"},{\"field\":\"YZZBL\",\"alia\":\"银行贷款占总投资比例\",\"table\":\"XMJBXX\"},{\"field\":\"ZXLMKD\",\"alia\":\"主线路面宽度\",\"table\":\"XMJBXX\"},{\"field\":\"ZXLMHD\",\"alia\":\"主线路面长度\",\"table\":\"XMJBXX\"},{\"field\":\"SJDW\",\"alia\":\"设计单位\",\"table\":\"XMJBXX\"},{\"field\":\"XMYZ\",\"alia\":\"项目业主\",\"table\":\"XMJBXX\"},{\"field\":\"JFDW\",\"alia\":\"甲方单位\",\"table\":\"XMJBXX\"},{\"field\":\"TZMS\",\"alia\":\"投资模式\",\"table\":\"XMJBXX\"},{\"field\":\"JSZT\",\"alia\":\"建设状态\",\"table\":\"XMJBXX\"},{\"field\":\"起点\",\"alia\":\"起点\",\"table\":\"XMJBXX\"},{\"field\":\"终点\",\"alia\":\"终点\",\"table\":\"XMJBXX\"},{\"field\":\"主线路面类型\",\"alia\":\"主线路面类型\",\"table\":\"XMJBXX\"},{\"field\":\"主线路面类型\",\"alia\":\"主线面层厚度\",\"table\":\"XMJBXX\"}]";
        //String json="[{\"field\":\"XMFR\",\"alia\":\"项目法人\",\"table\":\"BDJBXX\"},{\"field\":\"YFDW\",\"alia\":\"乙方单位\",\"table\":\"BDJBXX\"},{\"field\":\"SGDW\",\"alia\":\"施工单位\",\"table\":\"BDJBXX\"},{\"field\":\"JFDW\",\"alia\":\"甲方单位\",\"table\":\"BDBGHZ\"},{\"field\":\"JLDW\",\"alia\":\"监理单位\",\"table\":\"BDBGHZ\"}]";
        String json="[{\"field\":\"GKPFDW\",\"alia\":\"工可批复单位\",\"table\":\"XMBGHZ\"},{\"field\":\"BGDJ\",\"alia\":\"变更等级\",\"table\":\"SSJDSJ\"}]";
        List<Map<String,String>> indicators= JSON.parseObject(json,List.class);
        for (Map<String, String> indicator : indicators) {
            String field=indicator.get("field");
            String alia=indicator.get("alia");
            String table=indicator.get("table");
            System.out.println(table+"__"+alia+"__"+field);
            createSJSDInfo(domain,table,field,alia);
        }
    }
    public void createSJSDInfo(String domain,String table,String field,String nodeName) {
        String cypherNode = String.format("merge (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'',agg:1}) return n", domain,nodeName, entityValue);
        List<HashMap<String, Object>> nodesQl = neo4jUtil.GetGraphNode(cypherNode);
        if (nodesQl == null || nodesQl.size() == 0) return;
        HashMap<String, Object> nodeItem = nodesQl.get(0);
        String sqlIndicator = String.format("select DISTINCT(%s) as indicator from %s where %s is not null ",field,table,field);
        List<Map<String, Object>> mapList = sqlserverJdbcTemplate.queryForList(sqlIndicator);
        if(mapList==null||mapList.size()==0) return;
        for (Map<String, Object> item : mapList) {
            String indicator=item.get("indicator").toString();
            String cypherSql1 = String.format("merge (n:`%s`{name:'%s',type:%s,sortcode:999,isleaf:0,xmjc:'',agg:1}) return n", domain,indicator, entityValue);
            List<HashMap<String, Object>> nodesQl1 = neo4jUtil.GetGraphNode(cypherSql1);
            if (nodesQl1 == null || nodesQl1.size() == 0)  continue;
            HashMap<String, Object> nodeItem1 = nodesQl1.get(0);
            String LinkcypherSql1 = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND id(m) = %s "
                    + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), nodeItem.get("uuid"), nodeItem1.get("uuid"),"");
            neo4jUtil.excuteCypherSql(LinkcypherSql1);
            String sqlxm = String.format("select DISTINCT(XMJC) from %s where %s ='%s' ",table,field,indicator);
            List<Map<String, Object>> xmList = sqlserverJdbcTemplate.queryForList(sqlxm);
            if(xmList==null||xmList.size()==0) continue;
            //找到设计速度是xx的项目，创建连线
            for (Map<String, Object> xm : xmList) {
                String xmjc=xm.get("XMJC").toString();
                String LinkcypherSqlxm = String.format("MATCH (n:`%s`),(m:`%s`) WHERE id(n)=%s AND m.name = '%s' "
                        + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), nodeItem1.get("uuid"), xmjc,"");
                neo4jUtil.excuteCypherSql(LinkcypherSqlxm);
            }
        }
    }
    @Test
    public void createBDBGInfo() {
        String domain= "交通全网检索业务图谱";
        //String cypherNode = String.format("MATCH (n:`交通全网检索业务图谱`) where n.name='变更' and n.bdmc is not null return n");
        String cypherNode = String.format("MATCH (n:`交通全网检索业务图谱`) where n.name='变更' and n.bdmc is not null and n.createstatus is null return n");
        List<HashMap<String, Object>> nodesQl = neo4jUtil.GetGraphNode(cypherNode);
        if (nodesQl == null || nodesQl.size() == 0) return;
        for (HashMap<String, Object> mp : nodesQl) {
            String uuid=mp.get("uuid").toString();
            String xmjc=mp.get("xmjc").toString();
            String bdmc=mp.get("bdmc").toString();
            String sqlBgzfy = String.format("SELECT BGZFY as 变更总费用 from BDBGHZ where BDMC='%s' AND XMJC='%s' ",bdmc,xmjc);
            List<Map<String, Object>> BgzfymapList = sqlserverJdbcTemplate.queryForList(sqlBgzfy);
            if(BgzfymapList==null||BgzfymapList.size()==0) continue;
            Map<String, Object> first=BgzfymapList.get(0);
            String bgzfyvlaue=first.get("变更总费用")!=null?first.get("变更总费用").toString():"暂无数据";
            HashMap<String, Object> nodeBgzfy = createNodeAndLink(attribute,domain, "变更总费用",uuid,xmjc);
            //创建变更总费用
            HashMap<String, Object> bgzfynode =createNodeAndLink(entity,domain, bgzfyvlaue,nodeBgzfy.get("uuid").toString(),xmjc);
            //变更明细
            String sqlBdlbh = String.format("SELECT DISTINCT(QDMC) as 清单名称 from BGMXB b left join xmjbxx x on b.GCXM=x.GCXM where b.bdmc='%s' AND x.XMJC='%s' ",bdmc,xmjc);
            List<Map<String, Object>> bglbhmapList = sqlserverJdbcTemplate.queryForList(sqlBdlbh);
            if(bglbhmapList==null||bglbhmapList.size()==0) continue;
            String[] bgl=new String[]{"变更费用","计量单位","计量单价","变更等级","工程部位","变更申请号","变更令编号","变更理由","变更数量"};
            //清单名称
            for (Map<String, Object> bglbhItem : bglbhmapList){
                String qdmc=bglbhItem.get("清单名称").toString();
                HashMap<String, Object> realGengling = createNodeAndLink(entity,domain, qdmc, bgzfynode.get("uuid").toString(),xmjc);
                HashMap<String, Object> bglbh = createNodeAndLink(attribute,domain, "变更令编号",realGengling.get("uuid").toString(),xmjc);
                String sqlBglbh = String.format("SELECT DISTINCT(b.BGLBH) as 变更令编号 from BGMXB b left join xmjbxx x on b.GCXM=x.GCXM  where b.bdmc='%s'and b.QDMC='%s' AND x.XMJC='%s'",bdmc,qdmc,xmjc);
                List<Map<String, Object>> bglbhList = sqlserverJdbcTemplate.queryForList(sqlBglbh);
                for (Map<String, Object> blno : bglbhList) {
                    String bglbhstr=blno.get("变更令编号").toString();
                    HashMap<String, Object> bglNode = createNodeAndLink(entity,domain, bglbhstr, bglbh.get("uuid").toString(),xmjc);
                    String sqlBdl = String.format("SELECT b.QDBH as 清单编号,b.QDMC as 清单名称,b.JLDW as 计量单位,b.jldj as 计量单价,b.BGDJ as 变更等级,b.BGLBH as 变更令编号,b.BGSL as 变更数量,b.BGFY as 变更费用,b.GCBW as 工程部位,b.BGSQH as 变更申请号,b.BGLY as 变更理由 from BGMXB b left join xmjbxx x on b.GCXM=x.GCXM where b.bdmc='%s'and b.QDMC='%s' AND x.XMJC='%s'",bdmc,qdmc,xmjc);
                    List<Map<String, Object>> bglmapList = sqlserverJdbcTemplate.queryForList(sqlBdl);
                    if(bglmapList==null||bglmapList.size()==0) continue;
                    Map<String, Object> one=bglmapList.get(0);
                    for (String s : bgl) {
                        HashMap<String, Object> realProp =createNodeAndLink(attribute,domain, s, bglNode.get("uuid").toString(),xmjc);
                        String bglGcbw=one.get(s)==null?"暂无数据":one.get(s).toString();
                        if(StringUtil.isNotBlank(bglGcbw)){
                            bglGcbw=bglGcbw.replace("\\","、").replace("/","、");
                        }
                        createNodeAndLink(entity,domain, bglGcbw, realProp.get("uuid").toString(),xmjc);
                    }
                }

            }
        }

    }
}
