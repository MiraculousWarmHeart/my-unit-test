package cnki.kg.demo;

import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

@SpringBootTest
@WebAppConfiguration
public class TestZookeeper {
    // 注意：逗号前后不能有空格
    private static String connectString = "hd11:2181,hd12:2181,hd13:2181";

    private static int sessionTimeout = 2000;
    private ZooKeeper zkClient = null;
    private String parentNode = "/servers";

    public void init() throws Exception {
        zkClient = new ZooKeeper(connectString, sessionTimeout, new Watcher() {
            @Override
            public void process(WatchedEvent watchedEvent) {
                // 收到事件通知后的回调函数（用户的业务逻辑）
                System.out.println(watchedEvent.getType() + "--"+ watchedEvent.getPath());
                // 再次启动监听
                try {
                    List<String> children = zkClient.getChildren("/",
                            true);
                    for (String child : children) { System.out.println(child);
                    }
                } catch (Exception e) { e.printStackTrace();
                }
            }
        });
    }
    @Test
    public void createNode() throws KeeperException, InterruptedException {
        // 参数 1：要创建的节点的路径； 参数 2：节点数据 ； 参数 3：节点权限 ； 参数 4：节点的类型
        String	nodeCreated	=zkClient.create("/atguigu","tc".getBytes(StandardCharsets.UTF_8), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
    }
    @Test
    public void getChildren() throws KeeperException, InterruptedException {
        List<String> children = zkClient.getChildren("/", true); for (String child : children) {
            System.out.println(child);
        }
        // 延时阻塞
        Thread.sleep(Long.MAX_VALUE);
    }
    @Test
    public void exist() throws KeeperException, InterruptedException {
        Stat stat = zkClient.exists("/atguigu", false);

        System.out.println(stat == null ? "not exist" : "exist");
    }
    @Test
    public void resigerToServer() throws Exception {
        String hostName="";
        // 1 获取 zk 连接
        getConnect();
        // 2 利用 zk 连接注册服务器信息
        registServer(hostName);
        // 3 启动业务功能
        business(hostName);
    }

    public void getConnect() throws KeeperException, InterruptedException, IOException {
        zkClient = new ZooKeeper(connectString, sessionTimeout, new Watcher() {

            @Override
            public void process(WatchedEvent event) {
            }
        });
    }
    // 注册服务器
    public void registServer(String hostname) throws Exception{

        String create = zkClient.create(parentNode + "/server", hostname.getBytes(),	ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL_SEQUENTIAL);

        System.out.println(hostname +" is online "+ create);
    }

    // 业务功能
    public void business(String hostname) throws Exception{ System.out.println(hostname + " is working ...");
        Thread.sleep(Long.MAX_VALUE);
    }

}
