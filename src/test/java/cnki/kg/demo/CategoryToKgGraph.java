package cnki.kg.demo;

import cnki.kg.demo.util.Neo4jUtil;
import cnki.kg.demo.util.StringUtil;
import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@SpringBootTest
@WebAppConfiguration
public class CategoryToKgGraph {

    @Autowired
    Neo4jUtil neo4jUtil;
    @Autowired
    @Qualifier("mysqlJdbcTemplate")
    JdbcTemplate mysqlJdbcTemplate;
    private String domain="传统文化_58a774d2-cab1-4b29-9ac0-31f20c09d823";
    @Test
    public void transfer() throws IOException {
        String sql="select CategoryId,CategoryCode as code,CategoryName as name,SearchNavCategoryId,ParentId,ParentCode,IsLeaf from searchnavcategorydetail where SearchNavCategoryId=184";
        List<Map<String, Object>> maps=mysqlJdbcTemplate.queryForList(sql);
        //getTree(0, maps);
        if(maps.size()>0){
            for (Map<String, Object> map : maps) {
                Integer id=Integer.parseInt(map.get("CategoryId").toString());
                String code=map.get("code").toString();
                List<Map<String,Object>> treeList = maps.stream().filter(m -> Integer.parseInt(m.get("ParentId").toString())==id).collect(Collectors.toList());
                for (Map<String, Object> mm : treeList) {
                    String code2=mm.get("code").toString();
                    createShips(domain,code,code2);
                }
            }
        }
    }

    private void getTree(Integer id, List<Map<String,Object>> nodelList) {
        List<Map<String,Object>> treeList = nodelList.stream().filter(m -> Integer.parseInt(m.get("ParentId").toString())==id).collect(Collectors.toList());
        if(treeList.size()>0){
            for (Map<String, Object> cate : treeList) {
                createNode(domain,cate);
                getTree(Integer.parseInt(cate.get("CategoryId").toString()), nodelList);
            }
        }
    }
    private void  createNode(String domain,Map<String,Object> record){
        String propertiesString = neo4jUtil.getFilterPropertiesJson(JSON.toJSONString(record));
        String cypherSql = String.format("create (n:`%s`%s) return n", domain, propertiesString);
        neo4jUtil.GetGraphNode(cypherSql);
    }
    private void  createShips(String domain,String pCode,String code){
        if(StringUtil.isNotBlank(pCode)){
            String cypherSql2 = String.format("MATCH (n:`%s`),(m:`%s`) WHERE n.code='%s' AND m.code ='%s' "
                    + "merge (n)-[r:RE{name:'%s'}]->(m)" + "RETURN r", domain.trim(), domain.trim(), pCode,code,"");
            neo4jUtil.excuteCypherSql(cypherSql2);
        }
    }
}
