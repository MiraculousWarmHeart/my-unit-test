package cnki.kg.demo;

import cnki.kg.demo.util.Neo4jUtil;
import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.List;


@SpringBootTest
@WebAppConfiguration
public class CreateCtwhJson {

    @Autowired
    Neo4jUtil neo4jUtil;
    @Autowired
    @Qualifier("mysqlJdbcTemplate")
    JdbcTemplate mysqlJdbcTemplate;
    @Test
    public void selet() {
        String cypher = "match (n:`zhctwh`)-[r]->(m)-[rr]->(mm) where n.code in['27301'] return n,r,m,rr,mm";
        HashMap<String, Object> ns = neo4jUtil.GetGraphNodeAndShip(cypher);
        List<HashMap<String, Object>> nodes=(List<HashMap<String, Object>>)ns.get("node");
        for (HashMap<String, Object> node : nodes) {
            node.remove("similar");
            node.remove("hypernym");
            node.remove("showchild");
            node.remove("type");
            node.remove("state");
        }
        System.out.println(JSON.toJSONString(ns));

    }
}
