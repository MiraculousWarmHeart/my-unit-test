package cnki.kg.demo;

import cnki.kg.demo.entity.intent.NlpResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@SpringBootTest
@WebAppConfiguration
public class TestJSONSerialzable {


    @Test
    public void Test() {
        String intentStr = "{\n" +
                "    \"NlpResult\": {\n" +
                "        \"keyword\": \"务正线_设计速度\",\n" +
                "        \"region\": \"\",\n" +
                "        \"datetime\": \"\",\n" +
                "        \"domainResult\": [\n" +
                "            {\n" +
                "                \"words\": [\n" +
                "                    {\n" +
                "                        \"synonymcontent\": \"务正线\",\n" +
                "                        \"hyponymcontent\": \"\",\n" +
                "                        \"hypernymcontent\": \"项目名称\",\n" +
                "                        \"content\": \"务川至正安高速公路\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"synonymcontent\": \"设计时速\",\n" +
                "                        \"hyponymcontent\": \"最高速度\",\n" +
                "                        \"hypernymcontent\": \"\",\n" +
                "                        \"content\": \"设计速度\"\n" +
                "                    }\n" +
                "                ],\n" +
                "                \"domain\": \"设计速度\",\n" +
                "                \"domainId\": 121,\n" +
                "                \"domainScore\": 0.6666666666666666,\n" +
                "                \"templateId\": -1,\n" +
                "                \"items\": [\n" +
                "                    {\n" +
                "                        \"subdomainCode\": \"GHSJJDXMXX\",\n" +
                "                        \"score\": 0,\n" +
                "                        \"subdomainName\": \"规划设计阶段项目信息\",\n" +
                "                        \"statement\": [\n" +
                "                            {\n" +
                "                                \"target\": \"项目名称\",\n" +
                "                                \"condition\": \"=\",\n" +
                "                                \"value\": \"务川至正安高速公路\",\n" +
                "                                \"fieldtype\": 1\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"target\": \"设计速度\",\n" +
                "                                \"condition\": \"=\",\n" +
                "                                \"value\": 1,\n" +
                "                                \"fieldtype\": 1\n" +
                "                            }\n" +
                "                        ]\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"subdomainCode\": \"XMJBXX\",\n" +
                "                        \"score\": 0,\n" +
                "                        \"subdomainName\": \"项目基本信息\",\n" +
                "                        \"statement\": [\n" +
                "                            {\n" +
                "                                \"target\": \"项目名称\",\n" +
                "                                \"condition\": \"=\",\n" +
                "                                \"value\": \"务川至正安高速公路\",\n" +
                "                                \"fieldtype\": 1\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"target\": \"设计速度\",\n" +
                "                                \"condition\": \"=\",\n" +
                "                                \"value\": 1,\n" +
                "                                \"fieldtype\": 1\n" +
                "                            }\n" +
                "                        ]\n" +
                "                    }\n" +
                "                ]\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}";
        NlpResult result = JSON.parseObject(intentStr, NlpResult.class);
        System.out.println(result);
    }
    @Test
    public void Test2() {
        String intentStr = "{\n" +
                "        \"keyword\": \"务正线_设计速度\",\n" +
                "        \"region\": \"2\",\n" +
                "        \"datetime\": \"2\",\n" +
                "        \"domainResult\": []\n" +
                "    }";
        NlpResult result = JSON.parseObject(intentStr, NlpResult.class);
        System.out.println(result);
    }

    @Test
    public void TestJJ(){
        String json="[\n" +
                "    [\n" +
                "        \"贵州省\",\n" +
                "        \"贵阳市\",\n" +
                "        \"云岩区\"\n" +
                "    ],\n" +
                "    [\n" +
                "        \"贵州省\",\n" +
                "        \"遵义市\"\n" +
                "    ]\n" +
                "]";
        List<List<String>> parse = JSONObject.parseObject(json, new TypeReference<List<List<String>>>(){});
        System.out.println(parse);
    }
    @Test
    public void TestJJj(){
        String json="[]";
        List<List<String>> parse = JSONObject.parseObject(json, new TypeReference<List<List<String>>>(){});
        List<String> parse2 = JSONObject.parseObject(json, new TypeReference<List<String>>(){});
        System.out.println(parse);
    }
    @Test
    public void TestMergeCode(){
        //String json="['0001','00010002','00010002003','0002','0003','00020004','00050006','000700080009']";
        String json="['0001','00010002','00010002003','0002','0003','00020004','00050006','000700080009']";
        List<String> parse = JSONObject.parseObject(json, new TypeReference<List<String>>(){});
        List<String> strings = handleCategory(parse);
        System.out.println(strings);
    }
    private List<String> handleCategory( List<String> categoryData){
        List<String> codes=new ArrayList<>();
        List<String> mergeCodes=new ArrayList<>();
        List<String> dataList=categoryData.stream().sorted(Comparator.<String>comparingInt(String::length)).collect(Collectors.toList());
        for (String systemCode : dataList) {
            if(codes.contains(systemCode)) {
                continue;
            }
            List<String> list=categoryData.stream().filter(s -> s.startsWith(systemCode)).collect(Collectors.toList());
            if(list.size()>0){
                codes.addAll(list);
            }
            mergeCodes.add(systemCode);
        }
        return mergeCodes;
    }
}
