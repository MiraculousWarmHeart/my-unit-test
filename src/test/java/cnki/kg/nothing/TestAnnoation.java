package cnki.kg.nothing;

import cnki.kg.demo.annotation.FirstAnnoation;
import org.junit.jupiter.api.Test;

public class TestAnnoation {

    @Test
    @FirstAnnoation(name = "tan",value = 18)
    public void test(){
        System.out.println("aa");
    }

    @Test
    @FirstAnnoation(name = "tan",value = 18)
    public void test2() throws NoSuchMethodException {
        boolean hasAnnotation = TestAnnoation.class.getMethod("test2").isAnnotationPresent(FirstAnnoation.class);
        if ( hasAnnotation ) {
            FirstAnnoation testAnnotation = TestAnnoation.class.getMethod("test2").getAnnotation(FirstAnnoation.class);
            System.out.println("name:"+testAnnotation.name());
            System.out.println("value:"+testAnnotation.value());
        }
    }
}
