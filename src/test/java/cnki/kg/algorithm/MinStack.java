package cnki.kg.algorithm;

import org.junit.jupiter.api.Test;

import java.util.Stack;

public class MinStack {
    public Stack<int[]> stack=new Stack<>();
    public MinStack() {

    }

    public void push(int val) {
        if(stack.isEmpty()) {
            stack.push(new int[]{val,val});
        }else {
            stack.push(new int[]{val,Math.min(val,getMin())});
        }

    }

    public void pop() {
        stack.pop();
    }

    public int top() {
        return stack.peek()[0];
    }

    public int getMin() {
        int i = stack.peek()[1];
        return i;
    }
}
