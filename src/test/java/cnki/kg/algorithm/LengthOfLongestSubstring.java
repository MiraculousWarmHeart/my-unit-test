package cnki.kg.algorithm;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class LengthOfLongestSubstring {
    @Test
    public void test(){
        String s="pwwkew";
        System.out.println(lengthOfLongestSubstring(s));
    }

    public int lengthOfLongestSubstring(String s) {
        Map<Character,Integer> mp=new HashMap<>();
        int ans=0;
        for (int right = 0, left=0; right < s.length(); right++) {
            Character a=s.charAt(right);
            if(mp.containsKey(a)){
                left=Math.max(mp.get(a),left);
            }
            ans=Math.max(ans,right-left+1);
            mp.put(a,right+1);
        }
        return ans;
    }
    @Test
    public void testReverse(){
       // System.out.println(reverse(123));
        System.out.println(reverse(1534236469));//溢出返回0
    }
    public int reverse(int x) {
        if(x==0) return 0;
        // int flag=1;update
        // if(x<0){
        //     flag=-1;
        // }
        // String a=Math.abs(x)+"";
        // String b="";
        // for(int i=a.length()-1;i>=0;i--){
        //     b+=a.charAt(i);
        // }
        // return Integer.parseInt(b)*flag;
        int res = 0;
        int last = 0;
        while(x!=0) {
            //每次取末尾数字
            int tmp = x%10;
            last = res;
            res = res*10 + tmp;
            //判断整数溢出
            if(last != res/10)
            {
                return 0;
            }
            x /= 10;
        }
        return res;
    }
    @Test
    public void testIsPalindrome(){
        System.out.println(isPalindrome(12321));
    }
    public boolean isPalindrome(int x) {
        // 特殊情况：
        // 如上所述，当 x < 0 时，x 不是回文数。
        // 同样地，如果数字的最后一位是 0，为了使该数字为回文，
        // 则其第一位数字也应该是 0
        // 只有 0 满足这一属性
        if (x < 0 || (x % 10 == 0 && x != 0)) {
            return false;
        }

        int revertedNumber = 0;
        while (x > revertedNumber) {
            revertedNumber = revertedNumber * 10 + x % 10;
            x /= 10;
        }

        // 当数字长度为奇数时，我们可以通过 revertedNumber/10 去除处于中位的数字。
        // 例如，当输入为 12321 时，在 while 循环的末尾我们可以得到 x = 12，revertedNumber = 123，
        // 由于处于中位的数字不影响回文（它总是与自己相等），所以我们可以简单地将其去除。
        return x == revertedNumber || x == revertedNumber / 10;
    }
    public boolean isPalindrome2(int x) {
        String str = String.valueOf(x);
        StringBuilder sb = new StringBuilder(str);
        return sb.reverse().toString().equals(str);
    }
}
