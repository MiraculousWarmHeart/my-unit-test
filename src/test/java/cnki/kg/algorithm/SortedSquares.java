package cnki.kg.algorithm;

import org.junit.jupiter.api.Test;

public class SortedSquares {
    @Test
    public void test(){
        
    }
    
    public int[] sortedSquares(int[] nums){
        int[] result=new int[nums.length];
        int resIndex=nums.length-1;
        int left=0;
        int right=nums.length-1;
        while(left<=right){
            if(nums[left]*nums[left]<=nums[right]*nums[right]){
                result[resIndex--]=nums[right]*nums[right];
                --right;
            }else {
                result[resIndex--]=nums[left]*nums[left];
                ++left;
            }
        }
        return result;
    }
}
