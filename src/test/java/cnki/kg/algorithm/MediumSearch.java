package cnki.kg.algorithm;

import org.junit.jupiter.api.Test;

public class MediumSearch {

    @Test
    public  void test() {
int [] nums=new int[]{-1,0,3,5,9,12};
        int search = search(nums, 3);
        System.out.println(search);
    }
    public int search3(int[] nums, int target) {
        int left=0;
        int right=nums.length-1;
        int mid=0;
       while (left<=right){
           mid=(right-left)/2+left;
           if(nums[mid]<target){
               left=mid+1;
           }else if(nums[mid]>target){
               right=mid-1;
           }else {
               return mid;
           }
       }
       return -1;
    }
    public int search(int[] nums, int target) {
       int left=0,right=nums.length-1,mid=0;
       while (left<=right){
           mid=left+(right-left)/2;
           if(nums[mid]>target){
               right=mid-1;
           }else if(nums[mid]<target){
               left=mid+1;
           }else {
               return mid;
           }
       }
       return -1;
    }
}
