package cnki.kg.algorithm;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class IsAnagram {
    @Test
    public void  test(){
       boolean res= isAnagram("anagram","nagarama");
        System.out.println(res);
    }
    public boolean isAnagram(String s, String t) {

        Map<Character,Integer> mp=new HashMap<>();
        for (char c : s.toCharArray()) {
           if(mp.containsKey(c)){
               mp.put(c,mp.get(c)+1);
           }else{
               mp.put(c,1);
           }
        }
        for (char c : t.toCharArray()) {
            if(mp.containsKey(c)){
                mp.put(c,mp.get(c)-1);
            }else{
                return false;
            }
        }
        for (Character ch : mp.keySet()) {
            if (mp.get(ch) != 0) {
                return false;
            }
        }
        return true;
    }
    public boolean isAnagram2(String s, String t) {

        int[] record = new int[26];
        for (char c : s.toCharArray()) {
            System.out.println(c - 'a');
            System.out.println("c:"+c+"a:"+'a');
            record[c - 'a'] += 1;
        }
        for (char c : t.toCharArray()) {
            record[c - 'a'] -= 1;
        }
        for (int i : record) {
            if (i != 0) {
                return false;
            }
        }
        return true;
    }
}
