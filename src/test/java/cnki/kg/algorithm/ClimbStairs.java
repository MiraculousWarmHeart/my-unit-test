package cnki.kg.algorithm;

import org.junit.jupiter.api.Test;

public class ClimbStairs {
    @Test
    public void  test(){
        int res2=climbStairs(10);
        System.out.println(res2);
    }
    public int climbStairs(int n) {
        int[] dp = new int[n + 1];
        dp[0] = 1;
        dp[1] = 1;
        for(int i = 2; i <= n; i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }
        return dp[n];
    }
}
