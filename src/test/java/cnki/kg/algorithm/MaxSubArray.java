package cnki.kg.algorithm;

import org.junit.jupiter.api.Test;

public class MaxSubArray {

    @Test
    public void  test(){
        int[] nums1={-2,1,-3,4,-1,2,1,-5,4};
        int res2=maxSubArray(nums1);
        System.out.println(res2);
    }
    public int maxSubArray(int[] nums) {
        int res = nums[0];
        int sum = 0;
        for (int num : nums) {
            if (sum > 0)
                sum += num;
            else
                sum = num;
            res = Math.max(res, sum);
        }
        return res;
    }
}
