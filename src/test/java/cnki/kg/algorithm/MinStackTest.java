package cnki.kg.algorithm;

import org.junit.jupiter.api.Test;

public class MinStackTest {
    @Test
    public void test() {
        MinStack minStack = new MinStack();
        minStack.push(-2);
        minStack.push(0);
        minStack.push(-3);
       int a= minStack.getMin();   //--> 返回 -3.
        minStack.pop();
        int b=minStack.top();      //--> 返回 0.
        int c=minStack.getMin();   //--> 返回 -2.

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }
}
