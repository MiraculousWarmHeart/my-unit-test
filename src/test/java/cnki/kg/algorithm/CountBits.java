package cnki.kg.algorithm;

import org.junit.jupiter.api.Test;

public class CountBits {
    @Test
    public void test(){
        int a=count(2);
        int[] arr=countBits(2);
        System.out.println(arr);
    }
    public int[] countBits(int n) {
        int[] arr=new int[n+1];
        for (int i = 0; i <= n; i++) {
            arr[i]=countOnes(i);
        }
        return arr;
    }

    public int count(int n){
        if(n==0) return 0;
        int count=0;
        while (n!=0){
            int x=n%2;
            if(x==1){
                count++;
            }
            n=n/2;
        }
        return count;
    }
    public int countOnes(int x) {
        int ones = 0;
        while (x > 0) {
            x &= (x - 1);
            ones++;
        }
        return ones;
    }
}
