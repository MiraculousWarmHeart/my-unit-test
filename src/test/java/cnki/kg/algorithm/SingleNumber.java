package cnki.kg.algorithm;

import org.junit.jupiter.api.Test;

import java.util.*;

public class SingleNumber {
    @Test
    public void test() {
        //int[] arr = new int[]{4,1,2,1,2};
        int[] arr = new int[]{2,2,1};
        int res2 = singleNumber(arr);
        System.out.println(res2);
    }
    public int singleNumber(int[] nums) {
        if(nums==null) return -1;
        if(nums.length==1) return nums[0];
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i++) {
            if(i==0&&nums[1]!=nums[0]){
                return nums[0];
            }
            else if(i==nums.length-1&&nums[i]!=nums[i-1]){
                return nums[i];
            }else{
                if(nums[i]!=nums[i-1]&&nums[i]!=nums[i+1]){
                    return nums[i];
                }
            }

        }
        return -1;
    }

    public int singleNumber2(int[] nums) {
        if(nums==null) return -1;
        if(nums.length==1) return nums[0];
        Map<Integer,Integer> mp=new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if(mp.containsKey(nums[i])){
                mp.put(nums[i],mp.get(nums[i])+1);
            }else {
                mp.put(nums[i],1);
            }

        }
        int res=-1;
        for (Integer key : mp.keySet()) {
            if(mp.get(key)==1){
                return key;
            }
            continue;
        }
        return res;
    }
    public String minNumber(int[] nums) {
        // String[] arr = new String[nums.length];
        // for (int i = 0; i < nums.length; i++) {
        //     arr[i] = String.valueOf(nums[i]);
        // }
        // Arrays.sort(arr, new Comparator<String>() {
        //     @Override
        //     public int compare(String o1, String o2) {
        //         return (o1 + o2).compareTo(o2 + o1);
        //     }
        // });
        // StringBuilder sb = new StringBuilder();
        // for (String s : arr) {
        //     sb.append(s);
        // }
        // return sb.toString();
        List<String> list = new ArrayList<>();
        for (int num : nums) {
            list.add(String.valueOf(num));
        }
        //list.sort((o1,o2)->(o1+o2).compareTo(o2+o1));
        list.sort((o1, o2) -> (o1 + o2).compareTo(o2 + o1));
        return String.join("", list);
    }
}
