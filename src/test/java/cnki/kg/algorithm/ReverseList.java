package cnki.kg.algorithm;

import java.util.Stack;

public class ReverseList {
    public ListNode reverseList(ListNode head) {
        Stack<ListNode> stack=new Stack<>();
        ListNode start = new ListNode(0);
        ListNode temp = start;
        while (head != null) {
            stack.push(head);
            head = head.next;
        }
        while (!stack.isEmpty()) {
            temp.next = stack.pop();
            temp = temp.next;
        }
        temp.next = null;
        return start.next;
    }
    public ListNode reverseList3(ListNode head) {
        ListNode prev = null;
        ListNode cur = head;
        ListNode temp = null;
        while (cur != null) {
            temp=cur.next;
            cur.next=prev;
            prev=cur;
            cur=temp;
        }
        return prev;

    }
}
