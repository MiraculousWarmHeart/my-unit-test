package cnki.kg.demo.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class KbaseConfig {
	@Value("${kbase.JdbcUrl}")
	private String url;

	@Value("${kbase.Username}")
	private String username;

	@Value("${kbase.Password}")
	private String password;

	@Value("${kbase.DriverClassName}")
	private String driverclassname;

}