package cnki.kg.demo.config;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Config;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.util.concurrent.TimeUnit;

@Configuration
public class Neo4jConfig {

    @Value("${spring.neo4j.url}")
    private String url;

    @Value("${spring.neo4j.username}")
    private String username;

    @Value("${spring.neo4j.password}")
    private String password;

    private static boolean cluster;
    @Value("${spring.neo4j.cluster}")
    public  void setCluster(boolean cluster) {
        Neo4jConfig.cluster = cluster;
    }




    /**
     * 图数据库驱动模式
     *
     * @return
     */
    @Lazy
    @Bean
    public Driver neo4jDriver() {
        Config config = Config.builder()
                .withMaxConnectionLifetime(5, TimeUnit.MINUTES)
                .withMaxConnectionPoolSize(2)
                .withLeakedSessionsLogging()
                .build();

        Driver driver = null;
        try {
            driver = GraphDatabase.driver(url, AuthTokens.basic(username, password), config);
        } catch (Exception e) {
            System.out.println(e);
        }
        return driver;
    }

    public static boolean isCluster() {
        return cluster;
    }
}