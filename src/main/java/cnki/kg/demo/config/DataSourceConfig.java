package cnki.kg.demo.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {
    @Bean(name = "mysqlDatasource")
    @Qualifier("mysqlDatasource")
    @ConfigurationProperties(prefix="spring.datasource.mysql")
    public DataSource mysqlDatasource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "sqlserverDataSource")
    @Qualifier("sqlserverDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.sqlserver")
    public DataSource sqlserverDataSource() {
        return DataSourceBuilder.create().build();
    }
    @Bean(name = "ctwhDataSource")
    @Qualifier("ctwhDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.ctwhmysql")
    public DataSource ctwhDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "szrwDataSource")
    @Qualifier("szrwDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.szrwmysql")
    public DataSource szrwDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name="mysqlJdbcTemplate")
    public JdbcTemplate mysqlJdbcTemplate (@Qualifier("mysqlDatasource")  DataSource dataSource ) {
        return new JdbcTemplate(dataSource);
    }

    @Bean(name = "sqlserverJdbcTemplate")
    public JdbcTemplate sqlserverJdbcTemplate(@Qualifier("sqlserverDataSource") DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }
    @Bean(name = "ctwhJdbcTemplate")
    public JdbcTemplate ctwhJdbcTemplate(@Qualifier("ctwhDataSource") DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }
    @Bean(name = "szrwJdbcTemplate")
    public JdbcTemplate szrwJdbcTemplate(@Qualifier("szrwDataSource") DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }
}
