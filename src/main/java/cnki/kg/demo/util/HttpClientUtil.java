package cnki.kg.demo.util;

import org.apache.http.*;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class HttpClientUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpClientUtil.class);

    private static CloseableHttpClient httpClient = HttpClients.createDefault();

    private static RequestConfig requestConfig;

    /**
     * 执行get请求
     *
     * @param url
     * @return
     * @throws Exception
     */
    public static String doGet(String url, Map<String, String> params, String encode) throws Exception {
        LOGGER.info("执行GET请求，URL = {}", url);
        if (null != params) {
            URIBuilder builder = new URIBuilder(url);
            for (Map.Entry<String, String> entry : params.entrySet()) {
                builder.setParameter(entry.getKey(), entry.getValue());
            }
            url = builder.build().toString();
        }
        // 创建http GET请求
        HttpGet httpGet = new HttpGet(url);
        //设置超时
        requestConfig = RequestConfig.custom().setConnectTimeout(100000).build();

        httpGet.setConfig(requestConfig);
        CloseableHttpResponse response = null;
        try {
            // 执行请求
            response = httpClient.execute(httpGet);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                if (encode == null) {
                    encode = "UTF-8";
                }
                return EntityUtils.toString(response.getEntity(), encode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            httpGet.abort();
            if (response != null) {
                response.close();
            }
            // 此处不能关闭httpClient，如果关闭httpClient，连接池也会销毁
        }
        return null;
    }

    public static String doGet(String url, String encode) throws Exception {
        return doGet(url, null, encode);
    }

    public static String doGet(String url) throws Exception {
        return doGet(url, null, null);
    }

    /**
     * 带参数的get请求
     *
     * @param url
     * @param params
     * @return
     * @throws Exception
     */
    public static String doGet(String url, Map<String, String> params) throws Exception {
        return doGet(url, params, null);
    }

    /**
     * 执行POST请求
     *
     * @param url
     * @param params
     * @return
     * @throws Exception
     */
    public static String doPost(String url, Map<String, String> params, String encode) throws Exception {
        // 创建http POST请求
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);

        if (null != params) {
            // 设置2个post参数，一个是scope、一个是q
            List<NameValuePair> parameters = new ArrayList<NameValuePair>(0);
            for (Map.Entry<String, String> entry : params.entrySet()) {
                parameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }

            // 构造一个form表单式的实体
            UrlEncodedFormEntity formEntity = null;
            if (encode != null) {
                formEntity = new UrlEncodedFormEntity(parameters, encode);
            } else {
                formEntity = new UrlEncodedFormEntity(parameters);
            }
            // 将请求实体设置到httpPost对象中
            httpPost.setEntity(formEntity);
        }

        CloseableHttpResponse response = null;
        try {
            // 执行请求
            response = httpClient.execute(httpPost);
            // 判断返回状态是否为200
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                return EntityUtils.toString(response.getEntity(), "UTF-8");
            }
        } finally {
            if (response != null) {
                response.close();
            }
        }
        return null;
    }

    /**
     * post请求(用于key-value格式的参数)
     *
     * @param url
     * @param params
     * @return
     */
    public static String doHttpPost(String url, Map params) {
        BufferedReader in = null;
        try {
            // 定义HttpClient
            HttpClient client = new DefaultHttpClient();
            // 实例化HTTP方法
            HttpPost request = new HttpPost();
            request.setURI(new URI(url));
            //设置参数
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            for (Iterator iter = params.keySet().iterator(); iter.hasNext(); ) {
                String name = (String) iter.next();
                String value = String.valueOf(params.get(name));
                nvps.add(new BasicNameValuePair(name, value));
            }
            request.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
            HttpResponse response = client.execute(request);
            int code = response.getStatusLine().getStatusCode();
            if (code == 200) {    //请求成功
                in = new BufferedReader(new InputStreamReader(response.getEntity()
                        .getContent(), "utf-8"));
                StringBuffer sb = new StringBuffer("");
                String line = "";
                String NL = System.getProperty("line.separator");
                while ((line = in.readLine()) != null) {
                    sb.append(line + NL);
                }

                in.close();

                return sb.toString();
            } else {    //
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * post请求(用于key-value格式的参数)
     *
     * @param url
     * @param params
     * @return
     */
    public static String doHttpPost(String url, Map params, String xAuthToken) {
        BufferedReader in = null;
        try {
            // 定义HttpClient
            HttpClient client = new DefaultHttpClient();
            // 实例化HTTP方法
            HttpPost request = new HttpPost();
            request.setURI(new URI(url));
            request.setHeader("x-auth-token", xAuthToken);
            //设置参数
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            for (Iterator iter = params.keySet().iterator(); iter.hasNext(); ) {
                String name = (String) iter.next();
                String value = String.valueOf(params.get(name));
                nvps.add(new BasicNameValuePair(name, value));
            }
            request.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
            HttpResponse response = client.execute(request);
            int code = response.getStatusLine().getStatusCode();
            if (code == 200) {    //请求成功
                in = new BufferedReader(new InputStreamReader(response.getEntity()
                        .getContent(), "utf-8"));
                StringBuffer sb = new StringBuffer("");
                String line = "";
                String NL = System.getProperty("line.separator");
                while ((line = in.readLine()) != null) {
                    sb.append(line + NL);
                }

                in.close();

                return sb.toString();
            } else {    //
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 执行POST请求
     *
     * @param url
     * @param params
     * @return
     * @throws Exception
     */
    public static String doPost(String url, Map<String, String> params) throws Exception {
        // 创建http POST请求
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);

        if (null != params) {
            // 设置2个post参数，一个是scope、一个是q
            List<NameValuePair> parameters = new ArrayList<NameValuePair>(0);
            for (Map.Entry<String, String> entry : params.entrySet()) {
                parameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }

            // 构造一个form表单式的实体
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(parameters);
            // 将请求实体设置到httpPost对象中
            httpPost.setEntity(formEntity);
        }

        CloseableHttpResponse response = null;
        try {
            // 执行请求
            response = httpClient.execute(httpPost);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                return EntityUtils.toString(response.getEntity(), "UTF-8");
            }
        } finally {
            if (response != null) {
                response.close();
            }
        }
        return null;
    }

    public static String doPostJson(String url, String json) throws Exception {
        final String APPLICATION_JSON = "application/json";
        final String CONTENT_TYPE_TEXT_JSON = "text/json";
        // 创建http POST请求
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        StringEntity stringEntity = null;
        if (null != json) {
            //设置请求体为 字符串
            stringEntity = new StringEntity(json, "UTF-8");
            httpPost.setEntity(stringEntity);
        }

        CloseableHttpResponse response = null;
        try {
            // 执行请求
            response = httpClient.execute(httpPost);
            // 判断返回状态是否为200
            int statusCode = response.getStatusLine().getStatusCode();
            String newuri = "";
            if (statusCode == 200) {
                return EntityUtils.toString(response.getEntity(), "UTF-8");
            } else if ((statusCode == HttpStatus.SC_MOVED_TEMPORARILY) || (statusCode == HttpStatus.SC_MOVED_PERMANENTLY) || (statusCode == HttpStatus.SC_SEE_OTHER) || (statusCode == HttpStatus.SC_TEMPORARY_REDIRECT)) {
                Header header = response.getFirstHeader("location"); // 跳转的目标地址是在 HTTP-HEAD 中的
                newuri = header.getValue(); // 这就是跳转后的地址，再向这个地址发出新申请，以便得到跳转后的信息是啥。
                System.out.println(newuri);
                httpPost = new HttpPost(newuri);
                httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
                stringEntity.setContentType(CONTENT_TYPE_TEXT_JSON);
                response = httpClient.execute(httpPost);
                statusCode = response.getStatusLine().getStatusCode();
                System.out.println("login" + statusCode);
                return EntityUtils.toString(response.getEntity(), "UTF-8");
            }
        } finally {
            if (response != null) {
                response.close();
            }
        }
        return null;
    }

    public static String post1(String url, String body) {
        CloseableHttpClient httpClient = null;
        HttpPost httpPost = null;
        String result = null;
        try {
            httpClient = HttpClients.createDefault();
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(20000).setConnectTimeout(20000).build();
            httpPost = new HttpPost(url);
            httpPost.setConfig(requestConfig);
            httpPost.setEntity(new StringEntity(body));
            CloseableHttpResponse response = httpClient.execute(httpPost);
            HttpEntity httpEntity = response.getEntity();
            result = EntityUtils.toString(httpEntity, "utf-8");
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (httpPost != null) {
                    httpPost.releaseConnection();
                }
                if (httpClient != null) {
                    httpClient.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static void login(HttpClient client) throws Exception {
        final String APPLICATION_JSON = "application/json";
        final String CONTENT_TYPE_TEXT_JSON = "text/json";
        String url = "http://192.168.105.88/materail/material/add";
        String js = "{\"material\":{\"content\":\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAK8AAACvCAIAAAAE8BkiAAAFuElEQVR42u3dW47bMBAFUe9/085XkGQAU2Tf6pbglD6NGUkkj0H2Y5LX28vr9/VyCrx+anhl15/blX548cnq1T//cHifnZc/GvLRq4b3SVZQDWpQgxp2NJxtM6VBUneu3RA3XRsy9Yja9H76LTWoQQ1qONJQ2/h31gw/kYQEJ+GG46LOH2pQgxrUcKOG2khqu+BAIEdt6uHLhwc1NahBDWr4Ag1h2NaXoQuHQ3031KAGNajhmRrCek9fSNa38eMR5pEP6hEPykyrQQ1q+FIN1LbqJ3d98sRuFz9Rg+vxFRqoa7JKFJbTwm0+PJH0lcqi5VODGtSghs8awlUMI8PJ+G0yOzmQJGUrbWpQgxrUsNAwUICnqjJUq8HAaYNqvAifpQY1qEENiIZw66VK8t25tjxEpA4rA+F6YYBqUIMa1HCkoRZK4QsTTh/e+oCvx8AA91GqQQ1qUMNCQy1jGEZH4bDDYI9KpPadG2o3rH391KAGNaiB1VDzgSf4qAwmVYgaiEvxQwZwblCDGtTw32hgt5/yFk7NI1VO66vY3RIMX76qGtSgBjXUcpHh1ntLuahvf+1bj/D4goxCDWpQgxp2sk94+SpcTryFIpTXF12HdT7k9KMGNahBDQsNYcYQn74wjAwLYzVwYeBNpWiTcalBDWpQw46G8NzQV8uhyjyTge5kIhXJYKpBDWpQQ00DdVzAV5ra1KmAGd/UkZaFo3OVGtSgBjUsNIRllclYCG+qwKHgJ62x+F8NalCDGigNfVDw8tVAcwYeS+PB8P5rqEENalDDjoZ7t+e+iZhMm1KxItXNsP/FVoMa1KCGo3MDHvAMBHIDCb6+c0N41glrXWpQgxrUUNDQt1STy0Dt7tQqUi0dTd8NNahBDWpYaKDSguFb9sVU+KtSkSE+5GRcalCDGtSwowHPM+IFJCqdh6f8Jpk2jUsNalCDGnYizL78V7jVUcEVlcXDk63hjIUBvBrUoAY1XGoIs2YDKchQXm1XxmO8MBMajuLyVdWgBjWoYaEB36vCCDNEgK/05PxQhSg1qEENamA1DOS/8DNBbVeezFeyS/Xm/p16NahBDWpINDQlucqhFBXjha1+AyMN1x7pR1GDGtSgBrwTLgwR8VTmQJ8ZFc1SK81+2dSgBjWoYaf3qa+Lq5ZnHCjzhMtABXt4srV+blCDGtSghmpMEWYMa51nYVKyT164Qn2Jy+TpalCDGtSwE1MMzEi4eAMRJr5Ufc9if10NalCDGo409JX/H7L2k9FsuMD4kC/6G9SgBjWoYU9DbfH62hHwFORDzgRhApRaAjWoQQ1quNTQFyKGW3g4R3jhpy9pG57GwoeqQQ1qUEPh7ynCpCQVRlJLXntV/KyDd2AM9TeoQQ1qUAP3n3NTPWRU1Bc+YqCnralT4TT5qwY1qEEN839PMbDSA7tyeFTqM0QtwUVmWg1qUIMaDvsbqMoNvlR4ArSvpETNWDi9u+cGNahBDWpYVq36+hv6cm1UhEnhDik3Lfnih9WgBjWoYUcDXm4Pq/5UlNU3odT7PCGnqQY1qEENCw21XbA2+wM9EH376y0FtnCi9pdSDWpQgxp2zg21vYoKrmqJQjw2w0NfPC6lzkxqUIMa1FDQQMWTfYUWPDc62RWBP4udKDWoQQ1qWGgICzZ4OwKVG321XbUFHqjYJV7VoAY1qAHXQCUcqRuGicu+glYYao6V7tSgBjWogYow+3ZKvHgWPqsvVkz61d4N/3CzGtSgBjXsa6ht6tRvJbX51srWLaU76g33UapBDWpQw5EGfPbDeGkylYmX3AbCbKQDQw1qUIMaBjRQG214yOiLHvFcbV8vReHspQY1qEENt2igEITRY3gmeNrph4r2eyNMNahBDd+uoS+rGC4VXjybjBXDIhw+G2pQgxrUUNCA9zeEQWNfpwKepsQj8IGXv4gw1aAGNajhLw1eXmrw+uf6BV8+c33Y6PYcAAAAAElFTkSuQmCC\",\"parameter\":\"{\\\"Indicates\\\":\\\"[{\\\"IndicateName\\\":\\\"国民总收入\\\",\\\"Unit\\\":\\\"亿元\\\",\\\"FormularIndicateName\\\":\\\"国民总收入\\\"}]\\\",\\\"Times\\\":\\\"2010-2017\\\",\\\"Zones\\\":\\\"[{\\\"XjCode\\\":\\\"xj35\\\",\\\"ZoneCode\\\":\\\"100000\\\",\\\"ZoneName\\\":\\\"中国\\\"},{\\\"XjCode\\\":\\\"xj01\\\",\\\"ZoneCode\\\":\\\"110000\\\",\\\"ZoneName\\\":\\\"北京市\\\"}]\\\",\\\"moduleType\\\":\\\"\\\"}\",\"source\":\"\",\"title\":\"BDMSMATRAILTEST\",\"type\":1,\"url\":\"http://www.baidu.com\"}}";
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
        StringEntity se = new StringEntity(js);
        se.setContentType(CONTENT_TYPE_TEXT_JSON);

        httpPost.setEntity(se);

        HttpResponse response = null;

        response = client.execute(httpPost);

        //----------判断是否重定向开始
        int code = response.getStatusLine().getStatusCode();
        String newuri = "";
        if (code == 302) {
            Header header = response.getFirstHeader("location"); // 跳转的目标地址是在 HTTP-HEAD 中的
            newuri = header.getValue(); // 这就是跳转后的地址，再向这个地址发出新申请，以便得到跳转后的信息是啥。
            System.out.println(newuri);
            System.out.println(code);

            httpPost = new HttpPost(newuri);
            httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");

            se = new StringEntity(js);
            se.setContentType(CONTENT_TYPE_TEXT_JSON);

            httpPost.setEntity(se);

            response = client.execute(httpPost);
            code = response.getStatusLine().getStatusCode();
            System.out.println("login" + code);
        }

        //------------重定向结束
        HttpEntity entity = null;
        entity = response.getEntity();
        String s2 = EntityUtils.toString(entity, "UTF-8");
        System.out.println(s2);
    }

    public static void main(String[] args) {
//    	Material material = new Material();
//    	material.setTitle("添加素材TEST");
//    	material.setContent("data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAK8AAACvCAIAAAAE8BkiAAAFuElEQVR42u3dW47bMBAFUe9/085XkGQAU2Tf6pbglD6NGUkkj0H2Y5LX28vr9/VyCrx+anhl15/blX548cnq1T//cHifnZc/GvLRq4b3SVZQDWpQgxp2NJxtM6VBUneu3RA3XRsy9Yja9H76LTWoQQ1qONJQ2/h31gw/kYQEJ+GG46LOH2pQgxrUcKOG2khqu+BAIEdt6uHLhwc1NahBDWr4Ag1h2NaXoQuHQ3031KAGNajhmRrCek9fSNa38eMR5pEP6hEPykyrQQ1q+FIN1LbqJ3d98sRuFz9Rg+vxFRqoa7JKFJbTwm0+PJH0lcqi5VODGtSghs8awlUMI8PJ+G0yOzmQJGUrbWpQgxrUsNAwUICnqjJUq8HAaYNqvAifpQY1qEENiIZw66VK8t25tjxEpA4rA+F6YYBqUIMa1HCkoRZK4QsTTh/e+oCvx8AA91GqQQ1qUMNCQy1jGEZH4bDDYI9KpPadG2o3rH391KAGNaiB1VDzgSf4qAwmVYgaiEvxQwZwblCDGtTw32hgt5/yFk7NI1VO66vY3RIMX76qGtSgBjXUcpHh1ntLuahvf+1bj/D4goxCDWpQgxp2sk94+SpcTryFIpTXF12HdT7k9KMGNahBDQsNYcYQn74wjAwLYzVwYeBNpWiTcalBDWpQw46G8NzQV8uhyjyTge5kIhXJYKpBDWpQQ00DdVzAV5ra1KmAGd/UkZaFo3OVGtSgBjUsNIRllclYCG+qwKHgJ62x+F8NalCDGigNfVDw8tVAcwYeS+PB8P5rqEENalDDjoZ7t+e+iZhMm1KxItXNsP/FVoMa1KCGo3MDHvAMBHIDCb6+c0N41glrXWpQgxrUUNDQt1STy0Dt7tQqUi0dTd8NNahBDWpYaKDSguFb9sVU+KtSkSE+5GRcalCDGtSwowHPM+IFJCqdh6f8Jpk2jUsNalCDGnYizL78V7jVUcEVlcXDk63hjIUBvBrUoAY1XGoIs2YDKchQXm1XxmO8MBMajuLyVdWgBjWoYaEB36vCCDNEgK/05PxQhSg1qEENamA1DOS/8DNBbVeezFeyS/Xm/p16NahBDWpINDQlucqhFBXjha1+AyMN1x7pR1GDGtSgBrwTLgwR8VTmQJ8ZFc1SK81+2dSgBjWoYaf3qa+Lq5ZnHCjzhMtABXt4srV+blCDGtSghmpMEWYMa51nYVKyT164Qn2Jy+TpalCDGtSwE1MMzEi4eAMRJr5Ufc9if10NalCDGo409JX/H7L2k9FsuMD4kC/6G9SgBjWoYU9DbfH62hHwFORDzgRhApRaAjWoQQ1quNTQFyKGW3g4R3jhpy9pG57GwoeqQQ1qUEPh7ynCpCQVRlJLXntV/KyDd2AM9TeoQQ1qUAP3n3NTPWRU1Bc+YqCnralT4TT5qwY1qEEN839PMbDSA7tyeFTqM0QtwUVmWg1qUIMaDvsbqMoNvlR4ArSvpETNWDi9u+cGNahBDWpYVq36+hv6cm1UhEnhDik3Lfnih9WgBjWoYUcDXm4Pq/5UlNU3odT7PCGnqQY1qEENCw21XbA2+wM9EH376y0FtnCi9pdSDWpQgxp2zg21vYoKrmqJQjw2w0NfPC6lzkxqUIMa1FDQQMWTfYUWPDc62RWBP4udKDWoQQ1qWGgICzZ4OwKVG321XbUFHqjYJV7VoAY1qAHXQCUcqRuGicu+glYYao6V7tSgBjWogYow+3ZKvHgWPqsvVkz61d4N/3CzGtSgBjXsa6ht6tRvJbX51srWLaU76g33UapBDWpQw5EGfPbDeGkylYmX3AbCbKQDQw1qUIMaBjRQG214yOiLHvFcbV8vReHspQY1qEENt2igEITRY3gmeNrph4r2eyNMNahBDd+uoS+rGC4VXjybjBXDIhw+G2pQgxrUUNCA9zeEQWNfpwKepsQj8IGXv4gw1aAGNajhLw1eXmrw+uf6BV8+c33Y6PYcAAAAAElFTkSuQmCC");
//    	material.setParameter("{\"Indicates\":\"[{\"IndicateName\":\"国民总收入\",\"Unit\":\"亿元\",\"FormularIndicateName\":\"国民总收入\"}]\",\"Times\":\"2010-2017\",\"Zones\":\"[{\"XjCode\":\"xj35\",\"ZoneCode\":\"100000\",\"ZoneName\":\"中国\"},{\"XjCode\":\"xj01\",\"ZoneCode\":\"110000\",\"ZoneName\":\"北京市\"}]\",\"moduleType\":\"\"}");
//    	material.setSource("");
//    	material.setType(1);
//    	material.setUrl("http://www.baidu.com");
//    	String json = JSONObject.toJSONString(material);//将java对象转换为字符串
//    	String newJson = "{\"material\":"+json+"}";
//    	System.out.println("#####"+newJson);
//    	try {
//			String result = HttpClientUtil.doPostJson("http://192.168.105.88/materail/material/add", newJson);
//			System.out.println("@@@@@@@@@"+result);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
        try {
            HttpClient client = HttpClients.createDefault();
            login(client);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}