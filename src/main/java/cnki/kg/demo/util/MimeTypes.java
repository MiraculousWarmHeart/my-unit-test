package cnki.kg.demo.util;

import java.util.HashMap;

public class MimeTypes {
    public static final String MIME_APPLICATION_ANDREW_INSET = "application/andrew-inset";
    public static final String MIME_APPLICATION_JSON = "application/json";
    public static final String MIME_APPLICATION_ZIP = "application/zip";
    public static final String MIME_APPLICATION_X_GZIP = "application/x-gzip";
    public static final String MIME_APPLICATION_TGZ = "application/tgz";
    public static final String MIME_APPLICATION_MSWORD = "application/msword";
    public static final String MIME_APPLICATION_MSWORD_2007 = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    public static final String MIME_APPLICATION_VND_TEXT = "application/vnd.oasis.opendocument.text";
    public static final String MIME_APPLICATION_POSTSCRIPT = "application/postscript";
    public static final String MIME_APPLICATION_PDF = "application/pdf";
    public static final String MIME_APPLICATION_JNLP = "application/jnlp";
    public static final String MIME_APPLICATION_MAC_BINHEX40 = "application/mac-binhex40";
    public static final String MIME_APPLICATION_MAC_COMPACTPRO = "application/mac-compactpro";
    public static final String MIME_APPLICATION_MATHML_XML = "application/mathml+xml";
    public static final String MIME_APPLICATION_OCTET_STREAM = "application/octet-stream";
    public static final String MIME_APPLICATION_ODA = "application/oda";
    public static final String MIME_APPLICATION_RDF_XML = "application/rdf+xml";
    public static final String MIME_APPLICATION_JAVA_ARCHIVE = "application/java-archive";
    public static final String MIME_APPLICATION_RDF_SMIL = "application/smil";
    public static final String MIME_APPLICATION_SRGS = "application/srgs";
    public static final String MIME_APPLICATION_SRGS_XML = "application/srgs+xml";
    public static final String MIME_APPLICATION_VND_MIF = "application/vnd.mif";
    public static final String MIME_APPLICATION_VND_MSEXCEL = "application/vnd.ms-excel";
    public static final String MIME_APPLICATION_VND_MSEXCEL_2007 = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String MIME_APPLICATION_VND_SPREADSHEET = "application/vnd.oasis.opendocument.spreadsheet";
    public static final String MIME_APPLICATION_VND_MSPOWERPOINT = "application/vnd.ms-powerpoint";
    public static final String MIME_APPLICATION_VND_RNREALMEDIA = "application/vnd.rn-realmedia";
    public static final String MIME_APPLICATION_X_BCPIO = "application/x-bcpio";
    public static final String MIME_APPLICATION_X_CDLINK = "application/x-cdlink";
    public static final String MIME_APPLICATION_X_CHESS_PGN = "application/x-chess-pgn";
    public static final String MIME_APPLICATION_X_CPIO = "application/x-cpio";
    public static final String MIME_APPLICATION_X_CSH = "application/x-csh";
    public static final String MIME_APPLICATION_X_DIRECTOR = "application/x-director";
    public static final String MIME_APPLICATION_X_DVI = "application/x-dvi";
    public static final String MIME_APPLICATION_X_FUTURESPLASH = "application/x-futuresplash";
    public static final String MIME_APPLICATION_X_GTAR = "application/x-gtar";
    public static final String MIME_APPLICATION_X_HDF = "application/x-hdf";
    public static final String MIME_APPLICATION_X_JAVASCRIPT = "application/x-javascript";
    public static final String MIME_APPLICATION_X_KOAN = "application/x-koan";
    public static final String MIME_APPLICATION_X_LATEX = "application/x-latex";
    public static final String MIME_APPLICATION_X_NETCDF = "application/x-netcdf";
    public static final String MIME_APPLICATION_X_OGG = "application/x-ogg";
    public static final String MIME_APPLICATION_X_SH = "application/x-sh";
    public static final String MIME_APPLICATION_X_SHAR = "application/x-shar";
    public static final String MIME_APPLICATION_X_SHOCKWAVE_FLASH = "application/x-shockwave-flash";
    public static final String MIME_APPLICATION_X_STUFFIT = "application/x-stuffit";
    public static final String MIME_APPLICATION_X_SV4CPIO = "application/x-sv4cpio";
    public static final String MIME_APPLICATION_X_SV4CRC = "application/x-sv4crc";
    public static final String MIME_APPLICATION_X_TAR = "application/x-tar";
    public static final String MIME_APPLICATION_X_RAR_COMPRESSED = "application/x-rar-compressed";
    public static final String MIME_APPLICATION_X_TCL = "application/x-tcl";
    public static final String MIME_APPLICATION_X_TEX = "application/x-tex";
    public static final String MIME_APPLICATION_X_TEXINFO = "application/x-texinfo";
    public static final String MIME_APPLICATION_X_TROFF = "application/x-troff";
    public static final String MIME_APPLICATION_X_TROFF_MAN = "application/x-troff-man";
    public static final String MIME_APPLICATION_X_TROFF_ME = "application/x-troff-me";
    public static final String MIME_APPLICATION_X_TROFF_MS = "application/x-troff-ms";
    public static final String MIME_APPLICATION_X_USTAR = "application/x-ustar";
    public static final String MIME_APPLICATION_X_WAIS_SOURCE = "application/x-wais-source";
    public static final String MIME_APPLICATION_VND_MOZZILLA_XUL_XML = "application/vnd.mozilla.xul+xml";
    public static final String MIME_APPLICATION_XHTML_XML = "application/xhtml+xml";
    public static final String MIME_APPLICATION_XSLT_XML = "application/xslt+xml";
    public static final String MIME_APPLICATION_XML = "application/xml";
    public static final String MIME_APPLICATION_XML_DTD = "application/xml-dtd";
    public static final String MIME_IMAGE_BMP = "image/bmp";
    public static final String MIME_IMAGE_CGM = "image/cgm";
    public static final String MIME_IMAGE_GIF = "image/gif";
    public static final String MIME_IMAGE_IEF = "image/ief";
    public static final String MIME_IMAGE_JPEG = "image/jpeg";
    public static final String MIME_IMAGE_TIFF = "image/tiff";
    public static final String MIME_IMAGE_PNG = "image/png";
    public static final String MIME_IMAGE_SVG_XML = "image/svg+xml";
    public static final String MIME_IMAGE_VND_DJVU = "image/vnd.djvu";
    public static final String MIME_IMAGE_WAP_WBMP = "image/vnd.wap.wbmp";
    public static final String MIME_IMAGE_X_CMU_RASTER = "image/x-cmu-raster";
    public static final String MIME_IMAGE_X_ICON = "image/x-icon";
    public static final String MIME_IMAGE_X_PORTABLE_ANYMAP = "image/x-portable-anymap";
    public static final String MIME_IMAGE_X_PORTABLE_BITMAP = "image/x-portable-bitmap";
    public static final String MIME_IMAGE_X_PORTABLE_GRAYMAP = "image/x-portable-graymap";
    public static final String MIME_IMAGE_X_PORTABLE_PIXMAP = "image/x-portable-pixmap";
    public static final String MIME_IMAGE_X_RGB = "image/x-rgb";
    public static final String MIME_AUDIO_BASIC = "audio/basic";
    public static final String MIME_AUDIO_MIDI = "audio/midi";
    public static final String MIME_AUDIO_MPEG = "audio/mpeg";
    public static final String MIME_AUDIO_X_AIFF = "audio/x-aiff";
    public static final String MIME_AUDIO_X_MPEGURL = "audio/x-mpegurl";
    public static final String MIME_AUDIO_X_PN_REALAUDIO = "audio/x-pn-realaudio";
    public static final String MIME_AUDIO_X_WAV = "audio/x-wav";
    public static final String MIME_CHEMICAL_X_PDB = "chemical/x-pdb";
    public static final String MIME_CHEMICAL_X_XYZ = "chemical/x-xyz";
    public static final String MIME_MODEL_IGES = "model/iges";
    public static final String MIME_MODEL_MESH = "model/mesh";
    public static final String MIME_MODEL_VRLM = "model/vrml";
    public static final String MIME_TEXT_PLAIN = "text/plain";
    public static final String MIME_TEXT_RICHTEXT = "text/richtext";
    public static final String MIME_TEXT_RTF = "text/rtf";
    public static final String MIME_TEXT_HTML = "text/html";
    public static final String MIME_TEXT_CALENDAR = "text/calendar";
    public static final String MIME_TEXT_CSS = "text/css";
    public static final String MIME_TEXT_SGML = "text/sgml";
    public static final String MIME_TEXT_TAB_SEPARATED_VALUES = "text/tab-separated-values";
    public static final String MIME_TEXT_VND_WAP_XML = "text/vnd.wap.wml";
    public static final String MIME_TEXT_VND_WAP_WMLSCRIPT = "text/vnd.wap.wmlscript";
    public static final String MIME_TEXT_X_SETEXT = "text/x-setext";
    public static final String MIME_TEXT_X_COMPONENT = "text/x-component";
    public static final String MIME_VIDEO_QUICKTIME = "video/quicktime";
    public static final String MIME_VIDEO_MPEG = "video/mpeg";
    public static final String MIME_VIDEO_VND_MPEGURL = "video/vnd.mpegurl";
    public static final String MIME_VIDEO_X_MSVIDEO = "video/x-msvideo";
    public static final String MIME_VIDEO_X_MS_WMV = "video/x-ms-wmv";
    public static final String MIME_VIDEO_X_SGI_MOVIE = "video/x-sgi-movie";
    public static final String MIME_X_CONFERENCE_X_COOLTALK = "x-conference/x-cooltalk";
    private static HashMap<String, String> mimeTypeMapping=new HashMap<>();
    private static HashMap<String, String> extMapping=new HashMap<>();

    static {
        initMimeType();
        initExtType();
    }

    private static void initMimeType(){
        registerMimeType(MIME_APPLICATION_VND_MOZZILLA_XUL_XML, "xul");
        registerMimeType(MIME_APPLICATION_JSON, "json");
        registerMimeType(MIME_X_CONFERENCE_X_COOLTALK, "ice");
        registerMimeType(MIME_VIDEO_X_SGI_MOVIE, "movie");
        registerMimeType(MIME_VIDEO_X_MSVIDEO, "avi");
        registerMimeType(MIME_VIDEO_X_MS_WMV, "wmv");
        registerMimeType(MIME_VIDEO_VND_MPEGURL, "m4u");
        registerMimeType(MIME_TEXT_X_COMPONENT, "htc");
        registerMimeType(MIME_TEXT_X_SETEXT, "etx");
        registerMimeType(MIME_TEXT_VND_WAP_WMLSCRIPT, "wmls");
        registerMimeType(MIME_TEXT_VND_WAP_XML, "wml");
        registerMimeType(MIME_TEXT_TAB_SEPARATED_VALUES, "tsv");
        registerMimeType(MIME_TEXT_SGML, "sgml");
        registerMimeType(MIME_TEXT_CSS, "css");
        registerMimeType(MIME_TEXT_CALENDAR, "ics");
        registerMimeType(MIME_MODEL_VRLM, "vrlm");
        registerMimeType(MIME_MODEL_MESH, "mesh");
        registerMimeType(MIME_MODEL_IGES, "iges");
        registerMimeType(MIME_IMAGE_X_RGB, "rgb");
        registerMimeType(MIME_IMAGE_X_PORTABLE_PIXMAP, "ppm");
        registerMimeType(MIME_IMAGE_X_PORTABLE_GRAYMAP, "pgm");
        registerMimeType(MIME_IMAGE_X_PORTABLE_BITMAP, "pbm");
        registerMimeType(MIME_IMAGE_X_PORTABLE_ANYMAP, "pnm");
        registerMimeType(MIME_IMAGE_X_ICON, "ico");
        registerMimeType(MIME_IMAGE_X_CMU_RASTER, "ras");
        registerMimeType(MIME_IMAGE_WAP_WBMP, "wbmp");
        registerMimeType(MIME_IMAGE_VND_DJVU, "djvu");
        registerMimeType(MIME_IMAGE_SVG_XML, "svg");
        registerMimeType(MIME_IMAGE_IEF, "ief");
        registerMimeType(MIME_IMAGE_CGM, "cgm");
        registerMimeType(MIME_IMAGE_BMP, "bmp");
        registerMimeType(MIME_CHEMICAL_X_XYZ, "xyz");
        registerMimeType(MIME_CHEMICAL_X_PDB, "pdb");
        registerMimeType(MIME_AUDIO_X_PN_REALAUDIO, "ra");
        registerMimeType(MIME_AUDIO_X_MPEGURL, "m3u");
        registerMimeType(MIME_AUDIO_X_AIFF, "aiff");
        registerMimeType(MIME_AUDIO_MPEG, "mp3");
        registerMimeType(MIME_AUDIO_MIDI, "midi");
        registerMimeType(MIME_APPLICATION_XML_DTD, "dtd");
        registerMimeType(MIME_APPLICATION_XML, "xml");
        registerMimeType(MIME_APPLICATION_XSLT_XML, "xslt");
        registerMimeType(MIME_APPLICATION_XHTML_XML, "xhtml");
        registerMimeType(MIME_APPLICATION_X_WAIS_SOURCE, "src");
        registerMimeType(MIME_APPLICATION_X_USTAR, "ustar");
        registerMimeType(MIME_APPLICATION_X_TROFF_MS, "ms");
        registerMimeType(MIME_APPLICATION_X_TROFF_ME, "me");
        registerMimeType(MIME_APPLICATION_X_TROFF_MAN, "man");
        registerMimeType(MIME_APPLICATION_X_TROFF, "roff");
        registerMimeType(MIME_APPLICATION_X_TEXINFO, "texi");
        registerMimeType(MIME_APPLICATION_X_TEX, "tex");
        registerMimeType(MIME_APPLICATION_X_TCL, "tcl");
        registerMimeType(MIME_APPLICATION_X_SV4CRC, "sv4crc");
        registerMimeType(MIME_APPLICATION_X_SV4CPIO, "sv4cpio");
        registerMimeType(MIME_APPLICATION_X_STUFFIT, "sit");
        registerMimeType(MIME_APPLICATION_X_SHOCKWAVE_FLASH, "swf");
        registerMimeType(MIME_APPLICATION_X_SHAR, "shar");
        registerMimeType(MIME_APPLICATION_X_SH, "sh");
        registerMimeType(MIME_APPLICATION_X_NETCDF, "cdf");
        registerMimeType(MIME_APPLICATION_X_LATEX, "latex");
        registerMimeType(MIME_APPLICATION_X_KOAN, "skm");
        registerMimeType(MIME_APPLICATION_X_JAVASCRIPT, "js");
        registerMimeType(MIME_APPLICATION_X_HDF, "hdf");
        registerMimeType(MIME_APPLICATION_X_GTAR, "gtar");
        registerMimeType(MIME_APPLICATION_X_FUTURESPLASH, "spl");
        registerMimeType(MIME_APPLICATION_X_DVI, "dvi");
        registerMimeType(MIME_APPLICATION_X_DIRECTOR, "dir");
        registerMimeType(MIME_APPLICATION_X_CSH, "csh");
        registerMimeType(MIME_APPLICATION_X_CPIO, "cpio");
        registerMimeType(MIME_APPLICATION_X_CHESS_PGN, "pgn");
        registerMimeType(MIME_APPLICATION_X_CDLINK, "vcd");
        registerMimeType(MIME_APPLICATION_X_BCPIO, "bcpio");
        registerMimeType(MIME_APPLICATION_VND_RNREALMEDIA, "rm");
        registerMimeType(MIME_APPLICATION_VND_MSPOWERPOINT, "ppt");
        registerMimeType(MIME_APPLICATION_VND_MIF, "mif");
        registerMimeType(MIME_APPLICATION_SRGS_XML, "grxml");
        registerMimeType(MIME_APPLICATION_SRGS, "gram");
        registerMimeType(MIME_APPLICATION_RDF_SMIL, "smil");
        registerMimeType(MIME_APPLICATION_RDF_XML, "rdf");
        registerMimeType(MIME_APPLICATION_X_OGG, "ogg");
        registerMimeType(MIME_APPLICATION_ODA, "oda");
        registerMimeType(MIME_APPLICATION_MATHML_XML, "mathml");
        registerMimeType(MIME_APPLICATION_MAC_COMPACTPRO, "cpt");
        registerMimeType(MIME_APPLICATION_MAC_BINHEX40, "hqx");
        registerMimeType(MIME_APPLICATION_JNLP, "jnlp");
        registerMimeType(MIME_APPLICATION_ANDREW_INSET, "ez");
        registerMimeType(MIME_TEXT_PLAIN, "txt");
        registerMimeType(MIME_TEXT_RTF, "rtf");
        registerMimeType(MIME_TEXT_RICHTEXT, "rtx");
        registerMimeType(MIME_TEXT_HTML, "html");
        registerMimeType(MIME_APPLICATION_ZIP, "zip");
        registerMimeType(MIME_APPLICATION_X_RAR_COMPRESSED, "rar");
        registerMimeType(MIME_APPLICATION_X_GZIP, "gzip");
        registerMimeType(MIME_APPLICATION_TGZ, "tgz");
        registerMimeType(MIME_APPLICATION_X_TAR, "tar");
        registerMimeType(MIME_IMAGE_GIF, "gif");
        registerMimeType(MIME_IMAGE_JPEG, "jpg");
        registerMimeType(MIME_IMAGE_TIFF, "tiff");
        registerMimeType(MIME_IMAGE_PNG, "png");
        registerMimeType(MIME_AUDIO_BASIC, "au");
        registerMimeType(MIME_AUDIO_X_WAV, "wav");
        registerMimeType(MIME_VIDEO_QUICKTIME, "mov");
        registerMimeType(MIME_VIDEO_MPEG, "mpg");
        registerMimeType(MIME_APPLICATION_MSWORD, "doc");
        registerMimeType(MIME_APPLICATION_MSWORD_2007, "docx");
        registerMimeType(MIME_APPLICATION_VND_TEXT, "odt");
        registerMimeType(MIME_APPLICATION_VND_MSEXCEL, "xls");
        registerMimeType(MIME_APPLICATION_VND_SPREADSHEET, "ods");
        registerMimeType(MIME_APPLICATION_POSTSCRIPT, "ps");
        registerMimeType(MIME_APPLICATION_PDF, "pdf");
        registerMimeType(MIME_APPLICATION_OCTET_STREAM, "exe");
        registerMimeType(MIME_APPLICATION_JAVA_ARCHIVE, "jar");
    }
    private static void initExtType(){
        registerExtType("xul", MIME_APPLICATION_VND_MOZZILLA_XUL_XML);
        registerExtType("json", MIME_APPLICATION_JSON);
        registerExtType("ice", MIME_X_CONFERENCE_X_COOLTALK);
        registerExtType("movie", MIME_VIDEO_X_SGI_MOVIE);
        registerExtType("avi", MIME_VIDEO_X_MSVIDEO);
        registerExtType("wmv", MIME_VIDEO_X_MS_WMV);
        registerExtType("m4u", MIME_VIDEO_VND_MPEGURL);
        registerExtType("mxu", MIME_VIDEO_VND_MPEGURL);
        registerExtType("htc", MIME_TEXT_X_COMPONENT);
        registerExtType("etx", MIME_TEXT_X_SETEXT);
        registerExtType("wmls", MIME_TEXT_VND_WAP_WMLSCRIPT);
        registerExtType("wml", MIME_TEXT_VND_WAP_XML);
        registerExtType("tsv", MIME_TEXT_TAB_SEPARATED_VALUES);
        registerExtType("sgm", MIME_TEXT_SGML);
        registerExtType("sgml", MIME_TEXT_SGML);
        registerExtType("css", MIME_TEXT_CSS);
        registerExtType("ifb", MIME_TEXT_CALENDAR);
        registerExtType("ics", MIME_TEXT_CALENDAR);
        registerExtType("wrl", MIME_MODEL_VRLM);
        registerExtType("vrlm", MIME_MODEL_VRLM);
        registerExtType("silo", MIME_MODEL_MESH);
        registerExtType("mesh", MIME_MODEL_MESH);
        registerExtType("msh", MIME_MODEL_MESH);
        registerExtType("iges", MIME_MODEL_IGES);
        registerExtType("igs", MIME_MODEL_IGES);
        registerExtType("rgb", MIME_IMAGE_X_RGB);
        registerExtType("ppm", MIME_IMAGE_X_PORTABLE_PIXMAP);
        registerExtType("pgm", MIME_IMAGE_X_PORTABLE_GRAYMAP);
        registerExtType("pbm", MIME_IMAGE_X_PORTABLE_BITMAP);
        registerExtType("pnm", MIME_IMAGE_X_PORTABLE_ANYMAP);
        registerExtType("ico", MIME_IMAGE_X_ICON);
        registerExtType("ras", MIME_IMAGE_X_CMU_RASTER);
        registerExtType("wbmp", MIME_IMAGE_WAP_WBMP);
        registerExtType("djv", MIME_IMAGE_VND_DJVU);
        registerExtType("djvu", MIME_IMAGE_VND_DJVU);
        registerExtType("svg", MIME_IMAGE_SVG_XML);
        registerExtType("ief", MIME_IMAGE_IEF);
        registerExtType("cgm", MIME_IMAGE_CGM);
        registerExtType("bmp", MIME_IMAGE_BMP);
        registerExtType("xyz", MIME_CHEMICAL_X_XYZ);
        registerExtType("pdb", MIME_CHEMICAL_X_PDB);
        registerExtType("ra", MIME_AUDIO_X_PN_REALAUDIO);
        registerExtType("ram", MIME_AUDIO_X_PN_REALAUDIO);
        registerExtType("m3u", MIME_AUDIO_X_MPEGURL);
        registerExtType("aifc", MIME_AUDIO_X_AIFF);
        registerExtType("aif", MIME_AUDIO_X_AIFF);
        registerExtType("aiff", MIME_AUDIO_X_AIFF);
        registerExtType("mp3", MIME_AUDIO_MPEG);
        registerExtType("mp2", MIME_AUDIO_MPEG);
        registerExtType("mp1", MIME_AUDIO_MPEG);
        registerExtType("mpga", MIME_AUDIO_MPEG);
        registerExtType("kar", MIME_AUDIO_MIDI);
        registerExtType("mid", MIME_AUDIO_MIDI);
        registerExtType("midi", MIME_AUDIO_MIDI);
        registerExtType("dtd", MIME_APPLICATION_XML_DTD);
        registerExtType("xsl", MIME_APPLICATION_XML);
        registerExtType("xml", MIME_APPLICATION_XML);
        registerExtType("xslt", MIME_APPLICATION_XSLT_XML);
        registerExtType("xht", MIME_APPLICATION_XHTML_XML);
        registerExtType("xhtml", MIME_APPLICATION_XHTML_XML);
        registerExtType("src", MIME_APPLICATION_X_WAIS_SOURCE);
        registerExtType("ustar", MIME_APPLICATION_X_USTAR);
        registerExtType("ms", MIME_APPLICATION_X_TROFF_MS);
        registerExtType("me", MIME_APPLICATION_X_TROFF_ME);
        registerExtType("man", MIME_APPLICATION_X_TROFF_MAN);
        registerExtType("roff", MIME_APPLICATION_X_TROFF);
        registerExtType("tr", MIME_APPLICATION_X_TROFF);
        registerExtType("t", MIME_APPLICATION_X_TROFF);
        registerExtType("texi", MIME_APPLICATION_X_TEXINFO);
        registerExtType("texinfo", MIME_APPLICATION_X_TEXINFO);
        registerExtType("tex", MIME_APPLICATION_X_TEX);
        registerExtType("tcl", MIME_APPLICATION_X_TCL);
        registerExtType("sv4crc", MIME_APPLICATION_X_SV4CRC);
        registerExtType("sv4cpio", MIME_APPLICATION_X_SV4CPIO);
        registerExtType("sit", MIME_APPLICATION_X_STUFFIT);
        registerExtType("swf", MIME_APPLICATION_X_SHOCKWAVE_FLASH);
        registerExtType("shar", MIME_APPLICATION_X_SHAR);
        registerExtType("sh", MIME_APPLICATION_X_SH);
        registerExtType("cdf", MIME_APPLICATION_X_NETCDF);
        registerExtType("nc", MIME_APPLICATION_X_NETCDF);
        registerExtType("latex", MIME_APPLICATION_X_LATEX);
        registerExtType("skm", MIME_APPLICATION_X_KOAN);
        registerExtType("skt", MIME_APPLICATION_X_KOAN);
        registerExtType("skd", MIME_APPLICATION_X_KOAN);
        registerExtType("skp", MIME_APPLICATION_X_KOAN);
        registerExtType("js", MIME_APPLICATION_X_JAVASCRIPT);
        registerExtType("hdf", MIME_APPLICATION_X_HDF);
        registerExtType("gtar", MIME_APPLICATION_X_GTAR);
        registerExtType("spl", MIME_APPLICATION_X_FUTURESPLASH);
        registerExtType("dvi", MIME_APPLICATION_X_DVI);
        registerExtType("dxr", MIME_APPLICATION_X_DIRECTOR);
        registerExtType("dir", MIME_APPLICATION_X_DIRECTOR);
        registerExtType("dcr", MIME_APPLICATION_X_DIRECTOR);
        registerExtType("csh", MIME_APPLICATION_X_CSH);
        registerExtType("cpio", MIME_APPLICATION_X_CPIO);
        registerExtType("pgn", MIME_APPLICATION_X_CHESS_PGN);
        registerExtType("vcd", MIME_APPLICATION_X_CDLINK);
        registerExtType("bcpio", MIME_APPLICATION_X_BCPIO);
        registerExtType("rm", MIME_APPLICATION_VND_RNREALMEDIA);
        registerExtType("ppt", MIME_APPLICATION_VND_MSPOWERPOINT);
        registerExtType("mif", MIME_APPLICATION_VND_MIF);
        registerExtType("grxml", MIME_APPLICATION_SRGS_XML);
        registerExtType("gram", MIME_APPLICATION_SRGS);
        registerExtType("smil", MIME_APPLICATION_RDF_SMIL);
        registerExtType("smi", MIME_APPLICATION_RDF_SMIL);
        registerExtType("rdf", MIME_APPLICATION_RDF_XML);
        registerExtType("ogg", MIME_APPLICATION_X_OGG);
        registerExtType("oda", MIME_APPLICATION_ODA);
        registerExtType("dmg", MIME_APPLICATION_OCTET_STREAM);
        registerExtType("lzh", MIME_APPLICATION_OCTET_STREAM);
        registerExtType("so", MIME_APPLICATION_OCTET_STREAM);
        registerExtType("lha", MIME_APPLICATION_OCTET_STREAM);
        registerExtType("dms", MIME_APPLICATION_OCTET_STREAM);
        registerExtType("bin", MIME_APPLICATION_OCTET_STREAM);
        registerExtType("mathml", MIME_APPLICATION_MATHML_XML);
        registerExtType("cpt", MIME_APPLICATION_MAC_COMPACTPRO);
        registerExtType("hqx", MIME_APPLICATION_MAC_BINHEX40);
        registerExtType("jnlp", MIME_APPLICATION_JNLP);
        registerExtType("ez", MIME_APPLICATION_ANDREW_INSET);
        registerExtType("txt", MIME_TEXT_PLAIN);
        registerExtType("ini", MIME_TEXT_PLAIN);
        registerExtType("c", MIME_TEXT_PLAIN);
        registerExtType("h", MIME_TEXT_PLAIN);
        registerExtType("cpp", MIME_TEXT_PLAIN);
        registerExtType("cxx", MIME_TEXT_PLAIN);
        registerExtType("cc", MIME_TEXT_PLAIN);
        registerExtType("chh", MIME_TEXT_PLAIN);
        registerExtType("java", MIME_TEXT_PLAIN);
        registerExtType("csv", MIME_TEXT_PLAIN);
        registerExtType("bat", MIME_TEXT_PLAIN);
        registerExtType("cmd", MIME_TEXT_PLAIN);
        registerExtType("asc", MIME_TEXT_PLAIN);
        registerExtType("rtf", MIME_TEXT_RTF);
        registerExtType("rtx", MIME_TEXT_RICHTEXT);
        registerExtType("html", MIME_TEXT_HTML);
        registerExtType("htm", MIME_TEXT_HTML);
        registerExtType("zip", MIME_APPLICATION_ZIP);
        registerExtType("rar", MIME_APPLICATION_X_RAR_COMPRESSED);
        registerExtType("gzip", MIME_APPLICATION_X_GZIP);
        registerExtType("gz", MIME_APPLICATION_X_GZIP);
        registerExtType("tgz", MIME_APPLICATION_TGZ);
        registerExtType("tar", MIME_APPLICATION_X_TAR);
        registerExtType("gif", MIME_IMAGE_GIF);
        registerExtType("jpeg", MIME_IMAGE_JPEG);
        registerExtType("jpg", MIME_IMAGE_JPEG);
        registerExtType("jpe", MIME_IMAGE_JPEG);
        registerExtType("tiff", MIME_IMAGE_TIFF);
        registerExtType("tif", MIME_IMAGE_TIFF);
        registerExtType("png", MIME_IMAGE_PNG);
        registerExtType("au", MIME_AUDIO_BASIC);
        registerExtType("snd", MIME_AUDIO_BASIC);
        registerExtType("wav", MIME_AUDIO_X_WAV);
        registerExtType("mov", MIME_VIDEO_QUICKTIME);
        registerExtType("qt", MIME_VIDEO_QUICKTIME);
        registerExtType("mpeg", MIME_VIDEO_MPEG);
        registerExtType("mpg", MIME_VIDEO_MPEG);
        registerExtType("mpe", MIME_VIDEO_MPEG);
        registerExtType("abs", MIME_VIDEO_MPEG);
        registerExtType("doc", MIME_APPLICATION_MSWORD);
        registerExtType("docx", MIME_APPLICATION_MSWORD_2007);
        registerExtType("odt", MIME_APPLICATION_VND_TEXT);
        registerExtType("xls", MIME_APPLICATION_VND_MSEXCEL);
        registerExtType("xlsx", MIME_APPLICATION_VND_MSEXCEL_2007);
        registerExtType("ods", MIME_APPLICATION_VND_SPREADSHEET);
        registerExtType("eps", MIME_APPLICATION_POSTSCRIPT);
        registerExtType("ai", MIME_APPLICATION_POSTSCRIPT);
        registerExtType("ps", MIME_APPLICATION_POSTSCRIPT);
        registerExtType("pdf", MIME_APPLICATION_PDF);
        registerExtType("exe", MIME_APPLICATION_OCTET_STREAM);
        registerExtType("dll", MIME_APPLICATION_OCTET_STREAM);
        registerExtType("class", MIME_APPLICATION_OCTET_STREAM);
        registerExtType("jar", MIME_APPLICATION_JAVA_ARCHIVE);
    }
    /**
     * Registers MIME type for provided extension. Existing extension type will be overriden.
     */
    public static void registerExtType(String ext, String mimeType) {
        mimeTypeMapping.put(ext, mimeType);
    }
    public static void registerMimeType(String ext, String mimeType) {
        extMapping.put(ext, mimeType);
    }
    /**
     * Returns the corresponding MIME type to the given extension.
     * If no MIME type was found it returns 'application/octet-stream' type.
     */
    public static String getMimeType(String ext) {
        String mimeType = lookupMimeType(ext);
        if (mimeType == null) {
            mimeType = MIME_APPLICATION_OCTET_STREAM;
        }
        return mimeType;
    }

    /**
     * Simply returns MIME type or <code>null</code> if no type is found.
     */
    public static String lookupMimeType(String ext) {
        return mimeTypeMapping.get(ext.toLowerCase());
    }

    /**
     * Simply returns Ext or <code>null</code> if no Mimetype is found.
     */
    public static String lookupExt(String mimeType) {
        return extMapping.get(mimeType.toLowerCase());
    }

    /**
     * Returns the default Ext to the given MimeType.
     * If no MIME type was found it returns 'unknown' ext.
     */
    public static String getDefaultExt(String mimeType) {
        String ext = lookupExt(mimeType);
        if (ext == null) {
            ext = "unknown";
        }
        return ext;
    }
}
