package cnki.kg.demo.datatransfer;

import lombok.Data;

@Data
public class DataSourceItem {
    private String url ;
    private String driverName ;
    private String user ;
    private String password ;
    private String tableName ;
}
