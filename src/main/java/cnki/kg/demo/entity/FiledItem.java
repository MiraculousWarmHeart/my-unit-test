package cnki.kg.demo.entity;

import lombok.Data;

@Data
public class FiledItem {
    String fieldName;
    String fieldAliaName;
    boolean isPk;
    boolean isInKG;
    String fieldStyle;
    boolean isOverView;
    boolean isDetailView;
}
