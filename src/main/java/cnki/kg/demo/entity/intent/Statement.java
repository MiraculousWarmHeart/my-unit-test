package cnki.kg.demo.entity.intent;

import lombok.Data;

import java.io.Serializable;

@Data
public class Statement implements Serializable {
    private String target;
    private String condition;
    private Object value;
    private Integer fieldtype;
}
