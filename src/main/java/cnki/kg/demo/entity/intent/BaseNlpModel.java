package cnki.kg.demo.entity.intent;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class BaseNlpModel implements Serializable {
    private Double score;
    private String subdomainCode;
    private String subdomainName;
    private List<Statement> statement;
}
