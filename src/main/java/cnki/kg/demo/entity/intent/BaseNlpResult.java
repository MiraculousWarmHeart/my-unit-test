package cnki.kg.demo.entity.intent;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class BaseNlpResult implements Serializable {
    private String Keyword;
    private String domain;
    private List<BaseNlpModel> items;
    private List<Statement> statement;
    private String datetime;
    private String region;
}
