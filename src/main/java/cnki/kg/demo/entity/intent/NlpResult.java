package cnki.kg.demo.entity.intent;

import java.io.Serializable;

public class NlpResult implements Serializable {
    private String keyword;
    private String region;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDatetimett() {
        return datetimett;
    }

    public void setDatetimett(String datetimett) {
        this.datetimett = datetimett;
    }

    private String datetimett;
    //private List<DomainResult> domainResult;

}
