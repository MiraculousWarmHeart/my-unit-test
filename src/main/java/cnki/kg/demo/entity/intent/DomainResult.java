package cnki.kg.demo.entity.intent;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class DomainResult implements Serializable {
    private Integer domainId;
    private String domain;
    private Integer templateId;
    private Double domainScore;
    private List<BaseNlpModel> items;
    private List<WordItem> words;
}
