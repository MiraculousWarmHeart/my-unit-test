package cnki.kg.demo.entity.intent;

import lombok.Data;

@Data
public class WordItem {
    private String content;
    private String hypernymcontent;
    private String hyponymcontent;
    private String synonymcontent;
}
