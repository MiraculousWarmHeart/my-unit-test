package cnki.kg.demo.entity.intent;

import lombok.Data;

@Data
public class BaseEntityItem {
    private String entity;
    private String code;
    private String state;
}
