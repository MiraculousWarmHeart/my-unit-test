package cnki.kg.demo.entity;

import lombok.Data;

@Data
public class CategoryNode {
    private String CategoryNodeID;
    private Integer parentid;
    private Integer level;
    private Integer IsLeaf;
    private Integer Status;
    private Integer Birth;
    private Integer Death;
    private String PinYin;
    private double SortCode;
    private String CreateUser;
    private String CreateTime;
    private String ModifyUser;
    private String ModifyTime;
    private Integer CategoryTypeID;
    private String CategoryNodeName;
    private String formula;
    private String CategoryNodeCode;
    private String uuid;
    private String entityId;
    private String label;
}
