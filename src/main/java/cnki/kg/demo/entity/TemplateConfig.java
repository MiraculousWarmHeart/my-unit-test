package cnki.kg.demo.entity;

import lombok.Data;

@Data
public class TemplateConfig {
    private String fieldname; // 字段
    private String fieldalia; // 字段别名
    private String displayname; // 字段别名
    private String operate;//操作
    private Integer filedtype;//字段类型
    private String joinstr;//与前面条件关系，and/or
}
