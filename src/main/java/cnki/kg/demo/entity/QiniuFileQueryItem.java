package cnki.kg.demo.entity;

import lombok.Data;

@Data
public class QiniuFileQueryItem {
    private String marker;
    private int limit;
    private String prefix;
    private String delimiter;
}
