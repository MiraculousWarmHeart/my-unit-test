package cnki.kg.demo.entity;

import lombok.Data;

import java.util.List;
@Data
public class QuestionTemplate {
    private Integer templateid; // 问题编号
    private Integer tableid; // 表id
    private Integer intentiondomainid; // 意图领域id
    private String question; // 问题
    private String segment; // 分词
    private String regular; // 正则
    private String tablecode;
    private String tablename;
    private String modelIdStr;
    private List<TemplateConfig> templateconfigs;
}
